﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Group))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Group))==(Machine(Group));
  Level(Machine(Group))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Group)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Group))==(?)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Group))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Group))==(?);
  List_Includes(Machine(Group))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Group))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Group))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Group))==(?);
  Context_List_Variables(Machine(Group))==(?);
  Abstract_List_Variables(Machine(Group))==(?);
  Local_List_Variables(Machine(Group))==(?);
  List_Variables(Machine(Group))==(?);
  External_List_Variables(Machine(Group))==(?)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Group))==(?);
  Abstract_List_VisibleVariables(Machine(Group))==(?);
  External_List_VisibleVariables(Machine(Group))==(?);
  Expanded_List_VisibleVariables(Machine(Group))==(?);
  List_VisibleVariables(Machine(Group))==(?);
  Internal_List_VisibleVariables(Machine(Group))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Group))==(btrue);
  Gluing_List_Invariant(Machine(Group))==(btrue);
  Expanded_List_Invariant(Machine(Group))==(btrue);
  Abstract_List_Invariant(Machine(Group))==(btrue);
  Context_List_Invariant(Machine(Group))==(btrue);
  List_Invariant(Machine(Group))==(btrue)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Group))==(btrue);
  Abstract_List_Assertions(Machine(Group))==(btrue);
  Context_List_Assertions(Machine(Group))==(btrue);
  List_Assertions(Machine(Group))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Group))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Group))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Group))==(skip);
  Context_List_Initialisation(Machine(Group))==(skip);
  List_Initialisation(Machine(Group))==(skip)
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Group))==(?)
END
&
THEORY ListInstanciatedParametersX END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Group))==(btrue);
  List_Constraints(Machine(Group))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Group))==(?);
  List_Operations(Machine(Group))==(?)
END
&
THEORY ListInputX END
&
THEORY ListOutputX END
&
THEORY ListHeaderX END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX END
&
THEORY ListSubstitutionX END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Group))==(?);
  Inherited_List_Constants(Machine(Group))==(?);
  List_Constants(Machine(Group))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(Group),GROUP)==(?);
  Context_List_Enumerated(Machine(Group))==(?);
  Context_List_Defered(Machine(Group))==(?);
  Context_List_Sets(Machine(Group))==(?);
  List_Valuable_Sets(Machine(Group))==(GROUP);
  Inherited_List_Enumerated(Machine(Group))==(?);
  Inherited_List_Defered(Machine(Group))==(?);
  Inherited_List_Sets(Machine(Group))==(?);
  List_Enumerated(Machine(Group))==(?);
  List_Defered(Machine(Group))==(GROUP);
  List_Sets(Machine(Group))==(GROUP)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Group))==(?);
  Expanded_List_HiddenConstants(Machine(Group))==(?);
  List_HiddenConstants(Machine(Group))==(?);
  External_List_HiddenConstants(Machine(Group))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Group))==(btrue);
  Context_List_Properties(Machine(Group))==(btrue);
  Inherited_List_Properties(Machine(Group))==(btrue);
  List_Properties(Machine(Group))==(GROUP: FIN(INTEGER) & not(GROUP = {}))
END
&
THEORY ListSeenInfoX END
&
THEORY ListANYVarX END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Group)) == (GROUP | ? | ? | ? | ? | ? | ? | ? | Group);
  List_Of_HiddenCst_Ids(Machine(Group)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Group)) == (?);
  List_Of_VisibleVar_Ids(Machine(Group)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Group)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(Group)) == (Type(GROUP) == Cst(SetOf(atype(GROUP,"[GROUP","]GROUP"))))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
