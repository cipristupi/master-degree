﻿/* CatalogItem
 * Author: cipri
 * Creation date: 12/28/2016
 */
MACHINE
    CatalogItem
USES
    Student
SETS
    STATUS = { present , absent } ;
    CATALOGITEM
ABSTRACT_VARIABLES
    catalogItem , status , relatedStudent , mark
INVARIANT
    catalogItem <: CATALOGITEM &
    status : catalogItem --> STATUS &
    relatedStudent : catalogItem --> STUDENT &
    mark : catalogItem --> NATURAL1
INITIALISATION
    catalogItem := {} || status := {} || relatedStudent := {} || mark := {}
OPERATIONS

    //status property

    status_value <-- get_status ( catalogItem_value ) =
    PRE
        catalogItem_value : catalogItem
    THEN
        status_value := status ( catalogItem_value )
    END ;

    set_student_id ( catalogItem_value , status_value ) =
    PRE
        catalogItem_value : catalogItem &
        status_value : STATUS
    THEN
        status ( catalogItem_value ) := status_value
    END ;


    //relatedStudent property

    relatedStudent_value <-- get_relatedStudent ( catalogItem_value ) =
    PRE
        catalogItem_value : catalogItem
    THEN
        relatedStudent_value := relatedStudent ( catalogItem_value )
    END ;

    set_relatedStudent ( catalogItem_value , relatedStudent_value ) =
    PRE
        catalogItem_value : catalogItem &
        relatedStudent_value : STUDENT
    THEN
        relatedStudent ( catalogItem_value ) := relatedStudent_value
    END ;


    //mark property

    mark_value <-- get_mark ( catalogItem_value ) =
    PRE
        catalogItem_value : catalogItem
    THEN
        mark_value := mark ( catalogItem_value )
    END ;

    set_mark ( catalogItem_value , mark_value ) =
    PRE
        catalogItem_value : catalogItem &
        mark_value : NATURAL1
    THEN
        mark ( catalogItem_value ) := mark_value
    END

END
