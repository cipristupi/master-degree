﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Student))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Student))==(Machine(Student));
  Level(Machine(Student))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Student)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Student))==(String,Group)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Student))==(Lecture)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Student))==(?);
  List_Includes(Machine(Student))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Student))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Student))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Student))==(lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture);
  Context_List_Variables(Machine(Student))==(lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture);
  Abstract_List_Variables(Machine(Student))==(?);
  Local_List_Variables(Machine(Student))==(student_group,student_name,student_id,student);
  List_Variables(Machine(Student))==(student_group,student_name,student_id,student);
  External_List_Variables(Machine(Student))==(student_group,student_name,student_id,student)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Student))==(?);
  Abstract_List_VisibleVariables(Machine(Student))==(?);
  External_List_VisibleVariables(Machine(Student))==(?);
  Expanded_List_VisibleVariables(Machine(Student))==(?);
  List_VisibleVariables(Machine(Student))==(?);
  Internal_List_VisibleVariables(Machine(Student))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Student))==(btrue);
  Gluing_List_Invariant(Machine(Student))==(btrue);
  Expanded_List_Invariant(Machine(Student))==(btrue);
  Abstract_List_Invariant(Machine(Student))==(btrue);
  Context_List_Invariant(Machine(Student))==(lecture <: LECTURE & lecture_code: lecture >-> NATURAL1 & lecture_name: lecture --> STR & lecture_numberOfCredits: lecture --> NATURAL1 & lecture_semester: lecture --> NATURAL1 & lecture_preRequisite: lecture --> POW(lecture));
  List_Invariant(Machine(Student))==(student <: STUDENT & student_id: student >-> NATURAL1 & student_name: student --> STR & student_group: student --> GROUP)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Student))==(btrue);
  Abstract_List_Assertions(Machine(Student))==(btrue);
  Context_List_Assertions(Machine(Student))==(btrue);
  List_Assertions(Machine(Student))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Student))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Student))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Student))==(student,student_id,student_name,student_group:={},{},{},{});
  Context_List_Initialisation(Machine(Student))==(lecture,lecture_code,lecture_name,lecture_numberOfCredits,lecture_semester,lecture_preRequisite:={},{},{},{},{},{});
  List_Initialisation(Machine(Student))==(student:={} || student_id:={} || student_name:={} || student_group:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Student))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Student),Machine(Lecture))==(?);
  List_Instanciated_Parameters(Machine(Student),Machine(String))==(?);
  List_Instanciated_Parameters(Machine(Student),Machine(Group))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Student))==(btrue);
  List_Constraints(Machine(Student))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Student))==(create_student,get_student_id,set_student_id,get_student_name,set_student_name,get_student_group,set_student_group);
  List_Operations(Machine(Student))==(create_student,get_student_id,set_student_id,get_student_name,set_student_name,get_student_group,set_student_group)
END
&
THEORY ListInputX IS
  List_Input(Machine(Student),create_student)==(id_value,name_value,group_value);
  List_Input(Machine(Student),get_student_id)==(student_value);
  List_Input(Machine(Student),set_student_id)==(student_value,id_value);
  List_Input(Machine(Student),get_student_name)==(student_value);
  List_Input(Machine(Student),set_student_name)==(student_value,name_value);
  List_Input(Machine(Student),get_student_group)==(student_value);
  List_Input(Machine(Student),set_student_group)==(student_value,group_value)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Student),create_student)==(student_value);
  List_Output(Machine(Student),get_student_id)==(id_value);
  List_Output(Machine(Student),set_student_id)==(?);
  List_Output(Machine(Student),get_student_name)==(name_value);
  List_Output(Machine(Student),set_student_name)==(?);
  List_Output(Machine(Student),get_student_group)==(group_value);
  List_Output(Machine(Student),set_student_group)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Student),create_student)==(student_value <-- create_student(id_value,name_value,group_value));
  List_Header(Machine(Student),get_student_id)==(id_value <-- get_student_id(student_value));
  List_Header(Machine(Student),set_student_id)==(set_student_id(student_value,id_value));
  List_Header(Machine(Student),get_student_name)==(name_value <-- get_student_name(student_value));
  List_Header(Machine(Student),set_student_name)==(set_student_name(student_value,name_value));
  List_Header(Machine(Student),get_student_group)==(group_value <-- get_student_group(student_value));
  List_Header(Machine(Student),set_student_group)==(set_student_group(student_value,group_value))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Student),create_student)==(id_value: NATURAL1 & id_value/:ran(student_id) & name_value: STR & group_value: GROUP);
  List_Precondition(Machine(Student),get_student_id)==(student_value: student);
  List_Precondition(Machine(Student),set_student_id)==(student_value: student & id_value: NATURAL1 & id_value/:ran(student_id));
  List_Precondition(Machine(Student),get_student_name)==(student_value: student);
  List_Precondition(Machine(Student),set_student_name)==(student_value: student & name_value: STR);
  List_Precondition(Machine(Student),get_student_group)==(student_value: student);
  List_Precondition(Machine(Student),set_student_group)==(student_value: student & group_value: GROUP)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Student),set_student_group)==(student_value: student & group_value: GROUP | student_group:=student_group<+{student_value|->group_value});
  Expanded_List_Substitution(Machine(Student),get_student_group)==(student_value: student | group_value:=student_group(student_value));
  Expanded_List_Substitution(Machine(Student),set_student_name)==(student_value: student & name_value: STR | student_name:=student_name<+{student_value|->name_value});
  Expanded_List_Substitution(Machine(Student),get_student_name)==(student_value: student | name_value:=student_name(student_value));
  Expanded_List_Substitution(Machine(Student),set_student_id)==(student_value: student & id_value: NATURAL1 & id_value/:ran(student_id) | student_id:=student_id<+{student_value|->id_value});
  Expanded_List_Substitution(Machine(Student),get_student_id)==(student_value: student | id_value:=student_id(student_value));
  Expanded_List_Substitution(Machine(Student),create_student)==(id_value: NATURAL1 & id_value/:ran(student_id) & name_value: STR & group_value: GROUP | @std.(std: STUDENT-student ==> student,student_id,student_name,student_group,student_value:=student-{std},student_id<+{std|->id_value},student_name<+{std|->name_value},student_group<+{std|->group_value},std));
  List_Substitution(Machine(Student),create_student)==(ANY std WHERE std: STUDENT-student THEN student:=student-{std} || student_id(std):=id_value || student_name(std):=name_value || student_group(std):=group_value || student_value:=std END);
  List_Substitution(Machine(Student),get_student_id)==(id_value:=student_id(student_value));
  List_Substitution(Machine(Student),set_student_id)==(student_id(student_value):=id_value);
  List_Substitution(Machine(Student),get_student_name)==(name_value:=student_name(student_value));
  List_Substitution(Machine(Student),set_student_name)==(student_name(student_value):=name_value);
  List_Substitution(Machine(Student),get_student_group)==(group_value:=student_group(student_value));
  List_Substitution(Machine(Student),set_student_group)==(student_group(student_value):=group_value)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Student))==(?);
  Inherited_List_Constants(Machine(Student))==(?);
  List_Constants(Machine(Student))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(Student),STUDENT)==(?);
  Context_List_Enumerated(Machine(Student))==(?);
  Context_List_Defered(Machine(Student))==(STR,GROUP,LECTURE);
  Context_List_Sets(Machine(Student))==(STR,GROUP,LECTURE);
  List_Valuable_Sets(Machine(Student))==(STUDENT);
  Inherited_List_Enumerated(Machine(Student))==(?);
  Inherited_List_Defered(Machine(Student))==(?);
  Inherited_List_Sets(Machine(Student))==(?);
  List_Enumerated(Machine(Student))==(?);
  List_Defered(Machine(Student))==(STUDENT);
  List_Sets(Machine(Student))==(STUDENT)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Student))==(?);
  Expanded_List_HiddenConstants(Machine(Student))==(?);
  List_HiddenConstants(Machine(Student))==(?);
  External_List_HiddenConstants(Machine(Student))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Student))==(btrue);
  Context_List_Properties(Machine(Student))==(STR: FIN(INTEGER) & not(STR = {}) & GROUP: FIN(INTEGER) & not(GROUP = {}) & LECTURE: FIN(INTEGER) & not(LECTURE = {}));
  Inherited_List_Properties(Machine(Student))==(btrue);
  List_Properties(Machine(Student))==(STUDENT: FIN(INTEGER) & not(STUDENT = {}))
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Student),Machine(Group))==(?);
  Seen_Context_List_Enumerated(Machine(Student))==(?);
  Seen_Context_List_Invariant(Machine(Student))==(btrue);
  Seen_Context_List_Assertions(Machine(Student))==(btrue);
  Seen_Context_List_Properties(Machine(Student))==(btrue);
  Seen_List_Constraints(Machine(Student))==(btrue);
  Seen_List_Operations(Machine(Student),Machine(Group))==(?);
  Seen_Expanded_List_Invariant(Machine(Student),Machine(Group))==(btrue);
  Seen_Internal_List_Operations(Machine(Student),Machine(String))==(?);
  Seen_List_Operations(Machine(Student),Machine(String))==(?);
  Seen_Expanded_List_Invariant(Machine(Student),Machine(String))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Student),create_student)==(Var(std) == atype(STUDENT,?,?));
  List_ANY_Var(Machine(Student),get_student_id)==(?);
  List_ANY_Var(Machine(Student),set_student_id)==(?);
  List_ANY_Var(Machine(Student),get_student_name)==(?);
  List_ANY_Var(Machine(Student),set_student_name)==(?);
  List_ANY_Var(Machine(Student),get_student_group)==(?);
  List_ANY_Var(Machine(Student),set_student_group)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Student)) == (STUDENT | ? | student_group,student_name,student_id,student | ? | create_student,get_student_id,set_student_id,get_student_name,set_student_name,get_student_group,set_student_group | ? | seen(Machine(String)),seen(Machine(Group)),used(Machine(Lecture)) | ? | Student);
  List_Of_HiddenCst_Ids(Machine(Student)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Student)) == (?);
  List_Of_VisibleVar_Ids(Machine(Student)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Student)) == (?: ?);
  List_Of_Ids(Machine(Lecture)) == (LECTURE | ? | lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture | ? | create_lecture,delete_lecture,get_lecture_code,set_lecture_code,get_lecture_name,set_lecture_name,get_lecture_numberOfCredits,set_lecture_numberOfCredits,get_lecture_semester,set_lecture_semester,get_lecture_prerequisite,set_lecture_prerequisite | ? | seen(Machine(String)) | ? | Lecture);
  List_Of_HiddenCst_Ids(Machine(Lecture)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Lecture)) == (?);
  List_Of_VisibleVar_Ids(Machine(Lecture)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Lecture)) == (?: ?);
  List_Of_Ids(Machine(String)) == (STR | ? | ? | ? | ? | ? | ? | ? | String);
  List_Of_HiddenCst_Ids(Machine(String)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(String)) == (?);
  List_Of_VisibleVar_Ids(Machine(String)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(String)) == (?: ?);
  List_Of_Ids(Machine(Group)) == (GROUP | ? | ? | ? | ? | ? | ? | ? | Group);
  List_Of_HiddenCst_Ids(Machine(Group)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Group)) == (?);
  List_Of_VisibleVar_Ids(Machine(Group)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Group)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(Student)) == (Type(STUDENT) == Cst(SetOf(atype(STUDENT,"[STUDENT","]STUDENT"))))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Student)) == (Type(student_group) == Mvl(SetOf(atype(STUDENT,?,?)*atype(GROUP,"[GROUP","]GROUP")));Type(student_name) == Mvl(SetOf(atype(STUDENT,?,?)*atype(STR,"[STR","]STR")));Type(student_id) == Mvl(SetOf(atype(STUDENT,?,?)*btype(INTEGER,?,?)));Type(student) == Mvl(SetOf(atype(STUDENT,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Student)) == (Type(set_student_group) == Cst(No_type,atype(STUDENT,?,?)*atype(GROUP,?,?));Type(get_student_group) == Cst(atype(GROUP,?,?),atype(STUDENT,?,?));Type(set_student_name) == Cst(No_type,atype(STUDENT,?,?)*atype(STR,?,?));Type(get_student_name) == Cst(atype(STR,?,?),atype(STUDENT,?,?));Type(set_student_id) == Cst(No_type,atype(STUDENT,?,?)*btype(INTEGER,?,?));Type(get_student_id) == Cst(btype(INTEGER,?,?),atype(STUDENT,?,?));Type(create_student) == Cst(atype(STUDENT,?,?),btype(INTEGER,?,?)*atype(STR,?,?)*atype(GROUP,?,?)));
  Observers(Machine(Student)) == (Type(get_student_group) == Cst(atype(GROUP,?,?),atype(STUDENT,?,?));Type(get_student_name) == Cst(atype(STR,?,?),atype(STUDENT,?,?));Type(get_student_id) == Cst(btype(INTEGER,?,?),atype(STUDENT,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
