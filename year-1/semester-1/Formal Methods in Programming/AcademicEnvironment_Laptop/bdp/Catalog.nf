﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Catalog))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Catalog))==(Machine(Catalog));
  Level(Machine(Catalog))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Catalog)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Catalog))==(String,Group)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Catalog))==(Lecture,CatalogItem)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Catalog))==(?);
  List_Includes(Machine(Catalog))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Catalog))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Catalog))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Catalog))==(lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture,mark,relatedStudent,status,catalogItem);
  Context_List_Variables(Machine(Catalog))==(lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture,mark,relatedStudent,status,catalogItem);
  Abstract_List_Variables(Machine(Catalog))==(?);
  Local_List_Variables(Machine(Catalog))==(group,type,relatedLecture,catalogItems,catalog);
  List_Variables(Machine(Catalog))==(group,type,relatedLecture,catalogItems,catalog);
  External_List_Variables(Machine(Catalog))==(group,type,relatedLecture,catalogItems,catalog)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Catalog))==(?);
  Abstract_List_VisibleVariables(Machine(Catalog))==(?);
  External_List_VisibleVariables(Machine(Catalog))==(?);
  Expanded_List_VisibleVariables(Machine(Catalog))==(?);
  List_VisibleVariables(Machine(Catalog))==(?);
  Internal_List_VisibleVariables(Machine(Catalog))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Catalog))==(btrue);
  Gluing_List_Invariant(Machine(Catalog))==(btrue);
  Expanded_List_Invariant(Machine(Catalog))==(btrue);
  Abstract_List_Invariant(Machine(Catalog))==(btrue);
  Context_List_Invariant(Machine(Catalog))==(lecture <: LECTURE & lecture_code: lecture >-> NATURAL1 & lecture_name: lecture --> STR & lecture_numberOfCredits: lecture --> NATURAL1 & lecture_semester: lecture --> NATURAL1 & lecture_preRequisite: lecture --> POW(lecture) & catalogItem <: CATALOGITEM & status: catalogItem --> STATUS & relatedStudent: catalogItem --> STUDENT & mark: catalogItem --> NATURAL1);
  List_Invariant(Machine(Catalog))==(catalog <: CATALOG & catalogItems: catalog --> CATALOGITEM & relatedLecture: catalog --> LECTURE & type: catalog --> TYPE & group: catalog --> GROUPS)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Catalog))==(btrue);
  Abstract_List_Assertions(Machine(Catalog))==(btrue);
  Context_List_Assertions(Machine(Catalog))==(btrue);
  List_Assertions(Machine(Catalog))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Catalog))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Catalog))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Catalog))==(catalog,catalogItems,relatedLecture,type,group:={},{},{},{},{});
  Context_List_Initialisation(Machine(Catalog))==(lecture,lecture_code,lecture_name,lecture_numberOfCredits,lecture_semester,lecture_preRequisite:={},{},{},{},{},{};catalogItem,status,relatedStudent,mark:={},{},{},{});
  List_Initialisation(Machine(Catalog))==(catalog:={} || catalogItems:={} || relatedLecture:={} || type:={} || group:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Catalog))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Catalog),Machine(Lecture))==(?);
  List_Instanciated_Parameters(Machine(Catalog),Machine(CatalogItem))==(?);
  List_Instanciated_Parameters(Machine(Catalog),Machine(String))==(?);
  List_Instanciated_Parameters(Machine(Catalog),Machine(Group))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Catalog))==(btrue);
  List_Constraints(Machine(Catalog))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Catalog))==(create_catalog,get_catalog_items,set_catalogItems,get_relatedLecture,set_relatedLecture,get_group,set_group,get_type,set_type);
  List_Operations(Machine(Catalog))==(create_catalog,get_catalog_items,set_catalogItems,get_relatedLecture,set_relatedLecture,get_group,set_group,get_type,set_type)
END
&
THEORY ListInputX IS
  List_Input(Machine(Catalog),create_catalog)==(relatedLecture_value,type_value,group_value);
  List_Input(Machine(Catalog),get_catalog_items)==(catalog_value);
  List_Input(Machine(Catalog),set_catalogItems)==(catalog_value,catalogItems_value);
  List_Input(Machine(Catalog),get_relatedLecture)==(catalog_value);
  List_Input(Machine(Catalog),set_relatedLecture)==(catalog_value,relatedLecture_value);
  List_Input(Machine(Catalog),get_group)==(catalog_value);
  List_Input(Machine(Catalog),set_group)==(catalog_value,group_value);
  List_Input(Machine(Catalog),get_type)==(catalog_value);
  List_Input(Machine(Catalog),set_type)==(catalog_value,type_value)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Catalog),create_catalog)==(catalog_value);
  List_Output(Machine(Catalog),get_catalog_items)==(catalogItems_value);
  List_Output(Machine(Catalog),set_catalogItems)==(?);
  List_Output(Machine(Catalog),get_relatedLecture)==(relatedLecture_value);
  List_Output(Machine(Catalog),set_relatedLecture)==(?);
  List_Output(Machine(Catalog),get_group)==(group_value);
  List_Output(Machine(Catalog),set_group)==(?);
  List_Output(Machine(Catalog),get_type)==(type_value);
  List_Output(Machine(Catalog),set_type)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Catalog),create_catalog)==(catalog_value <-- create_catalog(relatedLecture_value,type_value,group_value));
  List_Header(Machine(Catalog),get_catalog_items)==(catalogItems_value <-- get_catalog_items(catalog_value));
  List_Header(Machine(Catalog),set_catalogItems)==(set_catalogItems(catalog_value,catalogItems_value));
  List_Header(Machine(Catalog),get_relatedLecture)==(relatedLecture_value <-- get_relatedLecture(catalog_value));
  List_Header(Machine(Catalog),set_relatedLecture)==(set_relatedLecture(catalog_value,relatedLecture_value));
  List_Header(Machine(Catalog),get_group)==(group_value <-- get_group(catalog_value));
  List_Header(Machine(Catalog),set_group)==(set_group(catalog_value,group_value));
  List_Header(Machine(Catalog),get_type)==(type_value <-- get_type(catalog_value));
  List_Header(Machine(Catalog),set_type)==(set_type(catalog_value,type_value))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Catalog),create_catalog)==(relatedLecture_value: LECTURE & type_value: TYPE & group_value: GROUPS);
  List_Precondition(Machine(Catalog),get_catalog_items)==(catalog_value: catalog);
  List_Precondition(Machine(Catalog),set_catalogItems)==(catalog_value: catalog & catalogItems_value: CATALOGITEM);
  List_Precondition(Machine(Catalog),get_relatedLecture)==(catalog_value: catalog);
  List_Precondition(Machine(Catalog),set_relatedLecture)==(catalog_value: catalog & relatedLecture_value: LECTURE);
  List_Precondition(Machine(Catalog),get_group)==(catalog_value: catalog);
  List_Precondition(Machine(Catalog),set_group)==(catalog_value: catalog & group_value: GROUPS);
  List_Precondition(Machine(Catalog),get_type)==(catalog_value: catalog);
  List_Precondition(Machine(Catalog),set_type)==(catalog_value: catalog & type_value: TYPE)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Catalog),set_type)==(catalog_value: catalog & type_value: TYPE | type:=type<+{catalog_value|->type_value});
  Expanded_List_Substitution(Machine(Catalog),get_type)==(catalog_value: catalog | type_value:=type(catalog_value));
  Expanded_List_Substitution(Machine(Catalog),set_group)==(catalog_value: catalog & group_value: GROUPS | group:=group<+{catalog_value|->group_value});
  Expanded_List_Substitution(Machine(Catalog),get_group)==(catalog_value: catalog | group_value:=group(catalog_value));
  Expanded_List_Substitution(Machine(Catalog),set_relatedLecture)==(catalog_value: catalog & relatedLecture_value: LECTURE | relatedLecture:=relatedLecture<+{catalog_value|->relatedLecture_value});
  Expanded_List_Substitution(Machine(Catalog),get_relatedLecture)==(catalog_value: catalog | relatedLecture_value:=relatedLecture(catalog_value));
  Expanded_List_Substitution(Machine(Catalog),set_catalogItems)==(catalog_value: catalog & catalogItems_value: CATALOGITEM | catalogItems:=catalogItems<+{catalog_value|->catalogItems_value});
  Expanded_List_Substitution(Machine(Catalog),get_catalog_items)==(catalog_value: catalog | catalogItems_value:=catalogItems(catalog_value));
  Expanded_List_Substitution(Machine(Catalog),create_catalog)==(relatedLecture_value: LECTURE & type_value: TYPE & group_value: GROUPS | @cat.(cat: CATALOG-catalog ==> catalog,relatedLecture,type,group,catalog_value:=catalog\/{cat},relatedLecture<+{cat|->relatedLecture_value},type<+{cat|->type_value},group<+{cat|->group_value},cat));
  List_Substitution(Machine(Catalog),create_catalog)==(ANY cat WHERE cat: CATALOG-catalog THEN catalog:=catalog\/{cat} || relatedLecture(cat):=relatedLecture_value || type(cat):=type_value || group(cat):=group_value || catalog_value:=cat END);
  List_Substitution(Machine(Catalog),get_catalog_items)==(catalogItems_value:=catalogItems(catalog_value));
  List_Substitution(Machine(Catalog),set_catalogItems)==(catalogItems(catalog_value):=catalogItems_value);
  List_Substitution(Machine(Catalog),get_relatedLecture)==(relatedLecture_value:=relatedLecture(catalog_value));
  List_Substitution(Machine(Catalog),set_relatedLecture)==(relatedLecture(catalog_value):=relatedLecture_value);
  List_Substitution(Machine(Catalog),get_group)==(group_value:=group(catalog_value));
  List_Substitution(Machine(Catalog),set_group)==(group(catalog_value):=group_value);
  List_Substitution(Machine(Catalog),get_type)==(type_value:=type(catalog_value));
  List_Substitution(Machine(Catalog),set_type)==(type(catalog_value):=type_value)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Catalog))==(?);
  Inherited_List_Constants(Machine(Catalog))==(?);
  List_Constants(Machine(Catalog))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(Catalog),STATUS)==({present,absent});
  Context_List_Enumerated(Machine(Catalog))==(STATUS);
  Context_List_Defered(Machine(Catalog))==(STR,GROUP,LECTURE,CATALOGITEM);
  Context_List_Sets(Machine(Catalog))==(STR,GROUP,LECTURE,STATUS,CATALOGITEM);
  List_Valuable_Sets(Machine(Catalog))==(CATALOG,GROUPS);
  Inherited_List_Enumerated(Machine(Catalog))==(?);
  Inherited_List_Defered(Machine(Catalog))==(?);
  Inherited_List_Sets(Machine(Catalog))==(?);
  List_Enumerated(Machine(Catalog))==(TYPE);
  List_Defered(Machine(Catalog))==(CATALOG,GROUPS);
  List_Sets(Machine(Catalog))==(CATALOG,TYPE,GROUPS);
  Set_Definition(Machine(Catalog),CATALOG)==(?);
  Set_Definition(Machine(Catalog),TYPE)==({normal,reExamination});
  Set_Definition(Machine(Catalog),GROUPS)==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Catalog))==(?);
  Expanded_List_HiddenConstants(Machine(Catalog))==(?);
  List_HiddenConstants(Machine(Catalog))==(?);
  External_List_HiddenConstants(Machine(Catalog))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Catalog))==(btrue);
  Context_List_Properties(Machine(Catalog))==(STR: FIN(INTEGER) & not(STR = {}) & GROUP: FIN(INTEGER) & not(GROUP = {}) & LECTURE: FIN(INTEGER) & not(LECTURE = {}) & CATALOGITEM: FIN(INTEGER) & not(CATALOGITEM = {}) & STATUS: FIN(INTEGER) & not(STATUS = {}));
  Inherited_List_Properties(Machine(Catalog))==(btrue);
  List_Properties(Machine(Catalog))==(CATALOG: FIN(INTEGER) & not(CATALOG = {}) & GROUPS: FIN(INTEGER) & not(GROUPS = {}) & TYPE: FIN(INTEGER) & not(TYPE = {}))
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Catalog),Machine(Group))==(?);
  Seen_Context_List_Enumerated(Machine(Catalog))==(?);
  Seen_Context_List_Invariant(Machine(Catalog))==(btrue);
  Seen_Context_List_Assertions(Machine(Catalog))==(btrue);
  Seen_Context_List_Properties(Machine(Catalog))==(btrue);
  Seen_List_Constraints(Machine(Catalog))==(btrue);
  Seen_List_Operations(Machine(Catalog),Machine(Group))==(?);
  Seen_Expanded_List_Invariant(Machine(Catalog),Machine(Group))==(btrue);
  Seen_Internal_List_Operations(Machine(Catalog),Machine(String))==(?);
  Seen_List_Operations(Machine(Catalog),Machine(String))==(?);
  Seen_Expanded_List_Invariant(Machine(Catalog),Machine(String))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Catalog),create_catalog)==(Var(cat) == atype(CATALOG,?,?));
  List_ANY_Var(Machine(Catalog),get_catalog_items)==(?);
  List_ANY_Var(Machine(Catalog),set_catalogItems)==(?);
  List_ANY_Var(Machine(Catalog),get_relatedLecture)==(?);
  List_ANY_Var(Machine(Catalog),set_relatedLecture)==(?);
  List_ANY_Var(Machine(Catalog),get_group)==(?);
  List_ANY_Var(Machine(Catalog),set_group)==(?);
  List_ANY_Var(Machine(Catalog),get_type)==(?);
  List_ANY_Var(Machine(Catalog),set_type)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Catalog)) == (CATALOG,TYPE,GROUPS,normal,reExamination | ? | group,type,relatedLecture,catalogItems,catalog | ? | create_catalog,get_catalog_items,set_catalogItems,get_relatedLecture,set_relatedLecture,get_group,set_group,get_type,set_type | ? | seen(Machine(String)),seen(Machine(Group)),used(Machine(Lecture)),used(Machine(CatalogItem)) | ? | Catalog);
  List_Of_HiddenCst_Ids(Machine(Catalog)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Catalog)) == (?);
  List_Of_VisibleVar_Ids(Machine(Catalog)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Catalog)) == (?: ?);
  List_Of_Ids(Machine(CatalogItem)) == (STATUS,CATALOGITEM,present,absent | ? | mark,relatedStudent,status,catalogItem | ? | get_status,set_student_id,get_relatedStudent,set_relatedStudent,get_mark,set_mark | ? | used(Machine(Student)) | ? | CatalogItem);
  List_Of_HiddenCst_Ids(Machine(CatalogItem)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(CatalogItem)) == (?);
  List_Of_VisibleVar_Ids(Machine(CatalogItem)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(CatalogItem)) == (?: ?);
  List_Of_Ids(Machine(Student)) == (STUDENT | ? | student_group,student_name,student_id,student | ? | create_student,get_student_id,set_student_id,get_student_name,set_student_name,get_student_group,set_student_group | ? | seen(Machine(String)),seen(Machine(Group)),used(Machine(Lecture)) | ? | Student);
  List_Of_HiddenCst_Ids(Machine(Student)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Student)) == (?);
  List_Of_VisibleVar_Ids(Machine(Student)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Student)) == (?: ?);
  List_Of_Ids(Machine(Lecture)) == (LECTURE | ? | lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture | ? | create_lecture,delete_lecture,get_lecture_code,set_lecture_code,get_lecture_name,set_lecture_name,get_lecture_numberOfCredits,set_lecture_numberOfCredits,get_lecture_semester,set_lecture_semester,get_lecture_prerequisite,set_lecture_prerequisite | ? | seen(Machine(String)) | ? | Lecture);
  List_Of_HiddenCst_Ids(Machine(Lecture)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Lecture)) == (?);
  List_Of_VisibleVar_Ids(Machine(Lecture)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Lecture)) == (?: ?);
  List_Of_Ids(Machine(String)) == (STR | ? | ? | ? | ? | ? | ? | ? | String);
  List_Of_HiddenCst_Ids(Machine(String)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(String)) == (?);
  List_Of_VisibleVar_Ids(Machine(String)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(String)) == (?: ?);
  List_Of_Ids(Machine(Group)) == (GROUP | ? | ? | ? | ? | ? | ? | ? | Group);
  List_Of_HiddenCst_Ids(Machine(Group)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Group)) == (?);
  List_Of_VisibleVar_Ids(Machine(Group)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Group)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(Catalog)) == (Type(CATALOG) == Cst(SetOf(atype(CATALOG,"[CATALOG","]CATALOG")));Type(TYPE) == Cst(SetOf(etype(TYPE,0,1)));Type(GROUPS) == Cst(SetOf(atype(GROUPS,"[GROUPS","]GROUPS"))))
END
&
THEORY ConstantsEnvX IS
  Constants(Machine(Catalog)) == (Type(normal) == Cst(etype(TYPE,0,1));Type(reExamination) == Cst(etype(TYPE,0,1)))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Catalog)) == (Type(group) == Mvl(SetOf(atype(CATALOG,?,?)*atype(GROUPS,"[GROUPS","]GROUPS")));Type(type) == Mvl(SetOf(atype(CATALOG,?,?)*etype(TYPE,0,1)));Type(relatedLecture) == Mvl(SetOf(atype(CATALOG,?,?)*atype(LECTURE,"[LECTURE","]LECTURE")));Type(catalogItems) == Mvl(SetOf(atype(CATALOG,?,?)*atype(CATALOGITEM,"[CATALOGITEM","]CATALOGITEM")));Type(catalog) == Mvl(SetOf(atype(CATALOG,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Catalog)) == (Type(set_type) == Cst(No_type,atype(CATALOG,?,?)*etype(TYPE,?,?));Type(get_type) == Cst(etype(TYPE,?,?),atype(CATALOG,?,?));Type(set_group) == Cst(No_type,atype(CATALOG,?,?)*atype(GROUPS,?,?));Type(get_group) == Cst(atype(GROUPS,?,?),atype(CATALOG,?,?));Type(set_relatedLecture) == Cst(No_type,atype(CATALOG,?,?)*atype(LECTURE,?,?));Type(get_relatedLecture) == Cst(atype(LECTURE,?,?),atype(CATALOG,?,?));Type(set_catalogItems) == Cst(No_type,atype(CATALOG,?,?)*atype(CATALOGITEM,?,?));Type(get_catalog_items) == Cst(atype(CATALOGITEM,?,?),atype(CATALOG,?,?));Type(create_catalog) == Cst(atype(CATALOG,?,?),atype(LECTURE,?,?)*etype(TYPE,?,?)*atype(GROUPS,?,?)));
  Observers(Machine(Catalog)) == (Type(get_type) == Cst(etype(TYPE,?,?),atype(CATALOG,?,?));Type(get_group) == Cst(atype(GROUPS,?,?),atype(CATALOG,?,?));Type(get_relatedLecture) == Cst(atype(LECTURE,?,?),atype(CATALOG,?,?));Type(get_catalog_items) == Cst(atype(CATALOGITEM,?,?),atype(CATALOG,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
