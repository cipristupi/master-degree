﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(CatalogItem))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(CatalogItem))==(Machine(CatalogItem));
  Level(Machine(CatalogItem))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(CatalogItem)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(CatalogItem))==(?)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(CatalogItem))==(Student)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(CatalogItem))==(?);
  List_Includes(Machine(CatalogItem))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(CatalogItem))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(CatalogItem))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(CatalogItem))==(student_group,student_name,student_id,student);
  Context_List_Variables(Machine(CatalogItem))==(student_group,student_name,student_id,student);
  Abstract_List_Variables(Machine(CatalogItem))==(?);
  Local_List_Variables(Machine(CatalogItem))==(mark,relatedStudent,status,catalogItem);
  List_Variables(Machine(CatalogItem))==(mark,relatedStudent,status,catalogItem);
  External_List_Variables(Machine(CatalogItem))==(mark,relatedStudent,status,catalogItem)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(CatalogItem))==(?);
  Abstract_List_VisibleVariables(Machine(CatalogItem))==(?);
  External_List_VisibleVariables(Machine(CatalogItem))==(?);
  Expanded_List_VisibleVariables(Machine(CatalogItem))==(?);
  List_VisibleVariables(Machine(CatalogItem))==(?);
  Internal_List_VisibleVariables(Machine(CatalogItem))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(CatalogItem))==(btrue);
  Gluing_List_Invariant(Machine(CatalogItem))==(btrue);
  Expanded_List_Invariant(Machine(CatalogItem))==(btrue);
  Abstract_List_Invariant(Machine(CatalogItem))==(btrue);
  Context_List_Invariant(Machine(CatalogItem))==(student <: STUDENT & student_id: student >-> NATURAL1 & student_name: student --> STR & student_group: student --> GROUP);
  List_Invariant(Machine(CatalogItem))==(catalogItem <: CATALOGITEM & status: catalogItem --> STATUS & relatedStudent: catalogItem --> STUDENT & mark: catalogItem --> NATURAL1)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(CatalogItem))==(btrue);
  Abstract_List_Assertions(Machine(CatalogItem))==(btrue);
  Context_List_Assertions(Machine(CatalogItem))==(btrue);
  List_Assertions(Machine(CatalogItem))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(CatalogItem))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(CatalogItem))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(CatalogItem))==(catalogItem,status,relatedStudent,mark:={},{},{},{});
  Context_List_Initialisation(Machine(CatalogItem))==(student,student_id,student_name,student_group:={},{},{},{});
  List_Initialisation(Machine(CatalogItem))==(catalogItem:={} || status:={} || relatedStudent:={} || mark:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(CatalogItem))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(CatalogItem),Machine(Student))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(CatalogItem))==(btrue);
  List_Constraints(Machine(CatalogItem))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(CatalogItem))==(get_status,set_student_id,get_relatedStudent,set_relatedStudent,get_mark,set_mark);
  List_Operations(Machine(CatalogItem))==(get_status,set_student_id,get_relatedStudent,set_relatedStudent,get_mark,set_mark)
END
&
THEORY ListInputX IS
  List_Input(Machine(CatalogItem),get_status)==(catalogItem_value);
  List_Input(Machine(CatalogItem),set_student_id)==(catalogItem_value,status_value);
  List_Input(Machine(CatalogItem),get_relatedStudent)==(catalogItem_value);
  List_Input(Machine(CatalogItem),set_relatedStudent)==(catalogItem_value,relatedStudent_value);
  List_Input(Machine(CatalogItem),get_mark)==(catalogItem_value);
  List_Input(Machine(CatalogItem),set_mark)==(catalogItem_value,mark_value)
END
&
THEORY ListOutputX IS
  List_Output(Machine(CatalogItem),get_status)==(status_value);
  List_Output(Machine(CatalogItem),set_student_id)==(?);
  List_Output(Machine(CatalogItem),get_relatedStudent)==(relatedStudent_value);
  List_Output(Machine(CatalogItem),set_relatedStudent)==(?);
  List_Output(Machine(CatalogItem),get_mark)==(mark_value);
  List_Output(Machine(CatalogItem),set_mark)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(CatalogItem),get_status)==(status_value <-- get_status(catalogItem_value));
  List_Header(Machine(CatalogItem),set_student_id)==(set_student_id(catalogItem_value,status_value));
  List_Header(Machine(CatalogItem),get_relatedStudent)==(relatedStudent_value <-- get_relatedStudent(catalogItem_value));
  List_Header(Machine(CatalogItem),set_relatedStudent)==(set_relatedStudent(catalogItem_value,relatedStudent_value));
  List_Header(Machine(CatalogItem),get_mark)==(mark_value <-- get_mark(catalogItem_value));
  List_Header(Machine(CatalogItem),set_mark)==(set_mark(catalogItem_value,mark_value))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(CatalogItem),get_status)==(catalogItem_value: catalogItem);
  List_Precondition(Machine(CatalogItem),set_student_id)==(catalogItem_value: catalogItem & status_value: STATUS);
  List_Precondition(Machine(CatalogItem),get_relatedStudent)==(catalogItem_value: catalogItem);
  List_Precondition(Machine(CatalogItem),set_relatedStudent)==(catalogItem_value: catalogItem & relatedStudent_value: STUDENT);
  List_Precondition(Machine(CatalogItem),get_mark)==(catalogItem_value: catalogItem);
  List_Precondition(Machine(CatalogItem),set_mark)==(catalogItem_value: catalogItem & mark_value: NATURAL1)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(CatalogItem),set_mark)==(catalogItem_value: catalogItem & mark_value: NATURAL1 | mark:=mark<+{catalogItem_value|->mark_value});
  Expanded_List_Substitution(Machine(CatalogItem),get_mark)==(catalogItem_value: catalogItem | mark_value:=mark(catalogItem_value));
  Expanded_List_Substitution(Machine(CatalogItem),set_relatedStudent)==(catalogItem_value: catalogItem & relatedStudent_value: STUDENT | relatedStudent:=relatedStudent<+{catalogItem_value|->relatedStudent_value});
  Expanded_List_Substitution(Machine(CatalogItem),get_relatedStudent)==(catalogItem_value: catalogItem | relatedStudent_value:=relatedStudent(catalogItem_value));
  Expanded_List_Substitution(Machine(CatalogItem),set_student_id)==(catalogItem_value: catalogItem & status_value: STATUS | status:=status<+{catalogItem_value|->status_value});
  Expanded_List_Substitution(Machine(CatalogItem),get_status)==(catalogItem_value: catalogItem | status_value:=status(catalogItem_value));
  List_Substitution(Machine(CatalogItem),get_status)==(status_value:=status(catalogItem_value));
  List_Substitution(Machine(CatalogItem),set_student_id)==(status(catalogItem_value):=status_value);
  List_Substitution(Machine(CatalogItem),get_relatedStudent)==(relatedStudent_value:=relatedStudent(catalogItem_value));
  List_Substitution(Machine(CatalogItem),set_relatedStudent)==(relatedStudent(catalogItem_value):=relatedStudent_value);
  List_Substitution(Machine(CatalogItem),get_mark)==(mark_value:=mark(catalogItem_value));
  List_Substitution(Machine(CatalogItem),set_mark)==(mark(catalogItem_value):=mark_value)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(CatalogItem))==(?);
  Inherited_List_Constants(Machine(CatalogItem))==(?);
  List_Constants(Machine(CatalogItem))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(CatalogItem),STATUS)==({present,absent});
  Context_List_Enumerated(Machine(CatalogItem))==(?);
  Context_List_Defered(Machine(CatalogItem))==(STUDENT);
  Context_List_Sets(Machine(CatalogItem))==(STUDENT);
  List_Valuable_Sets(Machine(CatalogItem))==(CATALOGITEM);
  Inherited_List_Enumerated(Machine(CatalogItem))==(?);
  Inherited_List_Defered(Machine(CatalogItem))==(?);
  Inherited_List_Sets(Machine(CatalogItem))==(?);
  List_Enumerated(Machine(CatalogItem))==(STATUS);
  List_Defered(Machine(CatalogItem))==(CATALOGITEM);
  List_Sets(Machine(CatalogItem))==(STATUS,CATALOGITEM);
  Set_Definition(Machine(CatalogItem),CATALOGITEM)==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(CatalogItem))==(?);
  Expanded_List_HiddenConstants(Machine(CatalogItem))==(?);
  List_HiddenConstants(Machine(CatalogItem))==(?);
  External_List_HiddenConstants(Machine(CatalogItem))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(CatalogItem))==(btrue);
  Context_List_Properties(Machine(CatalogItem))==(STUDENT: FIN(INTEGER) & not(STUDENT = {}));
  Inherited_List_Properties(Machine(CatalogItem))==(btrue);
  List_Properties(Machine(CatalogItem))==(CATALOGITEM: FIN(INTEGER) & not(CATALOGITEM = {}) & STATUS: FIN(INTEGER) & not(STATUS = {}))
END
&
THEORY ListSeenInfoX END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(CatalogItem),get_status)==(?);
  List_ANY_Var(Machine(CatalogItem),set_student_id)==(?);
  List_ANY_Var(Machine(CatalogItem),get_relatedStudent)==(?);
  List_ANY_Var(Machine(CatalogItem),set_relatedStudent)==(?);
  List_ANY_Var(Machine(CatalogItem),get_mark)==(?);
  List_ANY_Var(Machine(CatalogItem),set_mark)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(CatalogItem)) == (STATUS,CATALOGITEM,present,absent | ? | mark,relatedStudent,status,catalogItem | ? | get_status,set_student_id,get_relatedStudent,set_relatedStudent,get_mark,set_mark | ? | used(Machine(Student)) | ? | CatalogItem);
  List_Of_HiddenCst_Ids(Machine(CatalogItem)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(CatalogItem)) == (?);
  List_Of_VisibleVar_Ids(Machine(CatalogItem)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(CatalogItem)) == (?: ?);
  List_Of_Ids(Machine(Student)) == (STUDENT | ? | student_group,student_name,student_id,student | ? | create_student,get_student_id,set_student_id,get_student_name,set_student_name,get_student_group,set_student_group | ? | seen(Machine(String)),seen(Machine(Group)),used(Machine(Lecture)) | ? | Student);
  List_Of_HiddenCst_Ids(Machine(Student)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Student)) == (?);
  List_Of_VisibleVar_Ids(Machine(Student)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Student)) == (?: ?);
  List_Of_Ids(Machine(Lecture)) == (LECTURE | ? | lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture | ? | create_lecture,delete_lecture,get_lecture_code,set_lecture_code,get_lecture_name,set_lecture_name,get_lecture_numberOfCredits,set_lecture_numberOfCredits,get_lecture_semester,set_lecture_semester,get_lecture_prerequisite,set_lecture_prerequisite | ? | seen(Machine(String)) | ? | Lecture);
  List_Of_HiddenCst_Ids(Machine(Lecture)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Lecture)) == (?);
  List_Of_VisibleVar_Ids(Machine(Lecture)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Lecture)) == (?: ?);
  List_Of_Ids(Machine(String)) == (STR | ? | ? | ? | ? | ? | ? | ? | String);
  List_Of_HiddenCst_Ids(Machine(String)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(String)) == (?);
  List_Of_VisibleVar_Ids(Machine(String)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(String)) == (?: ?);
  List_Of_Ids(Machine(Group)) == (GROUP | ? | ? | ? | ? | ? | ? | ? | Group);
  List_Of_HiddenCst_Ids(Machine(Group)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Group)) == (?);
  List_Of_VisibleVar_Ids(Machine(Group)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Group)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(CatalogItem)) == (Type(STATUS) == Cst(SetOf(etype(STATUS,0,1)));Type(CATALOGITEM) == Cst(SetOf(atype(CATALOGITEM,"[CATALOGITEM","]CATALOGITEM"))))
END
&
THEORY ConstantsEnvX IS
  Constants(Machine(CatalogItem)) == (Type(present) == Cst(etype(STATUS,0,1));Type(absent) == Cst(etype(STATUS,0,1)))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(CatalogItem)) == (Type(mark) == Mvl(SetOf(atype(CATALOGITEM,?,?)*btype(INTEGER,?,?)));Type(relatedStudent) == Mvl(SetOf(atype(CATALOGITEM,?,?)*atype(STUDENT,"[STUDENT","]STUDENT")));Type(status) == Mvl(SetOf(atype(CATALOGITEM,?,?)*etype(STATUS,0,1)));Type(catalogItem) == Mvl(SetOf(atype(CATALOGITEM,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(CatalogItem)) == (Type(set_mark) == Cst(No_type,atype(CATALOGITEM,?,?)*btype(INTEGER,?,?));Type(get_mark) == Cst(btype(INTEGER,?,?),atype(CATALOGITEM,?,?));Type(set_relatedStudent) == Cst(No_type,atype(CATALOGITEM,?,?)*atype(STUDENT,?,?));Type(get_relatedStudent) == Cst(atype(STUDENT,?,?),atype(CATALOGITEM,?,?));Type(set_student_id) == Cst(No_type,atype(CATALOGITEM,?,?)*etype(STATUS,?,?));Type(get_status) == Cst(etype(STATUS,?,?),atype(CATALOGITEM,?,?)));
  Observers(Machine(CatalogItem)) == (Type(get_mark) == Cst(btype(INTEGER,?,?),atype(CATALOGITEM,?,?));Type(get_relatedStudent) == Cst(atype(STUDENT,?,?),atype(CATALOGITEM,?,?));Type(get_status) == Cst(etype(STATUS,?,?),atype(CATALOGITEM,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
