﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Lecture))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Lecture))==(Machine(Lecture));
  Level(Machine(Lecture))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Lecture)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Lecture))==(String)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Lecture))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Lecture))==(?);
  List_Includes(Machine(Lecture))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Lecture))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Lecture))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Lecture))==(?);
  Context_List_Variables(Machine(Lecture))==(?);
  Abstract_List_Variables(Machine(Lecture))==(?);
  Local_List_Variables(Machine(Lecture))==(lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture);
  List_Variables(Machine(Lecture))==(lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture);
  External_List_Variables(Machine(Lecture))==(lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Lecture))==(?);
  Abstract_List_VisibleVariables(Machine(Lecture))==(?);
  External_List_VisibleVariables(Machine(Lecture))==(?);
  Expanded_List_VisibleVariables(Machine(Lecture))==(?);
  List_VisibleVariables(Machine(Lecture))==(?);
  Internal_List_VisibleVariables(Machine(Lecture))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Lecture))==(btrue);
  Gluing_List_Invariant(Machine(Lecture))==(btrue);
  Expanded_List_Invariant(Machine(Lecture))==(btrue);
  Abstract_List_Invariant(Machine(Lecture))==(btrue);
  Context_List_Invariant(Machine(Lecture))==(btrue);
  List_Invariant(Machine(Lecture))==(lecture <: LECTURE & lecture_code: lecture >-> NATURAL1 & lecture_name: lecture --> STR & lecture_numberOfCredits: lecture --> NATURAL1 & lecture_semester: lecture --> NATURAL1 & lecture_preRequisite: lecture --> POW(lecture))
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Lecture))==(btrue);
  Abstract_List_Assertions(Machine(Lecture))==(btrue);
  Context_List_Assertions(Machine(Lecture))==(btrue);
  List_Assertions(Machine(Lecture))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Lecture))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Lecture))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Lecture))==(lecture,lecture_code,lecture_name,lecture_numberOfCredits,lecture_semester,lecture_preRequisite:={},{},{},{},{},{});
  Context_List_Initialisation(Machine(Lecture))==(skip);
  List_Initialisation(Machine(Lecture))==(lecture:={} || lecture_code:={} || lecture_name:={} || lecture_numberOfCredits:={} || lecture_semester:={} || lecture_preRequisite:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Lecture))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Lecture),Machine(String))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Lecture))==(btrue);
  List_Constraints(Machine(Lecture))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Lecture))==(create_lecture,delete_lecture,get_lecture_code,set_lecture_code,get_lecture_name,set_lecture_name,get_lecture_numberOfCredits,set_lecture_numberOfCredits,get_lecture_semester,set_lecture_semester,get_lecture_prerequisite,set_lecture_prerequisite);
  List_Operations(Machine(Lecture))==(create_lecture,delete_lecture,get_lecture_code,set_lecture_code,get_lecture_name,set_lecture_name,get_lecture_numberOfCredits,set_lecture_numberOfCredits,get_lecture_semester,set_lecture_semester,get_lecture_prerequisite,set_lecture_prerequisite)
END
&
THEORY ListInputX IS
  List_Input(Machine(Lecture),create_lecture)==(code_value,name_value,numberOfCredits_value,semester_value,prerequisite_value);
  List_Input(Machine(Lecture),delete_lecture)==(lecture_value);
  List_Input(Machine(Lecture),get_lecture_code)==(lecture_value);
  List_Input(Machine(Lecture),set_lecture_code)==(lecture_value,code_value);
  List_Input(Machine(Lecture),get_lecture_name)==(lecture_value);
  List_Input(Machine(Lecture),set_lecture_name)==(lecture_value,name_value);
  List_Input(Machine(Lecture),get_lecture_numberOfCredits)==(lecture_value);
  List_Input(Machine(Lecture),set_lecture_numberOfCredits)==(lecture_value,numberOfCredits_value);
  List_Input(Machine(Lecture),get_lecture_semester)==(lecture_value);
  List_Input(Machine(Lecture),set_lecture_semester)==(lecture_value,semester_value);
  List_Input(Machine(Lecture),get_lecture_prerequisite)==(lecture_value);
  List_Input(Machine(Lecture),set_lecture_prerequisite)==(lecture_value,prerequisite_value)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Lecture),create_lecture)==(lecture_value);
  List_Output(Machine(Lecture),delete_lecture)==(?);
  List_Output(Machine(Lecture),get_lecture_code)==(code_value);
  List_Output(Machine(Lecture),set_lecture_code)==(?);
  List_Output(Machine(Lecture),get_lecture_name)==(name_value);
  List_Output(Machine(Lecture),set_lecture_name)==(?);
  List_Output(Machine(Lecture),get_lecture_numberOfCredits)==(numberOfCredits_value);
  List_Output(Machine(Lecture),set_lecture_numberOfCredits)==(?);
  List_Output(Machine(Lecture),get_lecture_semester)==(semester_value);
  List_Output(Machine(Lecture),set_lecture_semester)==(?);
  List_Output(Machine(Lecture),get_lecture_prerequisite)==(prerequisite_value);
  List_Output(Machine(Lecture),set_lecture_prerequisite)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Lecture),create_lecture)==(lecture_value <-- create_lecture(code_value,name_value,numberOfCredits_value,semester_value,prerequisite_value));
  List_Header(Machine(Lecture),delete_lecture)==(delete_lecture(lecture_value));
  List_Header(Machine(Lecture),get_lecture_code)==(code_value <-- get_lecture_code(lecture_value));
  List_Header(Machine(Lecture),set_lecture_code)==(set_lecture_code(lecture_value,code_value));
  List_Header(Machine(Lecture),get_lecture_name)==(name_value <-- get_lecture_name(lecture_value));
  List_Header(Machine(Lecture),set_lecture_name)==(set_lecture_name(lecture_value,name_value));
  List_Header(Machine(Lecture),get_lecture_numberOfCredits)==(numberOfCredits_value <-- get_lecture_numberOfCredits(lecture_value));
  List_Header(Machine(Lecture),set_lecture_numberOfCredits)==(set_lecture_numberOfCredits(lecture_value,numberOfCredits_value));
  List_Header(Machine(Lecture),get_lecture_semester)==(semester_value <-- get_lecture_semester(lecture_value));
  List_Header(Machine(Lecture),set_lecture_semester)==(set_lecture_semester(lecture_value,semester_value));
  List_Header(Machine(Lecture),get_lecture_prerequisite)==(prerequisite_value <-- get_lecture_prerequisite(lecture_value));
  List_Header(Machine(Lecture),set_lecture_prerequisite)==(set_lecture_prerequisite(lecture_value,prerequisite_value))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Lecture),create_lecture)==(code_value: NATURAL1 & code_value/:ran(lecture_code) & name_value: STR & numberOfCredits_value: NATURAL1 & semester_value: NATURAL1 & prerequisite_value: POW(lecture));
  List_Precondition(Machine(Lecture),delete_lecture)==(lecture_value: lecture);
  List_Precondition(Machine(Lecture),get_lecture_code)==(lecture_value: lecture);
  List_Precondition(Machine(Lecture),set_lecture_code)==(lecture_value: lecture & code_value: NATURAL1 & code_value/:ran(lecture_code));
  List_Precondition(Machine(Lecture),get_lecture_name)==(lecture_value: lecture);
  List_Precondition(Machine(Lecture),set_lecture_name)==(lecture_value: lecture & name_value: STR);
  List_Precondition(Machine(Lecture),get_lecture_numberOfCredits)==(lecture_value: lecture);
  List_Precondition(Machine(Lecture),set_lecture_numberOfCredits)==(lecture_value: lecture & numberOfCredits_value: NATURAL1);
  List_Precondition(Machine(Lecture),get_lecture_semester)==(lecture_value: lecture);
  List_Precondition(Machine(Lecture),set_lecture_semester)==(lecture_value: lecture & semester_value: NATURAL1);
  List_Precondition(Machine(Lecture),get_lecture_prerequisite)==(lecture_value: lecture);
  List_Precondition(Machine(Lecture),set_lecture_prerequisite)==(lecture_value: lecture & prerequisite_value: POW(lecture))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Lecture),set_lecture_prerequisite)==(lecture_value: lecture & prerequisite_value: POW(lecture) | lecture_preRequisite:=lecture_preRequisite<+{lecture_value|->prerequisite_value});
  Expanded_List_Substitution(Machine(Lecture),get_lecture_prerequisite)==(lecture_value: lecture | prerequisite_value:=lecture_preRequisite(lecture_value));
  Expanded_List_Substitution(Machine(Lecture),set_lecture_semester)==(lecture_value: lecture & semester_value: NATURAL1 | lecture_semester:=lecture_semester<+{lecture_value|->semester_value});
  Expanded_List_Substitution(Machine(Lecture),get_lecture_semester)==(lecture_value: lecture | semester_value:=lecture_semester(lecture_value));
  Expanded_List_Substitution(Machine(Lecture),set_lecture_numberOfCredits)==(lecture_value: lecture & numberOfCredits_value: NATURAL1 | lecture_numberOfCredits:=lecture_numberOfCredits<+{lecture_value|->numberOfCredits_value});
  Expanded_List_Substitution(Machine(Lecture),get_lecture_numberOfCredits)==(lecture_value: lecture | numberOfCredits_value:=lecture_numberOfCredits(lecture_value));
  Expanded_List_Substitution(Machine(Lecture),set_lecture_name)==(lecture_value: lecture & name_value: STR | lecture_name:=lecture_name<+{lecture_value|->name_value});
  Expanded_List_Substitution(Machine(Lecture),get_lecture_name)==(lecture_value: lecture | name_value:=lecture_name(lecture_value));
  Expanded_List_Substitution(Machine(Lecture),set_lecture_code)==(lecture_value: lecture & code_value: NATURAL1 & code_value/:ran(lecture_code) | lecture_code:=lecture_code<+{lecture_value|->code_value});
  Expanded_List_Substitution(Machine(Lecture),get_lecture_code)==(lecture_value: lecture | code_value:=lecture_code(lecture_value));
  Expanded_List_Substitution(Machine(Lecture),delete_lecture)==(lecture_value: lecture | lecture,lecture_name,lecture_code,lecture_numberOfCredits,lecture_semester,lecture_preRequisite:=lecture-{lecture_value},{lecture_value}<<|lecture_name,{lecture_value}<<|lecture_code,{lecture_value}<<|lecture_numberOfCredits,{lecture_value}<<|lecture_semester,{lecture_value}<<|lecture_preRequisite);
  Expanded_List_Substitution(Machine(Lecture),create_lecture)==(code_value: NATURAL1 & code_value/:ran(lecture_code) & name_value: STR & numberOfCredits_value: NATURAL1 & semester_value: NATURAL1 & prerequisite_value: POW(lecture) | @lect.(lect: LECTURE-lecture ==> lecture,lecture_code,lecture_name,lecture_numberOfCredits,lecture_semester,lecture_preRequisite,lecture_value:=lecture\/{lect},lecture_code<+{lect|->code_value},lecture_name<+{lect|->name_value},lecture_numberOfCredits<+{lect|->numberOfCredits_value},lecture_semester<+{lect|->semester_value},lecture_preRequisite<+{lect|->prerequisite_value},lect));
  List_Substitution(Machine(Lecture),create_lecture)==(ANY lect WHERE lect: LECTURE-lecture THEN lecture:=lecture\/{lect} || lecture_code(lect):=code_value || lecture_name(lect):=name_value || lecture_numberOfCredits(lect):=numberOfCredits_value || lecture_semester(lect):=semester_value || lecture_preRequisite(lect):=prerequisite_value || lecture_value:=lect END);
  List_Substitution(Machine(Lecture),delete_lecture)==(lecture:=lecture-{lecture_value} || lecture_name:={lecture_value}<<|lecture_name || lecture_code:={lecture_value}<<|lecture_code || lecture_numberOfCredits:={lecture_value}<<|lecture_numberOfCredits || lecture_semester:={lecture_value}<<|lecture_semester || lecture_preRequisite:={lecture_value}<<|lecture_preRequisite);
  List_Substitution(Machine(Lecture),get_lecture_code)==(code_value:=lecture_code(lecture_value));
  List_Substitution(Machine(Lecture),set_lecture_code)==(lecture_code(lecture_value):=code_value);
  List_Substitution(Machine(Lecture),get_lecture_name)==(name_value:=lecture_name(lecture_value));
  List_Substitution(Machine(Lecture),set_lecture_name)==(lecture_name(lecture_value):=name_value);
  List_Substitution(Machine(Lecture),get_lecture_numberOfCredits)==(numberOfCredits_value:=lecture_numberOfCredits(lecture_value));
  List_Substitution(Machine(Lecture),set_lecture_numberOfCredits)==(lecture_numberOfCredits(lecture_value):=numberOfCredits_value);
  List_Substitution(Machine(Lecture),get_lecture_semester)==(semester_value:=lecture_semester(lecture_value));
  List_Substitution(Machine(Lecture),set_lecture_semester)==(lecture_semester(lecture_value):=semester_value);
  List_Substitution(Machine(Lecture),get_lecture_prerequisite)==(prerequisite_value:=lecture_preRequisite(lecture_value));
  List_Substitution(Machine(Lecture),set_lecture_prerequisite)==(lecture_preRequisite(lecture_value):=prerequisite_value)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Lecture))==(?);
  Inherited_List_Constants(Machine(Lecture))==(?);
  List_Constants(Machine(Lecture))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(Lecture),LECTURE)==(?);
  Context_List_Enumerated(Machine(Lecture))==(?);
  Context_List_Defered(Machine(Lecture))==(STR);
  Context_List_Sets(Machine(Lecture))==(STR);
  List_Valuable_Sets(Machine(Lecture))==(LECTURE);
  Inherited_List_Enumerated(Machine(Lecture))==(?);
  Inherited_List_Defered(Machine(Lecture))==(?);
  Inherited_List_Sets(Machine(Lecture))==(?);
  List_Enumerated(Machine(Lecture))==(?);
  List_Defered(Machine(Lecture))==(LECTURE);
  List_Sets(Machine(Lecture))==(LECTURE)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Lecture))==(?);
  Expanded_List_HiddenConstants(Machine(Lecture))==(?);
  List_HiddenConstants(Machine(Lecture))==(?);
  External_List_HiddenConstants(Machine(Lecture))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Lecture))==(btrue);
  Context_List_Properties(Machine(Lecture))==(STR: FIN(INTEGER) & not(STR = {}));
  Inherited_List_Properties(Machine(Lecture))==(btrue);
  List_Properties(Machine(Lecture))==(LECTURE: FIN(INTEGER) & not(LECTURE = {}))
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Lecture),Machine(String))==(?);
  Seen_Context_List_Enumerated(Machine(Lecture))==(?);
  Seen_Context_List_Invariant(Machine(Lecture))==(btrue);
  Seen_Context_List_Assertions(Machine(Lecture))==(btrue);
  Seen_Context_List_Properties(Machine(Lecture))==(btrue);
  Seen_List_Constraints(Machine(Lecture))==(btrue);
  Seen_List_Operations(Machine(Lecture),Machine(String))==(?);
  Seen_Expanded_List_Invariant(Machine(Lecture),Machine(String))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Lecture),create_lecture)==(Var(lect) == atype(LECTURE,?,?));
  List_ANY_Var(Machine(Lecture),delete_lecture)==(?);
  List_ANY_Var(Machine(Lecture),get_lecture_code)==(?);
  List_ANY_Var(Machine(Lecture),set_lecture_code)==(?);
  List_ANY_Var(Machine(Lecture),get_lecture_name)==(?);
  List_ANY_Var(Machine(Lecture),set_lecture_name)==(?);
  List_ANY_Var(Machine(Lecture),get_lecture_numberOfCredits)==(?);
  List_ANY_Var(Machine(Lecture),set_lecture_numberOfCredits)==(?);
  List_ANY_Var(Machine(Lecture),get_lecture_semester)==(?);
  List_ANY_Var(Machine(Lecture),set_lecture_semester)==(?);
  List_ANY_Var(Machine(Lecture),get_lecture_prerequisite)==(?);
  List_ANY_Var(Machine(Lecture),set_lecture_prerequisite)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Lecture)) == (LECTURE | ? | lecture_preRequisite,lecture_semester,lecture_numberOfCredits,lecture_name,lecture_code,lecture | ? | create_lecture,delete_lecture,get_lecture_code,set_lecture_code,get_lecture_name,set_lecture_name,get_lecture_numberOfCredits,set_lecture_numberOfCredits,get_lecture_semester,set_lecture_semester,get_lecture_prerequisite,set_lecture_prerequisite | ? | seen(Machine(String)) | ? | Lecture);
  List_Of_HiddenCst_Ids(Machine(Lecture)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Lecture)) == (?);
  List_Of_VisibleVar_Ids(Machine(Lecture)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Lecture)) == (?: ?);
  List_Of_Ids(Machine(String)) == (STR | ? | ? | ? | ? | ? | ? | ? | String);
  List_Of_HiddenCst_Ids(Machine(String)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(String)) == (?);
  List_Of_VisibleVar_Ids(Machine(String)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(String)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(Lecture)) == (Type(LECTURE) == Cst(SetOf(atype(LECTURE,"[LECTURE","]LECTURE"))))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Lecture)) == (Type(lecture_preRequisite) == Mvl(SetOf(atype(LECTURE,?,?)*SetOf(atype(LECTURE,?,?))));Type(lecture_semester) == Mvl(SetOf(atype(LECTURE,?,?)*btype(INTEGER,?,?)));Type(lecture_numberOfCredits) == Mvl(SetOf(atype(LECTURE,?,?)*btype(INTEGER,?,?)));Type(lecture_name) == Mvl(SetOf(atype(LECTURE,?,?)*atype(STR,"[STR","]STR")));Type(lecture_code) == Mvl(SetOf(atype(LECTURE,?,?)*btype(INTEGER,?,?)));Type(lecture) == Mvl(SetOf(atype(LECTURE,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Lecture)) == (Type(set_lecture_prerequisite) == Cst(No_type,atype(LECTURE,?,?)*SetOf(atype(LECTURE,?,?)));Type(get_lecture_prerequisite) == Cst(SetOf(atype(LECTURE,?,?)),atype(LECTURE,?,?));Type(set_lecture_semester) == Cst(No_type,atype(LECTURE,?,?)*btype(INTEGER,?,?));Type(get_lecture_semester) == Cst(btype(INTEGER,?,?),atype(LECTURE,?,?));Type(set_lecture_numberOfCredits) == Cst(No_type,atype(LECTURE,?,?)*btype(INTEGER,?,?));Type(get_lecture_numberOfCredits) == Cst(btype(INTEGER,?,?),atype(LECTURE,?,?));Type(set_lecture_name) == Cst(No_type,atype(LECTURE,?,?)*atype(STR,?,?));Type(get_lecture_name) == Cst(atype(STR,?,?),atype(LECTURE,?,?));Type(set_lecture_code) == Cst(No_type,atype(LECTURE,?,?)*btype(INTEGER,?,?));Type(get_lecture_code) == Cst(btype(INTEGER,?,?),atype(LECTURE,?,?));Type(delete_lecture) == Cst(No_type,atype(LECTURE,?,?));Type(create_lecture) == Cst(atype(LECTURE,?,?),btype(INTEGER,?,?)*atype(STR,?,?)*btype(INTEGER,?,?)*btype(INTEGER,?,?)*SetOf(atype(LECTURE,?,?))));
  Observers(Machine(Lecture)) == (Type(get_lecture_prerequisite) == Cst(SetOf(atype(LECTURE,?,?)),atype(LECTURE,?,?));Type(get_lecture_semester) == Cst(btype(INTEGER,?,?),atype(LECTURE,?,?));Type(get_lecture_numberOfCredits) == Cst(btype(INTEGER,?,?),atype(LECTURE,?,?));Type(get_lecture_name) == Cst(atype(STR,?,?),atype(LECTURE,?,?));Type(get_lecture_code) == Cst(btype(INTEGER,?,?),atype(LECTURE,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
