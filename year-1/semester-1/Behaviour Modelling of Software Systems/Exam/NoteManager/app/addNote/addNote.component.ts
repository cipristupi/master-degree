import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, NoteService } from '../_services/index';


@Component({
    moduleId: module.id,
    templateUrl: 'addNote.component.html'
})

export class AddNoteComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private NoteService: NoteService,
        private alertService: AlertService) { }

    addNote() {
        this.loading = true;
        this.NoteService.create(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Add successful', true);
                    this.router.navigate(['/home']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
