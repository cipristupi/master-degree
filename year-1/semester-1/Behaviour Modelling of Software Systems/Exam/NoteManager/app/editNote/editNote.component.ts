import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


import { AlertService, NoteService } from '../_services/index';


@Component({
    moduleId: module.id,
    templateUrl: 'editNote.component.html'
})

export class EditNoteComponent implements OnInit {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private NoteService: NoteService,
        private alertService: AlertService,
        private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.loadNote();
    }

    editNote() {
        this.loading = true;
        this.NoteService.update(this.model)
            .subscribe(
            data => {
                this.alertService.success('Update successful', true);
                this.router.navigate(['/home']);
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
    }

    loadNote() {
        var id = this.activatedRoute.snapshot.params['id'];
        this.model = this.NoteService.getById(id);
    }
}
