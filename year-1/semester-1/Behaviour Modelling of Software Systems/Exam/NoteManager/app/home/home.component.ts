﻿import { Component, OnInit, Input } from '@angular/core';

import { Note } from '../_models/index';
import { NoteService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    Notes: Note[] = [];
    activityInfo: string;
    loadingFailed: boolean;
    Progress: string;
    constructor(private NoteService: NoteService) {
    }

    ngOnInit() {
        this.loadAllNotes();
    }
    loadAgain() {
        this.loadAllNotes();
    }
    private loadAllNotes() {
        this.activityInfo = 'Loading...';
        if (this.NoteService.getFromLocalStorage() != undefined) {
            this.activityInfo = '';
            var result = this.NoteService.getFromLocalStorage();
            this.setSortedNotes(result);
            this.NoteService.getWithLastUpdate().subscribe(Notes => {
                this.setSortedNotes(Notes);
                this.activityInfo = '';
            }, error => {
                this.activityInfo = 'Loading failed, retry';
                this.loadingFailed = true;
                console.log('Loading failed', error);
            });

        }
        else {
            this.NoteService.getAll().subscribe(Notes => {
                this.setSortedNotes(Notes);
                this.activityInfo = '';
            }, error => {
                this.activityInfo = 'Loading failed, retry';
                this.loadingFailed = true;
                console.log('Loading failed', error);
            });
        }

    }

    private setSortedNotes(notes) {
        var sorted = notes.sort((n1, n2) => {
            if (n1.updated > n2.updated) {
                return 1;
            }

            if (n1.updated < n2.updated) {
                return -1;
            }

            return 0;
        })
        this.loadingFailed = false;
        this.Notes = sorted;
    }


}