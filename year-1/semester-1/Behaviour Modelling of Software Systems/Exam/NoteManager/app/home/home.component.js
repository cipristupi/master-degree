"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var index_1 = require("../_services/index");
var HomeComponent = (function () {
    function HomeComponent(NoteService) {
        this.NoteService = NoteService;
        this.Notes = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.loadAllNotes();
    };
    HomeComponent.prototype.loadAgain = function () {
        this.loadAllNotes();
    };
    HomeComponent.prototype.loadAllNotes = function () {
        var _this = this;
        this.activityInfo = 'Loading...';
        if (this.NoteService.getFromLocalStorage() != undefined) {
            this.activityInfo = '';
            var result = this.NoteService.getFromLocalStorage();
            this.setSortedNotes(result);
            this.NoteService.getWithLastUpdate().subscribe(function (Notes) {
                _this.setSortedNotes(Notes);
                _this.activityInfo = '';
            }, function (error) {
                _this.activityInfo = 'Loading failed, retry';
                _this.loadingFailed = true;
                console.log('Loading failed', error);
            });
        }
        else {
            this.NoteService.getAll().subscribe(function (Notes) {
                _this.setSortedNotes(Notes);
                _this.activityInfo = '';
            }, function (error) {
                _this.activityInfo = 'Loading failed, retry';
                _this.loadingFailed = true;
                console.log('Loading failed', error);
            });
        }
    };
    HomeComponent.prototype.setSortedNotes = function (notes) {
        var sorted = notes.sort(function (n1, n2) {
            if (n1.updated > n2.updated) {
                return 1;
            }
            if (n1.updated < n2.updated) {
                return -1;
            }
            return 0;
        });
        this.loadingFailed = false;
        this.Notes = sorted;
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'home.component.html'
    }),
    __metadata("design:paramtypes", [index_1.NoteService])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map