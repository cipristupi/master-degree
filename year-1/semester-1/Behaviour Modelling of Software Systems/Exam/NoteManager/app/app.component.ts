﻿import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs/Observable";


@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
})

export class AppComponent implements OnInit {
    // An internal "copy" of the connection state stream used because
    //  we want to map the values of the original stream. If we didn't 
    //  need to do that then we could use the service's observable 
    //  right in the template.
    //   
    connectionState$: Observable<string>;

    constructor(
    ) {
    }

    ngOnInit() {

    }
}