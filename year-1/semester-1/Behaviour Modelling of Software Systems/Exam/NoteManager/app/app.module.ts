﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { BaseRequestOptions } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './_directives/index';
import { AlertService, NoteService } from './_services/index';
import { HomeComponent } from './home/index';
import { AddNoteComponent } from './addNote/index';
import { EditNoteComponent } from './editNote/index';
import { AppSettings } from './_helpers/index'

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        AddNoteComponent,
        EditNoteComponent
    ],
    providers: [
        AlertService,
        NoteService,
        BaseRequestOptions
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }