﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';

// import { AuthGuard } from './_guards/index';
import { AddNoteComponent } from './addNote/index'
import { EditNoteComponent } from './editNote/index'

const appRoutes: Routes = [
    { path: '', component: HomeComponent},
    // { path: 'login', component: LoginComponent },
    // { path: 'register', component: RegisterComponent },
    { path: 'addNote', component: AddNoteComponent },
    { path: 'editNote/:id', component: EditNoteComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);