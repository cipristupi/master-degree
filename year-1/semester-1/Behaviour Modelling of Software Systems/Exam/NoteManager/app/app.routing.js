"use strict";
var router_1 = require("@angular/router");
var index_1 = require("./home/index");
// import { AuthGuard } from './_guards/index';
var index_2 = require("./addNote/index");
var index_3 = require("./editNote/index");
var appRoutes = [
    { path: '', component: index_1.HomeComponent },
    // { path: 'login', component: LoginComponent },
    // { path: 'register', component: RegisterComponent },
    { path: 'addNote', component: index_2.AddNoteComponent },
    { path: 'editNote/:id', component: index_3.EditNoteComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map