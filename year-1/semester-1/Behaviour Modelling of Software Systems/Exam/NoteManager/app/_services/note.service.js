"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var index_1 = require("../_helpers/index");
require("rxjs/add/operator/map");
var NoteService = (function () {
    function NoteService(http) {
        this.http = http;
    }
    NoteService.prototype.getAll = function () {
        var _this = this;
        var result = this.http.get(index_1.AppSettings.API_ENDPOINT + '/note').map(function (response) { return response.json(); });
        result.subscribe(function (Notes) {
            _this.updateLocalStorage(Notes);
        }, function (error) {
            console.log('Loading failed', error);
        });
        return result;
    };
    NoteService.prototype.getWithLastUpdate = function () {
        var _this = this;
        var lastUpdateLocalStorage = this.getLastUpdate();
        var result = this.http.get(index_1.AppSettings.API_ENDPOINT + '/note?lastUpdated' + lastUpdateLocalStorage).map(function (response) { return response.json(); });
        result.subscribe(function (Notes) {
            _this.updateLocalStorage(Notes);
        }, function (error) {
            console.log('Loading failed', error);
        });
        return result;
    };
    NoteService.prototype.getById = function (id) {
        return this.getNoteById(id);
    };
    NoteService.prototype.create = function (Note) {
        return this.http.post(index_1.AppSettings.API_ENDPOINT + '/api/Note/add', Note).map(function (response) { return response.json(); });
    };
    NoteService.prototype.update = function (Note) {
        return this.http.put(index_1.AppSettings.API_ENDPOINT + '/note/' + Note.id, Note).map(function (response) { return response.json(); });
    };
    NoteService.prototype.delete = function (id) {
        var url = index_1.AppSettings.API_ENDPOINT + '/api/Note/deleteNote/' + id;
        return this.http.post(url, null).map(function (response) { return response.json(); });
    };
    NoteService.prototype.updateLocalStorage = function (values) {
        localStorage.setItem('noteList', JSON.stringify(values));
    };
    NoteService.prototype.getFromLocalStorage = function () {
        return JSON.parse(localStorage.getItem('noteList'));
    };
    NoteService.prototype.getLastUpdate = function () {
        var sorted = this.getFromLocalStorage().sort(function (n1, n2) {
            if (n1.updated < n2.updated) {
                return 1;
            }
            if (n1.updated > n2.updated) {
                return -1;
            }
            return 0;
        });
        if (sorted.length > 0) {
            return sorted[0].Updated;
        }
    };
    NoteService.prototype.getNoteById = function (id) {
        var _this = this;
        var localStorageNote = null;
        if (this.getFromLocalStorage() != undefined) {
            for (var i = 0, len = this.getFromLocalStorage().length; i < len; i++) {
                var value = this.getFromLocalStorage()[i];
                if (value.id == id) {
                    return value;
                }
            }
        }
        else {
            this.getAll().subscribe(function (Notes) {
                for (var i = 0, len = Notes.length; i < len; i++) {
                    var value = _this.getFromLocalStorage()[i];
                    if (value.id == id) {
                        return value;
                    }
                }
            }, function (error) {
                return undefined;
            });
        }
        return undefined;
    };
    return NoteService;
}());
NoteService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], NoteService);
exports.NoteService = NoteService;
//# sourceMappingURL=note.service.js.map