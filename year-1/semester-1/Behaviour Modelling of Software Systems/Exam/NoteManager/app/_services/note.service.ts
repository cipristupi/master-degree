import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Note } from '../_models/index';
import { AppSettings } from '../_helpers/index'
import 'rxjs/add/operator/map'
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";

@Injectable()
export class NoteService {
    constructor(private http: Http) { }

    getAll() {
        var result = this.http.get(AppSettings.API_ENDPOINT + '/note').map((response: Response) => response.json())
        result.subscribe(Notes => {
            this.updateLocalStorage(Notes);
        }, error => {
            console.log('Loading failed', error);
        });
        return result;
    }

    getWithLastUpdate() {
        var lastUpdateLocalStorage = this.getLastUpdate();
        var result = this.http.get(AppSettings.API_ENDPOINT + '/note?lastUpdated'+lastUpdateLocalStorage).map((response: Response) => response.json())
        result.subscribe(Notes => {
            this.updateLocalStorage(Notes);
        }, error => {
            console.log('Loading failed', error);
        });
        return result;
    }

    getById(id: number) {

        return this.getNoteById(id);
    }

    create(Note: Note) {
        return this.http.post(AppSettings.API_ENDPOINT + '/api/Note/add', Note).map((response: Response) => response.json());
    }

    update(Note: Note) {
        return this.http.put(AppSettings.API_ENDPOINT + '/note/'+Note.id, Note).map((response: Response) => response.json());
    }

    delete(id: number) {
        var url = AppSettings.API_ENDPOINT + '/api/Note/deleteNote/' + id;
        return this.http.post(url, null).map((response: Response) => response.json());
    }

    updateLocalStorage(values) {
        localStorage.setItem('noteList', JSON.stringify(values));
    }

    getFromLocalStorage() {
        return JSON.parse(localStorage.getItem('noteList'));
    }

    getLastUpdate() {
        var sorted = this.getFromLocalStorage().sort((n1, n2) => {
            if (n1.updated < n2.updated) {
                return 1;
            }

            if (n1.updated > n2.updated) {
                return -1;
            }

            return 0;
        })
        if (sorted.length > 0) {
            return sorted[0].Updated;
        }
    }

    private getNoteById(id: number) {
        var localStorageNote = null;
        if (this.getFromLocalStorage() != undefined) {
            for (var i = 0, len = this.getFromLocalStorage().length; i < len; i++) {
                var value = this.getFromLocalStorage()[i];
                if (value.id == id) {
                    return value;
                }
            }
        } else {
            this.getAll().subscribe(Notes => {
                for (var i = 0, len = Notes.length; i < len; i++) {
                    var value = this.getFromLocalStorage()[i];
                    if (value.id == id) {
                        return value;
                    }
                }
            }, error => {
              return undefined
            });
        }
        return undefined;
    }

    
}