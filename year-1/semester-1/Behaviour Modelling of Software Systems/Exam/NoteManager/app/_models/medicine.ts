export class Note {
    id: number;
    name: string;
    addedDate: string;
    description: string;
    userId: string;
}