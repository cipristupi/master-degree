export class Note {
    id: string;
    text: string;
    updated: number;
    version: number;
}