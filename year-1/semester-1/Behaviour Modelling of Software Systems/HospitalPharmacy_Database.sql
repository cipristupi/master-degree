USE [master]
GO
/****** Object:  Database [HospitalPharmacy]    Script Date: 1/31/2017 11:01:37 PM ******/
CREATE DATABASE [HospitalPharmacy]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'HospitalPharmacy', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\HospitalPharmacy.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'HospitalPharmacy_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\HospitalPharmacy_log.ldf' , SIZE = 1856KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HospitalPharmacy].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HospitalPharmacy] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET ARITHABORT OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HospitalPharmacy] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HospitalPharmacy] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET  ENABLE_BROKER 
GO
ALTER DATABASE [HospitalPharmacy] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HospitalPharmacy] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [HospitalPharmacy] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET RECOVERY FULL 
GO
ALTER DATABASE [HospitalPharmacy] SET  MULTI_USER 
GO
ALTER DATABASE [HospitalPharmacy] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HospitalPharmacy] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HospitalPharmacy] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HospitalPharmacy] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'HospitalPharmacy', N'ON'
GO
USE [HospitalPharmacy]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 1/31/2017 11:01:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 1/31/2017 11:01:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 1/31/2017 11:01:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 1/31/2017 11:01:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 1/31/2017 11:01:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 1/31/2017 11:01:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Medicines]    Script Date: 1/31/2017 11:01:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medicines](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[Description] [text] NULL,
	[AddedDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUserId] [nvarchar](128) NULL,
 CONSTRAINT [PK_Medicines] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201701071530080_InitialCreate', N'HospitalPharmacy.AuthContext', 0x1F8B0800000000000400DD5CDD6EE33616BE2FB0EF20E86A5BA4567E7606B3813D45EA249D60273F1867067B37A025DA2146A254914A132CF6C97AD147EA2B94942859A448999215DB29061844FCF9CEE1E1217978F4C97FFEFEC7F8A7A728741E614A508C27EED1E8D07520F6E300E1E5C4CDE8E2C777EE4FEFFFF1DDF822889E9C2F65BB13DE8EF5C464E23E509A9C7A1EF11F6004C828427E1A937841477E1C792088BDE3C3C37F7B47471E64102EC3729CF1A70C5314C1FC813D4E63ECC3846620BC8E03181251CE6A6639AA7303224812E0C389FB212609A220BC7B006904FC67D7390B11607ACC60B8701D80714C01655A9E7E267046D3182F67092B00E1FD730259BB05080914DA9FAE9ADB0EE4F0980FC45B752CA1FC8CD038EA087874222CE3A9DD7BD9D7AD2CC76C77C16C4C9FF9A873FB4DDCAB00E6459FE290194015783A0D53DE78E25E5722CE487203E9A8EC382A202F5306F75B9C7E1BD5110F1CEB7E0795271D8F0EF9BF03679A85344BE104C38CA6203C70EEB27988FCFFC0E7FBF81BC49393A3F9E2E4DD9BB7203879FB2F78F2A63E523656D64E2A604577699CC094E90617D5F85DC793FB796AC7AA5BAD4F6115E64B6C51B8CE3578FA08F1923EB0E572FCCE752ED1130CCA12E15C9F31626B8875A269C61E6FB23004F31056F55EAB4CFE7F8BD4E3376F07917A031ED1329F7A453E5B3829719D4F30CC6BC9034A8AE525CDF757D1EC328D23FE2CFB5751FB751667A9CF07131B9BDC837409A9ACDDD85B39AF954B73A8E1DDBA44DD7FD7E69A36DD5BDB940FA8CF4A28456C7B3594FABEACDC5E1E37BCB7EDBFA7BD964DF42202281C6017B590C2C297054A23588DF2E798F92CC09D75BE0384B0A90D3E00F2D0A23AFB7300D567D0CF52E650330AA2E4C5A5DD3DC418DE64D19CAF9AEDC91A6C6AEE7F8B2F814FE3F402F35E1BE37D8CFD6F71462F70700E28FC4CFD12903FDEA3C81E601075CE7C1F1272C99C1906D39845E725E015A627C79DE1F856B6EB28661A0214B587315CCDAF65BB661C53AB360632F536BA48A64DC38FF112610B0DCB76060D8BEA760D459BAE1A72240B054533837E796DBB7A4593C122C17C3E863F9C73D8BFFB096D5AF03533CED836087F8118A66CAF0AEE00A530C5AB19B0D91C761111E4D3C785BEF801944BFA02C26C6851BD5643BEF6875F0D39ECFEAF865C4D56FC88021E7A585C90CAC60CDEAABDFEEEB57ECD299A6D7B3948C3DCB6F0EDEC01A6E5724648ECA37C1568526322B121EBCF0235677D96A3188D9A296103638E8E12E6DAAC848DCD559DEA169FC31052E89CF945EA700A880F82A619D980820E8A9527AA46B155C64456EE87864CE6E930E59D00BFE910B65211A6CD6581B08F1210AEB592D2D3F208E363AF64A835E73081980B5C6B091BE1FA040957A092A34CCA3A0B8DBD9AC7D939623D345D37E1DA38553FE35B76455D746CD04CC46C2FEA8C1A436DD11B35C6B0916E4CED6DDD1DC53DC46AD2D54BC9FEB8A3721532682682A6977747D950DB7647D918AFCB1D8B5BA7D59C2B57D0FD7146F9E2BBBB63BA69A56D7BA264893D73C4228A647D28EB01D3326796D1075E069FA8E676C5D413172C226255D52138E60C5239D5B20A58B581A4D70EA27A4D1BE0CAB3D6808AF77CAD409D342B336EAD88221EE8005BA6C95A61C5BEAEC0D6E6BD895D7FD9596B687E25AA3AA4D5DDA11A59E50A0DC7B60AF56B381A6F50F72979E01D8C22A54ECD563186B1D6816C6D3C620E2CECA28B3B0D8629C730AC654A5F5C63195D44651D53F5B78C1202192C538E6158CB08475C6318CDD96E7BBAF7378B7C180FB490CA1C44757A547563AF60378982B167A0418DAF419220BCACD1A24489332B3851D31F67DDE9425181E1F944C31AAAB4AD24D138054BA8D432D14CD34B94127A0E2898039E81990651A359FDAC34ECE8A5A4FA71D89CB8726B2F5BF3BF8B1E2A314C3E399B118540B864C38A784C9267B53593AEEFEE706E1A0841AA49A44FE3308BB0394432F72EDE99D5FB17254D84B1A7E8DF88821AB66A84A8B2E1ADA6A5B9120699A22A16E93F4D660893B1CB00B26E6E5350694629B345751453066967D3668A4B7A4ED586D3B4ADD5242820750051D411A3C6226880D5EAEC5165A2471D53AEB14754D81C7548A5AA839675CE86A464BDA2179EC1A2FA16F6129A2C8D3A7AB3D61E59C3D7A8436BAA7B606B7456EBEC5135948E3AB0A6DA1E7BC5EF5037CF3D3EAF8CD7909EBB607143DD6C2B3460BCCC7E38CC81577B8D5E07AA1577C4122FCA1B60A27C2F7DC97871EBE94B455A62335F326098771DE9F5B3BCE9B4BE3337634AEF94A58DBDED9DBA19AF9BC7BEA85F34EE716A934A7A759F53EE6D6371875AFF8D4BE3525534719DD28CEC507F26144623DE6034FB359C8608F22DBC6C700D305A40420B1E857B7C7874AC7C28B33F1FAD78840461872F57E439DB02250A3F82D467ABB64950D8E0C38E156823757C8503F83471FF97F73ACD3315FCAFBCF8C0B9229F31FA356315F769069DFF375995C313DDFBDA7DEB9F25D85BF5EABF5F8BAE07CE6DCA56CCA973A8D8B2CF0CCB1F2B74D2A6E8BA8136FD3E6178BDAB4962F86B5195D5D09FD03F477410327FA9E53F23F0F47D57D5B484FD8D1035A4FCA1F00631A18974DF07CB48B80FD823CD09F7DD06AB27E0F751CD48BE47B83B984ABDB7DF83CA9E3B3C6734D7A16D6C49B99DD7B29A37A238EEFA606A909F375AE84D827307B80D48CC3D3CE395F17F073B1D35F4DEC1B077E9DA2FCEE9DD171AEF8A9FB15BF6EEB69940D6325F254F779FD8678262B36342EEB6FDCB94B6DD67AA6307E6ED3E3998205BED9862BB6D0733E572F7D9C1ECB9B4FBE45FBB3E1E77E15DD6C7E3CEF9B14D6A90E1358B2EC9BB8E085B64C4D9ED7D1E330F28A2C5E203443D41CB246CE5294681AB2666A16666589BE0B5A4DA7681DD8489F3BC55A268D32ED6C0986C932DB6FA56D9A24DBB6C03277117FC5D2D435047A85EB37DB571995E015FD738006B33488E697843FE0AE8B99B1B425A2586D7BB7BCFC6DDDC0C432E8B0EECDBE6DB597622D67EA7909DCA042D5710FC570B31F4A5B3B06A7385177179242B1A954D949CCA35A4206007E5594AD102F89455F3AC70FE5D749E69E3EF26E630B8C2B7194D32CA860CA37928A5A8F8D1DE263FA718CB3A8F6F93FC973D8618025313F16CFA2DFE39436150E97DA9C9E2182078CC2072B07C2E29CFC52E9F2BA49B185B0209F355A1CE3D8C929081915B3C038FB08F6ECCFD3EC225F09F57393B13C8FA8990CD3E3E4760998288088C557FF6C87C38889EDEFF05DAD846DBAE530000, N'6.1.3-40302')
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName]) VALUES (N'2b486e38-6245-48a9-bab5-2563568b6771', NULL, 0, N'AMu4KHtiWxI9ZYewlExHNwSo7iPA0BQpm5guuv+pNLXz2jkmU/y8itDrX6M/CbveFQ==', N'3fb989b3-ce90-4b45-a388-abf4295f078b', NULL, 0, 0, NULL, 0, 0, N'cipristupi4', NULL, NULL)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName]) VALUES (N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', NULL, 0, N'APm4hU6i+0ug8jZo8tfKu+I//AuUv7HpqfbUpejPV2f9bfqeYoPVhxI/8Gmlk/BYag==', N'3ee1c914-9284-439a-8c31-1423a5a613a8', NULL, 0, 0, NULL, 0, 0, N'cipristupi', NULL, NULL)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName]) VALUES (N'b8ac647c-b7d8-4ad4-a4e5-274f1685cfd9', NULL, 0, N'AMpfBnK7uPQyPq+lhQ5NzUo8lqTg8Gx8alDckFahY67Bbg6GaexufTeGr84LkLH33w==', N'7dc926e7-2766-49be-aae1-1a00b4e0c9ea', NULL, 0, 0, NULL, 0, 0, N'test', NULL, NULL)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName]) VALUES (N'e374ab46-067d-433d-a560-b866530de8fa', NULL, 0, N'ABWYT7NIBqj2LzJePQTZ9dhp5/OBjuXnGGd1+3WylnQhM4JXzXN24yeQcF0qxmUNzQ==', N'f788fe02-606e-4f23-943a-1d1dfc67f9c2', NULL, 0, 0, NULL, 0, 0, N'cipristupi3', NULL, NULL)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName]) VALUES (N'e5bb7f3f-f7df-4b2b-911f-b6ea5f4d85a9', NULL, 0, N'AKx1PrAPAatv3ArPK+65b/BCBSZhlQSDYLyjmq2/jCx5do16kz+nhpbTL1w61pb5OA==', N'f51283dd-9af2-4588-a7e9-debb77c02e1d', NULL, 0, 0, NULL, 0, 0, N'cipristupi2', NULL, NULL)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName]) VALUES (N'ec39c27a-fe07-4f8a-9db3-1c625c0073d6', NULL, 0, N'AI+fmFcKxqTJdJxaFx5KFenCyg90eTqm18BeUp41oJ03CZIwU4Bym2S0snl539VlAw==', N'a2edbfd7-df2f-439b-8723-0cdc16a6351d', NULL, 0, 0, NULL, 0, 0, N'ana', NULL, NULL)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName]) VALUES (N'fc44f260-e25c-4514-8d44-02e74deee49c', NULL, 0, N'AP41ot2Mv0enZscjmbiGrDRknuLFX3l73u9tKWdinw5SvQqkZX/TScac+MB4UCaFPA==', N'40150e90-60cb-4f92-b4e9-f6ae6f73ef0b', NULL, 0, 0, NULL, 0, 0, N'ana1', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Medicines] ON 

GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (7, N'Rapfropupax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis e venit.', CAST(N'1962-04-01 14:38:52.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (8, N'Kliwerpedan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quorum nomen', CAST(N'1994-02-24 08:06:21.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (9, N'Supzapicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'rarendum pladior', CAST(N'1973-10-24 04:09:31.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (10, N'Suphupegin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare', CAST(N'1955-01-22 01:09:03.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (11, N'Winwerupower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1982-05-31 14:54:42.100' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (12, N'Retinonicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. quorum', CAST(N'1989-06-08 10:41:36.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (13, N'Endcadupantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens quo', CAST(N'1996-09-11 11:23:17.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (14, N'Truzapazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'homo, fecit,', CAST(N'1971-09-21 10:10:27.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (15, N'Raptinplin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam quo, Versus', CAST(N'1986-12-26 18:33:10.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (16, N'Hapmunackentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. si', CAST(N'1966-01-28 07:16:07.100' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (17, N'Emroban', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens quad', CAST(N'1993-08-09 03:03:04.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (18, N'Dopfropistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum quo ut', CAST(N'1991-11-16 19:59:39.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (19, N'Admunedex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. quad', CAST(N'2005-09-16 07:34:14.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (20, N'Qwizapadistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte Et', CAST(N'1953-07-05 13:31:20.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (21, N'Tipfropantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'egreddior', CAST(N'1971-06-21 03:02:03.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (22, N'Empebazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum plorum', CAST(N'1974-05-29 04:00:08.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (23, N'Qwiweropistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. et Id', CAST(N'1981-02-11 16:45:50.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (24, N'Suppebazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis nomen bono', CAST(N'1955-11-07 00:24:58.530' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (25, N'Grotanazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem.', CAST(N'1957-03-03 13:48:35.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (26, N'Zeezapazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum brevens,', CAST(N'1977-02-26 22:19:07.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (27, N'Klitanonin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e gravis et et', CAST(N'1990-10-21 16:50:53.130' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (28, N'Rerobexentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars novum quad', CAST(N'1991-04-26 12:41:43.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (29, N'Adrobax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens fecit.', CAST(N'1959-03-08 18:28:05.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (30, N'Parpebaquover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si Versus', CAST(N'1955-05-15 21:15:48.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (31, N'Rejubistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. si', CAST(N'1982-02-05 08:35:21.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (32, N'Winvenistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. pars', CAST(N'1955-09-22 00:39:26.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (33, N'Endpebover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. e Et', CAST(N'1996-11-23 05:59:44.100' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (34, N'Barhupover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum essit.', CAST(N'1961-05-09 23:47:36.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (35, N'Sursapower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sed manifestum', CAST(N'1998-02-28 12:42:07.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (36, N'Uptumegor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus vobis', CAST(N'1961-06-11 20:24:03.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (37, N'Growerupantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum sed et', CAST(N'1972-05-04 17:05:28.230' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (38, N'Barsapupantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis quis nomen', CAST(N'1994-04-22 17:38:55.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (39, N'Qwiquestadistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad quo', CAST(N'1956-12-29 05:35:15.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (40, N'Adpebopex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum delerium.', CAST(N'1991-03-13 18:45:49.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (41, N'Survenplex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit. parte', CAST(N'1959-07-07 13:51:41.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (42, N'Emtumin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non quad Multum', CAST(N'1956-08-03 06:36:05.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (43, N'Winerex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'manifestum Id', CAST(N'2000-12-30 17:18:18.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (44, N'Empebex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'habitatio nomen', CAST(N'2000-02-26 01:51:18.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (45, N'Endsipewex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si pladior Et Et', CAST(N'1990-02-18 06:21:01.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (46, N'Adsapadistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum quoque', CAST(N'1995-01-15 17:14:48.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (47, N'Frodiman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior. estis', CAST(N'1965-03-03 19:40:19.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (48, N'Lomtanonentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad egreddior', CAST(N'1982-04-16 16:24:54.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (49, N'Lomhupax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono e quad et', CAST(N'1995-11-12 19:22:37.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (50, N'Frozapar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum quantare', CAST(N'1969-06-20 08:59:13.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (51, N'Grotinewin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit. volcans', CAST(N'1964-06-06 08:32:37.230' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (52, N'Lomsipefistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e nomen quartu', CAST(N'1971-07-24 23:49:56.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (53, N'Lomhupin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, gravum', CAST(N'1993-09-08 07:59:17.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (54, N'Updimex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quad', CAST(N'1984-06-24 00:51:19.480' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (55, N'Klivenantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars Multum e', CAST(N'1982-05-20 20:49:32.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (56, N'Truglibexan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. non pars', CAST(N'1977-08-31 05:31:38.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (57, N'Parpickewower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad novum non', CAST(N'1992-10-20 07:48:20.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (58, N'Tiptumamicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, regit,', CAST(N'1967-12-09 15:36:28.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (59, N'Qwisapor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator non', CAST(N'1985-08-24 00:37:36.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (60, N'Inwerpaquicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis bono funem.', CAST(N'1997-05-08 14:34:10.110' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (61, N'Tupvenefan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro pars quad et', CAST(N'2000-07-24 19:50:55.900' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (62, N'Renipantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens e sed', CAST(N'1978-12-06 01:57:09.570' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (63, N'Unrobover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis et sed', CAST(N'1987-12-20 13:38:15.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (64, N'Undimantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, quo et', CAST(N'1971-06-12 06:53:43.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (65, N'Inrobentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non in fecundio,', CAST(N'1980-12-15 16:33:42.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (66, N'Zeepebor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans et non', CAST(N'1979-09-10 14:15:15.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (67, N'Ciptanin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, quad', CAST(N'2000-07-15 04:41:52.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (68, N'Emsipantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare fecit,', CAST(N'1988-05-05 11:35:40.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (69, N'Windimantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio,', CAST(N'1981-07-22 21:28:19.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (70, N'Varcadex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio, non', CAST(N'1978-07-17 22:09:06.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (71, N'Zeeeropor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono estum.', CAST(N'2001-09-24 14:10:38.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (72, N'Tipvenegentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, Sed', CAST(N'2007-04-18 09:24:08.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (73, N'Surrobistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut essit. et et', CAST(N'1987-11-19 14:24:23.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (74, N'Tupkilentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, Sed', CAST(N'1967-07-07 12:48:24.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (75, N'Parglibicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum', CAST(N'2001-12-28 23:39:38.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (76, N'Cipdudax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium.', CAST(N'1967-11-18 20:25:57.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (77, N'Varsipower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis egreddior', CAST(N'1981-05-28 10:41:50.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (78, N'Surdimax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, plurissimum', CAST(N'1988-12-16 14:54:32.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (79, N'Lomtumommazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et in in Tam Id', CAST(N'1973-09-17 18:03:57.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (80, N'Hapwerinover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et si eudis', CAST(N'1970-02-15 20:33:47.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (81, N'Zeeglibex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non Tam Tam', CAST(N'1964-12-27 02:38:47.570' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (82, N'Enddiman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e linguens quad', CAST(N'1961-07-08 00:25:58.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (83, N'Zeequestower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior', CAST(N'2003-06-13 17:19:05.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (84, N'Varglibower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu linguens', CAST(N'1997-09-17 09:28:52.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (85, N'Updimantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'volcans quo', CAST(N'1998-12-08 12:43:10.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (86, N'Upquestin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor quad', CAST(N'1963-07-16 08:45:24.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (87, N'Winzapimentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et quartu quo', CAST(N'1970-01-12 16:40:08.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (88, N'Monkilanistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum bono', CAST(N'1965-06-02 05:08:24.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (89, N'Trupebistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque parte', CAST(N'1986-12-20 02:58:04.270' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (90, N'Empebonar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum et', CAST(N'1973-07-01 07:10:29.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (91, N'Inkilower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si et delerium.', CAST(N'1976-08-21 12:35:42.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (92, N'Uptumefin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior in nomen', CAST(N'1957-05-28 01:29:00.130' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (93, N'Rappebex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans linguens', CAST(N'1961-08-30 17:01:19.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (94, N'Embanamower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans bono', CAST(N'1967-08-22 15:01:05.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (95, N'Raptanupover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen quoque et', CAST(N'1998-08-18 22:54:53.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (96, N'Zeemunefax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non pars', CAST(N'1988-07-31 11:54:31.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (97, N'Emtinedistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. quad bono', CAST(N'1976-08-13 16:57:49.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (98, N'Frosapackicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, eudis', CAST(N'1982-07-22 10:24:34.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (99, N'Endhupazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit. Versus', CAST(N'1972-03-13 09:17:36.400' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (100, N'Rapmunegin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono Multum', CAST(N'1963-06-11 16:05:45.520' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (101, N'Parquestilentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo regit,', CAST(N'2002-07-02 12:32:55.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (102, N'Klirobicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, pars', CAST(N'1984-10-28 08:04:39.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (103, N'Qwisapover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'rarendum plorum', CAST(N'1953-09-25 18:56:34.340' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (104, N'Endtumexan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen volcans', CAST(N'1970-04-28 21:13:31.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (105, N'Admunin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id fecit. plorum', CAST(N'1975-09-25 18:48:34.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (106, N'Supdimaquan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono e et in Sed', CAST(N'1965-02-17 23:08:28.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (107, N'Inmunan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono quad quis', CAST(N'1991-09-04 09:25:33.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (108, N'Monvenonin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars vobis', CAST(N'1979-03-05 19:10:53.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (109, N'Endtanazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum gravis', CAST(N'1990-06-26 01:11:40.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (110, N'Uprobantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio, ut', CAST(N'1967-07-04 00:23:34.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (111, N'Qwiquestazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis novum estis', CAST(N'2004-04-03 18:49:48.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (112, N'Sursipor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis esset', CAST(N'1956-11-04 16:53:48.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (113, N'Enddiman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum gravis', CAST(N'2002-03-22 21:32:30.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (114, N'Upwericator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1968-07-09 04:03:48.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (115, N'Thrumunedax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis non quad', CAST(N'1985-05-13 11:55:22.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (116, N'Adsapexor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in quoque', CAST(N'1992-01-18 19:24:56.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (117, N'Varfropor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1983-06-24 08:38:04.520' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (118, N'Lomkililistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non quorum', CAST(N'1998-04-03 04:09:23.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (119, N'Supbanefin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono quo, Tam', CAST(N'1980-07-03 18:10:32.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (120, N'Unglibopentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, Et novum', CAST(N'1970-06-03 07:48:17.340' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (121, N'Revenplin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non cognitio,', CAST(N'1962-08-29 21:35:07.130' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (122, N'Tupmunupex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis brevens,', CAST(N'1988-02-13 23:33:27.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (123, N'Lomwerilantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non linguens et', CAST(N'1954-05-18 08:37:36.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (124, N'Tuppebackentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo novum quo', CAST(N'1961-01-22 14:17:50.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (125, N'Froglibex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id in Et quis', CAST(N'1954-11-27 09:33:23.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (126, N'Adzapazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis volcans in', CAST(N'1997-04-30 12:41:38.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (127, N'Supquestommazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum esset', CAST(N'1998-08-19 16:22:49.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (128, N'Unwerefar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et linguens quad', CAST(N'1973-03-30 23:23:17.910' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (129, N'Winglibinower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis', CAST(N'1976-11-17 13:53:30.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (130, N'Qwiwerpower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad fecit. bono', CAST(N'1966-05-31 11:45:07.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (131, N'Emvenan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, habitatio', CAST(N'1999-04-06 03:28:07.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (132, N'Tupzapupin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'rarendum quis', CAST(N'2006-11-06 00:17:15.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (133, N'Rebanin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si novum', CAST(N'1969-10-06 05:36:55.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (134, N'Adpebentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, egreddior', CAST(N'1963-04-10 01:58:11.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (135, N'Emtanax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio, vobis', CAST(N'1975-08-16 08:00:51.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (136, N'Trumunackistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens si', CAST(N'1988-02-28 18:34:07.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (137, N'Reniponar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum quartu', CAST(N'1960-10-13 05:29:24.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (138, N'Groglibonor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut essit.', CAST(N'1986-02-15 20:05:33.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (139, N'Tupglibewover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum delerium.', CAST(N'1974-12-24 14:40:17.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (140, N'Rapdimedan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1986-10-21 13:34:42.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (141, N'Barerentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum dolorum', CAST(N'1979-11-10 23:19:51.520' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (142, N'Dopglibewantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam vobis quo,', CAST(N'1998-11-08 21:29:15.210' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (143, N'Cipmunicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis Quad ut', CAST(N'1982-12-07 03:56:43.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (144, N'Dopzapazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator', CAST(N'1972-06-19 02:34:29.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (145, N'Emtumantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad glavans Pro', CAST(N'1977-03-28 12:20:40.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (146, N'Qwisipopan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. quad', CAST(N'1972-10-17 19:41:54.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (147, N'Thruwerefazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum vobis', CAST(N'1963-03-07 03:56:29.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (148, N'Invenax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in regit, Multum', CAST(N'2001-02-20 12:57:30.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (149, N'Tuptumor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'manifestum si e', CAST(N'1990-04-10 03:05:43.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (150, N'Winglibedor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1959-08-02 03:29:56.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (151, N'Rapfropexower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum Et e', CAST(N'1958-02-08 03:16:27.110' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (152, N'Parsipex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. estum.', CAST(N'1986-05-12 12:48:26.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (153, N'Upsapistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis pars', CAST(N'1955-01-19 16:14:01.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (154, N'Barvenor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad glavans e', CAST(N'1954-10-12 01:29:19.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (155, N'Dopsapan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior. quo', CAST(N'1953-10-13 18:30:38.380' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (156, N'Bartanplar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quis estum.', CAST(N'1967-05-20 12:59:28.070' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (157, N'Upglibanan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio, quoque', CAST(N'1983-06-13 23:27:03.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (158, N'Indimin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. nomen in', CAST(N'2000-09-02 04:38:43.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (159, N'Klierilor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus habitatio', CAST(N'1968-06-02 11:40:07.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (160, N'Varfropazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu glavans', CAST(N'1986-05-27 06:06:55.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (161, N'Dopsipaquover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et fecundio,', CAST(N'1974-07-01 06:45:38.520' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (162, N'Trutumover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id transit.', CAST(N'1971-06-07 10:02:15.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (163, N'Haptineficator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'rarendum', CAST(N'1955-04-19 05:17:22.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (164, N'Wintanepex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. essit.', CAST(N'1977-01-26 13:26:33.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (165, N'Kliquestedower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et et quad', CAST(N'1976-03-28 20:55:27.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (166, N'Hapjubower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non volcans non', CAST(N'2005-05-28 18:32:32.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (167, N'Dopquestimex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e non quoque', CAST(N'1995-01-29 11:16:52.900' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (168, N'Tiptumewax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. Id estis', CAST(N'1991-12-14 19:34:18.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (169, N'Haperonex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si rarendum quo', CAST(N'2000-11-22 20:02:57.230' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (170, N'Qwivenilor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'egreddior Id', CAST(N'2005-03-14 17:21:14.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (171, N'Dopglibistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1984-04-07 13:55:32.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (172, N'Tiphupewar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare et', CAST(N'2005-05-07 12:32:30.910' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (173, N'Grodudonazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens venit.', CAST(N'1976-04-10 13:29:05.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (174, N'Raperedgazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens non et', CAST(N'2001-09-12 04:35:50.140' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (175, N'Surglibower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis non', CAST(N'1998-07-23 08:28:25.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (176, N'Varwerpepar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quo rarendum', CAST(N'1971-10-21 00:05:51.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (177, N'Retuminan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis bono', CAST(N'1958-03-10 19:36:43.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (178, N'Qwiquestopentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen parte', CAST(N'1989-11-28 06:03:06.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (179, N'Enderplor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in gravis plorum', CAST(N'1991-10-27 01:57:09.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (180, N'Emerommor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'esset pladior', CAST(N'1990-02-08 13:51:29.650' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (181, N'Supfropupan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum delerium.', CAST(N'1987-02-25 08:45:09.380' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (182, N'Dopweran', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et ut nomen et', CAST(N'2005-06-28 03:25:19.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (183, N'Winpickower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non eggredior.', CAST(N'1990-12-20 20:17:34.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (184, N'Thrutumepazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. essit.', CAST(N'1970-04-19 10:23:33.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (185, N'Trutanefistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, regit,', CAST(N'1961-04-23 08:28:40.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (186, N'Upjuban', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1965-11-25 14:39:41.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (187, N'Cippickin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. et in', CAST(N'2004-10-18 08:03:13.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (188, N'Varnipegover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estis nomen in', CAST(N'1967-12-27 04:07:36.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (189, N'Raptumicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sed eggredior.', CAST(N'1962-06-28 07:27:33.400' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (190, N'Tipwerin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio, venit.', CAST(N'1961-03-29 19:01:23.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (191, N'Retumador', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior in', CAST(N'1957-09-17 19:07:04.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (192, N'Surwerpanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad Longam,', CAST(N'1968-02-10 00:50:40.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (193, N'Surcadax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non rarendum non', CAST(N'1970-09-16 08:25:25.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (194, N'Upquestentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum quad', CAST(N'1996-07-29 08:33:21.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (195, N'Truveneficator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, linguens', CAST(N'1965-04-23 06:49:04.100' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (196, N'Lomrobopentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Longam, Longam,', CAST(N'1961-06-06 13:17:36.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (197, N'Thrududistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior brevens,', CAST(N'1996-07-23 16:11:52.440' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (198, N'Zeetanistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et gravis e', CAST(N'1958-11-11 14:30:10.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (199, N'Hapdudommentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo nomen Et', CAST(N'2004-03-22 05:39:02.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (200, N'Grobanegex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estis regit,', CAST(N'1979-10-30 17:48:08.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (201, N'Adjubar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, non', CAST(N'1985-09-18 08:25:01.180' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (202, N'Addimin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens quis et', CAST(N'1979-03-23 08:22:10.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (203, N'Hapkilin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars', CAST(N'1953-03-31 16:54:32.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (204, N'Qwidiman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non gravum', CAST(N'1993-01-25 09:44:28.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (205, N'Partumackicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen Tam', CAST(N'1983-04-25 13:34:24.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (206, N'Remunicower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id Quad Sed', CAST(N'1960-10-02 14:43:56.210' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (207, N'Haphupommentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit.', CAST(N'1963-02-01 23:19:20.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (208, N'Supzapplex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e Longam, in', CAST(N'1961-05-08 11:38:11.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (209, N'Rapwerpommin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. parte', CAST(N'1964-03-20 17:00:53.230' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (210, N'Barglibopin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed Et gravis', CAST(N'1989-12-23 18:24:46.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (211, N'Grozapar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor si', CAST(N'1962-03-12 17:22:44.340' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (212, N'Endfropackan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non Multum et', CAST(N'2001-07-15 17:23:56.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (213, N'Adweror', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens Longam,', CAST(N'1961-11-08 02:41:34.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (214, N'Thrusapepin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis et plorum', CAST(N'1986-04-23 21:48:38.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (215, N'Tupdudex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum non et et', CAST(N'2007-03-31 21:21:46.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (216, N'Truquestar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e plorum quo,', CAST(N'1977-01-02 03:20:18.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (217, N'Tiphupower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non in e gravis', CAST(N'1988-06-16 13:44:40.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (218, N'Tipzapan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si brevens, quis', CAST(N'1992-12-29 18:53:49.140' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (219, N'Fropebinex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum parte in', CAST(N'1968-10-06 04:57:53.450' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (220, N'Fronipover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed vantis.', CAST(N'1963-05-05 06:15:45.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (221, N'Winpebommower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque', CAST(N'1953-08-11 10:29:18.710' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (222, N'Thruglibistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum vobis', CAST(N'1980-02-04 07:09:16.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (223, N'Raprobicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e cognitio,', CAST(N'1982-06-19 12:06:45.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (224, N'Unglibinex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad in Pro Quad', CAST(N'1992-08-27 01:01:33.570' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (225, N'Wintanax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. quartu', CAST(N'2000-06-27 22:14:10.270' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (226, N'Winwerpin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id in et si et', CAST(N'1969-02-27 13:04:51.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (227, N'Thruwerpadex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens gravis', CAST(N'1987-08-23 04:07:04.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (228, N'Qwicadar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sed fecit.', CAST(N'1987-09-12 04:55:43.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (229, N'Parsapanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in et fecit.', CAST(N'1989-05-22 21:46:46.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (230, N'Dopcadackicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars et parte', CAST(N'1990-02-16 19:10:49.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (231, N'Fromunax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum Versus', CAST(N'1983-03-29 07:53:59.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (232, N'Rapglibistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'2007-04-29 05:06:33.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (233, N'Zeeglibentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estis fecit.', CAST(N'1993-09-07 00:24:04.450' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (234, N'Trubanilan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et transit.', CAST(N'1964-12-28 08:09:01.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (235, N'Klihupplower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare quad', CAST(N'1957-05-24 05:26:28.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (236, N'Endsapewover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1960-12-06 00:22:59.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (237, N'Klitinedicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam novum si', CAST(N'2005-03-30 10:30:46.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (238, N'Qwiweramistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. in Pro', CAST(N'2005-11-24 10:33:06.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (239, N'Surrobex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars non Quad', CAST(N'1994-12-24 20:49:09.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (240, N'Trududefistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'homo, estum.', CAST(N'1953-07-22 17:44:43.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (241, N'Klikilor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum quo homo,', CAST(N'1955-10-25 15:20:20.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (242, N'Zeepickicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e transit. quad', CAST(N'1996-06-12 20:25:02.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (243, N'Repebplor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et eudis', CAST(N'1956-10-08 11:06:43.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (244, N'Unpebopentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen transit.', CAST(N'1980-07-04 13:02:46.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (245, N'Tiprobar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono plurissimum', CAST(N'2007-01-17 01:38:39.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (246, N'Qwitinentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1964-03-04 01:43:07.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (247, N'Varhupower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, homo,', CAST(N'1959-04-04 22:09:06.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (248, N'Tipwerefax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id sed in', CAST(N'1992-05-24 10:36:47.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (249, N'Frosipex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro novum', CAST(N'1997-04-29 11:11:35.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (250, N'Vardudower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars non Sed', CAST(N'1996-04-20 03:28:45.150' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (251, N'Upfropantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quad quantare', CAST(N'1980-12-19 23:32:55.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (252, N'Tippebex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si Tam si funem.', CAST(N'1956-07-25 21:18:03.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (253, N'Lomsapentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis homo,', CAST(N'1998-02-04 09:32:16.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (254, N'Doptanaquex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in Sed gravis', CAST(N'1993-11-18 23:23:16.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (255, N'Rapsapinin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non quad non', CAST(N'1979-03-22 07:18:49.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (256, N'Intumegicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vantis. vobis', CAST(N'1958-08-16 01:15:45.060' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (257, N'Surcadewistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'homo, in quorum', CAST(N'2004-10-02 20:15:24.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (258, N'Endjubentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad vantis. sed', CAST(N'1968-08-03 15:52:59.400' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (259, N'Rewerpommower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. novum', CAST(N'2000-07-29 11:30:18.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (260, N'Hapfropackicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed quad estis', CAST(N'1967-02-27 00:29:33.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (261, N'Zeesipicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'habitatio quis', CAST(N'1981-02-09 10:36:51.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (262, N'Adglibedin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'egreddior in', CAST(N'1983-05-22 18:23:37.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (263, N'Trucadar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad quad nomen', CAST(N'1979-12-16 13:51:25.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (264, N'Cipmunamistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed delerium. si', CAST(N'1982-01-31 21:50:03.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (265, N'Tipkilplazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono novum', CAST(N'1959-09-28 10:30:46.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (266, N'Hapquestegover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio, gravum', CAST(N'1962-11-13 12:16:11.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (267, N'Dopsipazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro regit,', CAST(N'1960-11-08 16:59:52.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (268, N'Lomkilan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'esset plorum', CAST(N'2004-11-09 18:32:32.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (269, N'Rezapaquicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior. et', CAST(N'1981-12-01 16:22:09.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (270, N'Uprobistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1999-04-26 20:58:47.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (271, N'Endtinan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior. esset', CAST(N'1979-10-04 16:49:32.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (272, N'Qwidimistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum gravis si', CAST(N'1990-07-23 03:11:34.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (273, N'Vartinopax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque Sed', CAST(N'2006-01-04 22:04:40.140' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (274, N'Haprobantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro transit.', CAST(N'1968-03-17 11:13:09.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (275, N'Trubanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen parte quo', CAST(N'1960-07-11 14:55:24.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (276, N'Thrutinicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis quis', CAST(N'1965-02-27 14:04:16.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (277, N'Barjubupax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior. novum', CAST(N'1976-04-28 20:02:32.570' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (278, N'Emjubplax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator', CAST(N'1954-12-16 23:03:58.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (279, N'Rapsipopistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis novum', CAST(N'1993-08-09 23:22:54.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (280, N'Tipnipentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. cognitio,', CAST(N'1989-04-26 20:31:02.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (281, N'Klisapistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum esset', CAST(N'1962-11-26 14:55:44.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (282, N'Frowerollentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum et non in', CAST(N'2003-03-30 17:30:17.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (283, N'Adkilplor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad regit,', CAST(N'1998-05-14 17:44:08.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (284, N'Dopkilan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. regit,', CAST(N'2001-01-31 18:18:00.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (285, N'Ciprobedgan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in manifestum', CAST(N'1962-02-28 19:37:53.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (286, N'Cipfropicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans', CAST(N'1979-09-04 10:35:56.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (287, N'Trumunover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum venit.', CAST(N'1962-03-25 19:34:16.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (288, N'Rappebupex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars quartu', CAST(N'2007-10-19 01:00:47.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (289, N'Grotumamower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1993-10-08 00:50:11.520' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (290, N'Qwitanaquor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non linguens', CAST(N'2006-10-08 04:10:43.400' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (291, N'Suppickamin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum homo,', CAST(N'1964-03-04 04:07:34.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (292, N'Groerupower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sed novum', CAST(N'1989-08-27 18:37:47.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (293, N'Doppickepower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare Et', CAST(N'1958-04-28 23:02:47.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (294, N'Dopjubupin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in non vantis.', CAST(N'1980-10-28 20:19:35.820' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (295, N'Raptinower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen quad', CAST(N'1980-06-11 07:07:52.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (296, N'Winbanplin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum rarendum', CAST(N'1980-06-03 20:56:43.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (297, N'Tupwerpedgor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et in quo,', CAST(N'1969-06-29 06:33:36.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (298, N'Cipnipor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis plorum', CAST(N'1982-09-06 13:16:39.880' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (299, N'Tupkiledazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et et quo Multum', CAST(N'1973-10-08 02:31:59.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (300, N'Dopcadistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et ut', CAST(N'1997-03-06 13:22:28.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (301, N'Qwinipax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio,', CAST(N'1956-04-26 08:43:15.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (302, N'Rapdudex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio, ut', CAST(N'1962-05-25 03:36:33.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (303, N'Varquestonin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo et apparens', CAST(N'1963-06-24 18:14:38.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (304, N'Thruerepazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non non Tam Et', CAST(N'2006-01-02 14:12:47.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (305, N'Cipveninex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars linguens', CAST(N'1969-07-18 10:39:12.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (306, N'Endfropommax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis e quad', CAST(N'1998-05-29 20:21:21.100' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (307, N'Intinover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'homo, plorum non', CAST(N'1958-07-20 23:13:23.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (308, N'Adnipistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum.', CAST(N'1982-11-20 22:19:07.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (309, N'Thrupebplover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator Quad', CAST(N'1986-05-11 08:27:52.520' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (310, N'Surjubin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut brevens,', CAST(N'1967-01-23 17:14:33.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (311, N'Tipdudackar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis quo,', CAST(N'1990-04-18 05:13:12.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (312, N'Monkilicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens vobis', CAST(N'2002-10-18 21:11:26.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (313, N'Emnipadicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque et quad', CAST(N'1982-12-24 16:30:05.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (314, N'Endhupar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, cognitio,', CAST(N'1964-04-04 14:59:15.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (315, N'Truwerpamax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'egreddior', CAST(N'1957-04-03 09:33:48.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (316, N'Hapsapanantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu volcans', CAST(N'1985-11-11 17:06:34.150' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (317, N'Dopbanewistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis homo, pars', CAST(N'1993-01-07 00:39:29.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (318, N'Qwinipaquazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu Sed', CAST(N'1986-11-13 19:33:24.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (319, N'Refropilazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare', CAST(N'1972-12-04 02:12:09.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (320, N'Parfropexex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1988-01-23 23:54:50.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (321, N'Winsapegar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis e fecundio,', CAST(N'1953-08-21 16:41:54.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (322, N'Varglibewin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens pladior', CAST(N'1961-01-20 15:59:04.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (323, N'Qwicadin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque et et in', CAST(N'1964-02-03 17:42:21.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (324, N'Klibanentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quad quad', CAST(N'1983-02-23 12:11:26.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (325, N'Klirobor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio, Et', CAST(N'1988-02-23 16:51:13.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (326, N'Surrobexantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad homo,', CAST(N'1998-02-05 22:11:45.150' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (327, N'Frojubackar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare', CAST(N'1985-03-27 04:08:46.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (328, N'Barnipantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in plorum', CAST(N'1978-03-10 01:45:04.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (329, N'Unwerentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator', CAST(N'1995-10-06 04:05:17.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (330, N'Embanomman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non estum.', CAST(N'1988-02-06 17:58:40.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (331, N'Zeepebistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro cognitio,', CAST(N'2003-08-31 07:30:40.380' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (332, N'Zeedudazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quo, quoque', CAST(N'2001-08-22 06:45:52.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (333, N'Doptumower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'volcans pladior', CAST(N'1989-03-09 20:31:58.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (334, N'Monjubadower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed Longam, Tam', CAST(N'2004-06-04 10:00:25.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (335, N'Trusipedover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque plorum', CAST(N'1986-01-01 11:47:47.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (336, N'Cipzapar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, vobis et', CAST(N'1953-07-05 11:19:11.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (337, N'Parbanax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non quorum', CAST(N'1992-10-06 23:15:38.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (338, N'Montumax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum estis', CAST(N'2000-05-11 03:31:20.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (339, N'Inmunexan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte e bono', CAST(N'1984-07-17 07:38:48.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (340, N'Adsapanantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non linguens', CAST(N'1971-10-01 23:16:32.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (341, N'Winsipegax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum volcans', CAST(N'1974-02-15 11:22:33.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (342, N'Empebantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo brevens, quo', CAST(N'1987-05-04 21:15:08.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (343, N'Endweradentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare', CAST(N'1974-06-10 16:32:54.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (344, N'Montinaquicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis e habitatio', CAST(N'1979-09-16 07:35:38.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (345, N'Adfropicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro transit. Sed', CAST(N'1969-06-22 22:57:30.900' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (346, N'Cipdimor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus nomen Sed', CAST(N'1969-05-16 06:56:08.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (347, N'Wintinover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum quad', CAST(N'1959-09-05 02:07:42.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (348, N'Barwericator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'habitatio non', CAST(N'2002-01-23 08:03:32.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (349, N'Thruwerpin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis nomen', CAST(N'1976-07-28 10:45:47.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (350, N'Klitinar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad esset', CAST(N'2002-07-30 14:01:18.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (351, N'Monkilupentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens et quad', CAST(N'1995-05-27 20:17:14.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (352, N'Dopquesticar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad volcans non', CAST(N'1990-08-15 11:00:11.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (353, N'Unkilin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum parte et', CAST(N'1962-10-04 11:04:30.070' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (354, N'Zeekilamentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte Sed', CAST(N'1993-09-14 02:37:09.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (355, N'Truwerpantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum vobis', CAST(N'1996-07-16 20:40:33.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (356, N'Barquestex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator et', CAST(N'2003-04-05 16:44:10.130' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (357, N'Unnipistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum estum.', CAST(N'2006-04-20 09:54:12.230' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (358, N'Doppickex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'egreddior', CAST(N'1988-04-30 06:03:12.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (359, N'Updudistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1970-11-10 21:21:34.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (360, N'Qwierover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad et e', CAST(N'1979-06-11 12:27:27.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (361, N'Recadicex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et Multum Versus', CAST(N'1966-03-05 16:12:34.130' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (362, N'Zeeeradar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis', CAST(N'1988-05-11 21:28:18.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (363, N'Lomfropopover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum non Pro', CAST(N'1995-02-14 18:13:50.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (364, N'Moncadin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum Pro Et e', CAST(N'2006-04-03 14:29:11.820' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (365, N'Lomdimentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator quis', CAST(N'1971-05-25 09:43:52.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (366, N'Unericax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte si dolorum', CAST(N'1996-08-25 22:27:19.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (367, N'Suptumazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen si', CAST(N'1966-08-12 00:39:27.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (368, N'Dopsipar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans quo', CAST(N'1986-07-21 06:29:39.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (369, N'Dopmunower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono eggredior.', CAST(N'1986-04-03 03:11:27.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (370, N'Dopquesticazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum ut in', CAST(N'1956-09-29 22:14:37.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (371, N'Lomjubamor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et nomen quo', CAST(N'2001-02-03 17:50:55.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (372, N'Frotanimantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e apparens', CAST(N'2000-08-27 21:08:08.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (373, N'Endpebanar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. novum non', CAST(N'1955-04-30 11:54:36.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (374, N'Varwerex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis eggredior.', CAST(N'1954-03-06 02:55:37.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (375, N'Ciproban', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum pladior', CAST(N'1966-12-09 12:32:17.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (376, N'Adrobefower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior rarendum', CAST(N'1974-04-18 19:26:45.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (377, N'Upglibex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non novum ut Pro', CAST(N'1994-08-11 23:26:09.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (378, N'Hapvenower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum quad', CAST(N'1992-05-19 10:06:42.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (379, N'Upwerpex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam vobis quorum', CAST(N'1966-09-03 13:13:55.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (380, N'Inpebicax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum quorum', CAST(N'1971-10-11 09:55:05.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (381, N'Rapwerover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen et', CAST(N'1991-06-04 20:04:03.110' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (382, N'Winkilower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans quad', CAST(N'1991-03-03 20:05:14.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (383, N'Tupkilicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen nomen et', CAST(N'1954-08-13 14:59:16.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (384, N'Thrusipover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'rarendum Longam,', CAST(N'2001-03-07 14:03:58.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (385, N'Tupquesticator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu pars', CAST(N'2000-09-10 21:41:25.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (386, N'Zeebanax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. non', CAST(N'1966-01-16 20:30:28.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (387, N'Cipbanazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator non', CAST(N'1991-11-19 19:41:22.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (388, N'Recadollax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in si imaginator', CAST(N'2003-06-25 17:54:49.480' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (389, N'Surwerpimor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens et', CAST(N'1960-11-19 13:19:52.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (390, N'Varvenistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et dolorum et', CAST(N'1990-09-27 19:03:37.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (391, N'Rappebower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit. nomen', CAST(N'1955-03-17 01:24:58.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (392, N'Surglibentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens et', CAST(N'1957-06-09 17:12:26.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (393, N'Frowerpilan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Longam, non e', CAST(N'1982-07-02 06:09:45.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (394, N'Rapkilegazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non volcans', CAST(N'1958-02-06 19:11:20.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (395, N'Grozapin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum pladior', CAST(N'1958-07-30 07:03:58.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (396, N'Cipvenex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estis vantis. e', CAST(N'1993-10-05 13:54:14.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (397, N'Tipmunantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad linguens', CAST(N'2000-03-19 03:01:18.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (398, N'Rapwerpedan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu Quad non', CAST(N'1976-03-08 02:03:43.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (399, N'Adhupackin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum linguens', CAST(N'1961-04-24 21:51:45.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (400, N'Inmunar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. quo', CAST(N'1980-10-27 00:30:11.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (401, N'Emglibackicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1955-10-06 06:16:40.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (402, N'Upquestex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam et cognitio,', CAST(N'1981-02-13 19:38:02.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (403, N'Monsapar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro estis bono', CAST(N'1968-10-23 04:52:04.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (404, N'Truerefan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non pars et', CAST(N'1972-10-29 12:31:10.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (405, N'Varsipepistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et nomen et Sed', CAST(N'1954-06-26 20:10:23.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (406, N'Uphupanar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis non e', CAST(N'2002-09-28 23:09:05.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (407, N'Raptanex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1988-04-17 23:05:34.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (408, N'Addudeficator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. plorum', CAST(N'1961-08-01 08:08:53.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (409, N'Gromunor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum regit,', CAST(N'1979-10-07 14:04:51.440' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (410, N'Rappickover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e in Pro nomen', CAST(N'1975-04-01 01:13:47.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (411, N'Grotuminazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, pladior', CAST(N'1961-08-25 17:18:00.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (412, N'Invenazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, estis', CAST(N'1966-12-02 23:47:29.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (413, N'Upwerpistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte Multum et', CAST(N'1974-08-28 16:49:16.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (414, N'Resapan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens nomen', CAST(N'2007-01-30 11:26:29.450' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (415, N'Dopsipax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu quoque', CAST(N'1996-01-06 16:23:24.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (416, N'Updimantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens quo non', CAST(N'1983-01-01 20:07:19.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (417, N'Endbanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum si', CAST(N'1986-09-30 06:29:01.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (418, N'Qwirobantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. gravis', CAST(N'2001-12-09 04:09:51.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (419, N'Parvenin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. Pro quo', CAST(N'1958-07-24 16:47:45.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (420, N'Adtanazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1958-05-09 13:13:23.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (421, N'Updudegover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in quo brevens,', CAST(N'1957-10-08 02:45:57.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (422, N'Hapmunover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen parte', CAST(N'1971-02-16 06:44:38.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (423, N'Surkilicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono vobis quad', CAST(N'1978-09-01 17:09:46.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (424, N'Lompickistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'2003-07-06 08:07:50.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (425, N'Haprobexex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars quartu', CAST(N'1954-07-17 20:49:38.340' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (426, N'Endglibicex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam quo, gravum', CAST(N'1996-08-28 00:38:28.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (427, N'Thrudimamentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens,', CAST(N'1971-02-27 14:58:23.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (428, N'Thruvenommazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, non et', CAST(N'2005-03-22 16:43:16.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (429, N'Truwerentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis quis', CAST(N'2003-08-06 19:51:38.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (430, N'Thruglibax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. quartu in', CAST(N'1981-03-04 22:38:03.570' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (431, N'Adquestax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars plorum', CAST(N'1982-01-29 16:25:27.150' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (432, N'Uneror', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans', CAST(N'1971-06-06 21:22:26.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (433, N'Endbanopazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo quo,', CAST(N'1984-01-26 12:14:57.100' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (434, N'Hapsipackantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit.', CAST(N'1993-11-06 12:43:44.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (435, N'Qwikilantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, cognitio,', CAST(N'1969-01-06 03:38:24.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (436, N'Inerin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis non bono', CAST(N'1963-04-22 00:26:00.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (437, N'Froquestentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si quad transit.', CAST(N'1975-05-12 15:13:10.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (438, N'Tuperilazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio, esset', CAST(N'1989-10-14 02:28:06.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (439, N'Hapwerpax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior', CAST(N'1999-09-12 17:53:35.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (440, N'Adtinommower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad quoque e', CAST(N'2005-12-12 22:32:20.060' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (441, N'Varcadantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis', CAST(N'1985-09-29 21:24:54.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (442, N'Invenefantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, Id non quo', CAST(N'1983-04-21 11:32:29.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (443, N'Kliniponan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et egreddior et', CAST(N'1976-09-19 03:49:58.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (444, N'Klidudazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non quad eudis', CAST(N'1993-08-20 12:06:04.530' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (445, N'Endtinadax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis nomen', CAST(N'1982-02-03 01:45:37.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (446, N'Varpebimex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu bono et', CAST(N'1996-04-13 01:37:29.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (447, N'Emquestan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'egreddior quad', CAST(N'1990-10-12 00:23:09.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (448, N'Uphupopin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen pars in', CAST(N'1983-06-01 18:43:16.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (449, N'Tupkilax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed novum homo,', CAST(N'1970-01-31 02:47:55.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (450, N'Dopquestentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio,', CAST(N'1983-07-28 10:56:39.440' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (451, N'Mondudan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sed e quo Tam si', CAST(N'1960-01-07 07:25:12.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (452, N'Thruglibor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior linguens', CAST(N'2006-04-09 22:51:26.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (453, N'Adsapover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1970-08-16 09:15:57.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (454, N'Pardimin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis novum', CAST(N'1955-12-18 08:38:22.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (455, N'Vardimexor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, parte', CAST(N'1976-01-20 21:44:13.900' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (456, N'Klicadomman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro non vantis.', CAST(N'1992-08-18 23:32:02.530' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (457, N'Zeerobefazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen quoque', CAST(N'1955-06-16 02:28:15.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (458, N'Supjubanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, plorum', CAST(N'1973-04-18 20:44:13.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (459, N'Klicadamover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens Id', CAST(N'1971-04-15 15:41:31.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (460, N'Intumentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum apparens', CAST(N'1965-10-28 19:31:54.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (461, N'Winsipex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum non', CAST(N'1984-08-02 08:10:42.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (462, N'Thrujubefazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio,', CAST(N'1991-01-26 06:22:18.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (463, N'Grodudefistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1965-12-25 12:17:14.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (464, N'Unhupopistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'2006-08-03 15:26:47.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (465, N'Surzapopax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte si et', CAST(N'2001-03-01 08:48:20.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (466, N'Unglibazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si pladior quo,', CAST(N'1998-03-20 19:55:33.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (467, N'Surdudantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu egreddior', CAST(N'1990-11-06 11:32:32.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (468, N'Fronipantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit.', CAST(N'1988-10-21 04:05:32.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (469, N'Endfropupover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum non quad', CAST(N'1991-01-25 07:48:04.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (470, N'Thrujubover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo volcans', CAST(N'1985-09-17 15:57:24.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (471, N'Parbanan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen Multum', CAST(N'1960-08-23 04:54:08.450' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (472, N'Resapamower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'homo, e plorum', CAST(N'2004-12-16 04:00:55.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (473, N'Qwipickex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut si si quartu', CAST(N'1955-03-30 10:16:16.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (474, N'Lomwerupin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor quad', CAST(N'1960-08-10 18:15:25.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (475, N'Uppebeficator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'manifestum quis', CAST(N'1980-08-08 15:48:29.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (476, N'Supfropazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis', CAST(N'1982-06-18 15:13:27.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (477, N'Grohupover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e et Sed quorum', CAST(N'2002-05-29 02:54:39.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (478, N'Partumollax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'volcans linguens', CAST(N'1993-01-11 05:03:42.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (479, N'Endquestex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1974-06-03 06:22:12.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (480, N'Grojubewan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum Pro e non', CAST(N'1966-04-04 19:12:08.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (481, N'Rapweran', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis pladior', CAST(N'1958-11-18 00:15:18.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (482, N'Surpickedex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad funem. pars', CAST(N'1995-09-27 20:15:07.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (483, N'Emnipover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut estis vobis', CAST(N'1988-02-21 13:55:20.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (484, N'Thrucadicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, dolorum e', CAST(N'1973-01-10 08:54:58.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (485, N'Rewerpex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu regit,', CAST(N'1974-02-27 15:12:51.380' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (486, N'Thruzapantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e si apparens', CAST(N'1983-10-21 17:34:00.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (487, N'Trurobedicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et nomen in quo', CAST(N'1953-10-22 12:06:29.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (488, N'Addimamistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non quorum', CAST(N'1981-02-09 03:36:34.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (489, N'Adglibaquex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum nomen', CAST(N'1970-10-18 14:59:30.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (490, N'Frorobican', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. e estis', CAST(N'1984-05-19 04:06:22.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (491, N'Cippebefin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estis delerium.', CAST(N'1960-11-06 21:57:44.340' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (492, N'Frojubicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis nomen e', CAST(N'1955-01-24 18:28:56.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (493, N'Uppebower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id egreddior sed', CAST(N'1971-07-02 10:46:34.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (494, N'Suproban', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1986-11-15 22:21:41.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (495, N'Rejubackover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'esset Longam, e', CAST(N'1974-05-13 14:21:37.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (496, N'Haprobedazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum sed', CAST(N'2002-09-03 15:02:36.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (497, N'Gropickewover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'habitatio gravis', CAST(N'1970-04-27 23:15:14.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (498, N'Winnipaquar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. brevens,', CAST(N'1978-02-15 12:23:10.180' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (499, N'Frotanin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'esset in et', CAST(N'2000-06-20 03:17:43.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (500, N'Mondimilor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad homo, Sed', CAST(N'1991-11-15 05:50:04.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (501, N'Fromunistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad rarendum', CAST(N'1958-06-08 14:04:57.480' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (502, N'Requestopicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non Sed novum ut', CAST(N'1954-12-22 20:17:37.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (503, N'Hapbanantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si pars ut Pro', CAST(N'2000-05-06 10:55:11.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (504, N'Barnipazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis', CAST(N'1992-05-14 10:50:31.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (505, N'Qwicadazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit,', CAST(N'1975-12-06 06:02:30.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (506, N'Emhupollazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'esset quo,', CAST(N'1963-01-21 03:24:16.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (507, N'Dopsipin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad Tam Sed non', CAST(N'2004-01-12 12:35:10.440' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (508, N'Qwibanower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare volcans', CAST(N'1963-01-06 15:00:31.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (509, N'Adpickor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo quoque pars', CAST(N'1983-05-10 16:49:52.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (510, N'Emglibar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, glavans', CAST(N'2001-08-03 06:30:46.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (511, N'Groweran', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et vobis et', CAST(N'1998-03-22 20:37:22.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (512, N'Tippebommantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et fecundio,', CAST(N'1970-06-03 21:24:33.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (513, N'Klimuninicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator et', CAST(N'2006-11-15 14:22:01.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (514, N'Parwericator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo plurissimum', CAST(N'1958-01-11 20:29:23.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (515, N'Dopjubamower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus quartu', CAST(N'1959-12-12 19:16:35.570' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (516, N'Zeeglibar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. gravum', CAST(N'1996-11-04 11:55:35.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (517, N'Winmunax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'esset estis', CAST(N'1955-12-06 16:26:57.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (518, N'Tupzapazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. quo', CAST(N'2000-12-14 03:22:15.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (519, N'Fromunadover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Longam,', CAST(N'1985-06-07 00:13:56.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (520, N'Klitinimantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis quis', CAST(N'1970-05-12 15:14:20.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (521, N'Ciptanepentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. funem. e', CAST(N'1997-05-30 07:49:03.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (522, N'Emjuban', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis essit.', CAST(N'1962-09-28 07:55:57.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (523, N'Addimefor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut transit.', CAST(N'1971-01-04 13:06:02.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (524, N'Zeejubepin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo e Multum', CAST(N'1959-09-09 03:41:42.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (525, N'Dopquestonor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Longam, in', CAST(N'1977-11-11 17:59:39.910' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (526, N'Supnipewistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et vobis fecit.', CAST(N'1958-07-31 10:25:11.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (527, N'Unkilollan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1986-06-17 17:14:20.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (528, N'Zeebanefover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte sed eudis', CAST(N'1976-05-13 11:54:35.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (529, N'Kliwerentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum Multum', CAST(N'1994-03-30 03:53:48.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (530, N'Suptinomman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1985-12-05 00:45:53.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (531, N'Barnipaman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono bono parte', CAST(N'1965-02-21 15:58:12.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (532, N'Tupbanan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum quad', CAST(N'1977-04-13 14:31:29.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (533, N'Wintanimistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte gravis', CAST(N'2000-07-23 01:45:58.480' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (534, N'Happickower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum rarendum', CAST(N'1990-07-19 00:32:08.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (535, N'Ciphupplar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'egreddior Multum', CAST(N'1960-10-28 05:07:11.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (536, N'Surrobepin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1956-11-27 03:57:41.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (537, N'Supsapackantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen quartu', CAST(N'2003-05-23 09:19:56.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (538, N'Winrobaquicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'rarendum Tam', CAST(N'2003-10-09 03:04:48.880' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (539, N'Upwerpilantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et rarendum et e', CAST(N'2005-02-04 02:52:27.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (540, N'Thrutinex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'habitatio quorum', CAST(N'1964-02-08 11:48:34.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (541, N'Frokililower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte volcans', CAST(N'1987-10-09 04:58:49.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (542, N'Trupickadax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum nomen Tam', CAST(N'1998-10-15 21:05:57.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (543, N'Klitanepax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque nomen', CAST(N'2005-10-01 10:46:42.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (544, N'Cipsipex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum volcans in', CAST(N'1977-03-16 08:28:44.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (545, N'Hapdimex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor ut', CAST(N'1994-02-26 07:28:59.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (546, N'Winsapamazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum', CAST(N'1998-06-05 00:33:10.710' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (547, N'Adhupexazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis nomen', CAST(N'1956-03-10 04:06:55.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (548, N'Tipbanefantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens quo', CAST(N'1966-06-21 14:17:11.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (549, N'Froeredgan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis non in', CAST(N'1979-12-06 03:05:05.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (550, N'Tipcadopistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam parte gravum', CAST(N'1985-07-08 02:26:02.180' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (551, N'Retinentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum quo et', CAST(N'1999-09-29 22:35:38.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (552, N'Enderefazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sed nomen non', CAST(N'1977-05-13 19:52:59.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (553, N'Trujubover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, parte', CAST(N'2000-09-09 12:09:13.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (554, N'Lomfropar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, vantis.', CAST(N'1983-02-09 12:03:23.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (555, N'Montinepazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. quad', CAST(N'2002-06-16 22:01:00.070' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (556, N'Qwibanadan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in Tam Multum si', CAST(N'2003-09-10 06:09:00.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (557, N'Gropickower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen quo', CAST(N'1991-12-18 07:11:34.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (558, N'Emhupazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans quorum', CAST(N'1993-08-29 07:13:28.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (559, N'Ciphupaquin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'volcans plorum', CAST(N'1967-07-04 16:50:51.710' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (560, N'Gropebefax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id parte quo,', CAST(N'1970-05-08 19:37:19.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (561, N'Klikilistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et Pro quad', CAST(N'1984-08-04 10:09:56.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (562, N'Empickefax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis vobis Sed', CAST(N'1980-09-01 00:40:35.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (563, N'Trududexan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens plorum', CAST(N'1976-10-08 11:06:43.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (564, N'Parvenazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus quo et', CAST(N'2005-06-04 16:03:59.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (565, N'Cipglibentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad et estis', CAST(N'1989-06-12 14:32:33.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (566, N'Zeerobantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis in homo,', CAST(N'1973-12-10 06:05:52.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (567, N'Surzapplower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id brevens,', CAST(N'1977-11-10 19:38:35.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (568, N'Klierin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'habitatio', CAST(N'1969-04-30 13:49:52.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (569, N'Untinar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non plorum', CAST(N'2005-03-15 15:22:11.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (570, N'Trukilicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior in', CAST(N'1956-01-16 15:18:53.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (571, N'Supvenonar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et si funem.', CAST(N'2001-06-14 02:35:39.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (572, N'Hapwerentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad quo quoque', CAST(N'1960-02-27 13:22:08.900' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (573, N'Thrufropexex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis quorum in', CAST(N'2003-11-09 00:29:47.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (574, N'Zeepickover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non plorum', CAST(N'1974-04-30 12:25:21.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (575, N'Klipebupentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si in Pro venit.', CAST(N'1990-08-03 10:15:30.380' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (576, N'Zeefropax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum quoque', CAST(N'2006-08-14 10:43:06.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (577, N'Parweror', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. in quis', CAST(N'1964-12-26 21:09:15.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (578, N'Updimax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad gravis', CAST(N'2001-10-20 04:37:18.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (579, N'Tiptumaquistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum', CAST(N'1973-07-12 20:12:42.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (580, N'Trujubamazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad funem.', CAST(N'1987-06-08 17:26:29.210' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (581, N'Frotinommex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum quantare', CAST(N'1955-05-24 06:18:16.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (582, N'Frohupan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans quo', CAST(N'1972-12-28 02:57:09.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (583, N'Rappebinor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. Longam,', CAST(N'2004-06-07 08:49:29.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (584, N'Lomfropin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1961-09-23 01:27:46.150' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (585, N'Tipcadinan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et ut Tam regit,', CAST(N'1994-03-07 02:10:15.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (586, N'Trueristor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e nomen Versus', CAST(N'1995-10-10 20:05:13.570' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (587, N'Cipcadplower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1984-08-03 02:22:16.110' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (588, N'Cipbanover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus et novum', CAST(N'1970-06-14 13:42:34.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (589, N'Tupglibonover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id pars nomen Et', CAST(N'1996-02-01 02:31:08.710' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (590, N'Adtinover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior quo bono', CAST(N'1965-02-16 22:58:10.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (591, N'Klisapilax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1972-12-09 20:46:06.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (592, N'Ciprobax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non essit.', CAST(N'1962-09-25 02:06:27.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (593, N'Qwiglibamentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1989-04-07 10:13:57.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (594, N'Qwiwerpommicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro volcans', CAST(N'1987-09-30 07:15:08.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (595, N'Raprobor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro trepicandor', CAST(N'1987-07-06 01:38:38.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (596, N'Endsipaquax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens novum', CAST(N'1960-02-08 04:18:14.820' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (597, N'Frofropepex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum fecundio,', CAST(N'1975-01-08 09:59:14.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (598, N'Dopsipamar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non nomen et', CAST(N'1953-04-29 18:42:42.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (599, N'Tupdudackazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1995-10-03 17:56:10.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (600, N'Fropickefantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non gravis', CAST(N'1973-03-20 01:23:06.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (601, N'Unmunax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens Longam,', CAST(N'1973-07-26 04:54:35.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (602, N'Tipglibefistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si Id Tam quoque', CAST(N'1994-11-09 14:17:03.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (603, N'Tupjubin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu si', CAST(N'1969-04-06 22:54:59.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (604, N'Emeramower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non quantare', CAST(N'1976-11-20 11:46:11.520' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (605, N'Adbanistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Longam, Multum', CAST(N'1992-01-23 23:50:22.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (606, N'Qwitumexax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1992-08-03 09:13:29.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (607, N'Endcadover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte sed', CAST(N'1981-07-04 19:23:11.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (608, N'Dopwerpex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1979-05-13 23:43:51.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (609, N'Zeenipefor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. et gravis', CAST(N'2000-07-08 01:47:53.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (610, N'Trueristor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si gravum bono', CAST(N'2003-11-02 11:17:54.060' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (611, N'Upzapilor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in plorum Pro', CAST(N'1991-09-15 08:59:45.210' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (612, N'Upcadan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. quad', CAST(N'1998-02-27 02:34:16.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (613, N'Haptanommower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sed fecit. homo,', CAST(N'1973-02-17 11:00:45.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (614, N'Bartumover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad regit,', CAST(N'1988-01-25 23:24:22.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (615, N'Haptumilower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo quoque', CAST(N'1967-12-17 13:19:03.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (616, N'Tupwerpaquax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in et fecit.', CAST(N'1964-08-08 07:12:47.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (617, N'Inwerpin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum fecit,', CAST(N'1984-03-25 15:51:05.270' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (618, N'Windudicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quorum quantare', CAST(N'1968-01-17 09:38:57.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (619, N'Kliglibinantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte regit, et', CAST(N'1964-03-31 13:53:44.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (620, N'Parzapinor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. brevens,', CAST(N'2001-09-01 18:27:19.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (621, N'Thrupickover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen quo, pars', CAST(N'1997-05-08 03:43:36.650' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (622, N'Thruglibover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo Id estis', CAST(N'1960-05-30 05:56:00.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (623, N'Klivenaquicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1981-03-29 11:18:43.710' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (624, N'Uphupistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in venit. eudis', CAST(N'1993-01-27 16:24:00.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (625, N'Emquestan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo et et essit.', CAST(N'1991-02-05 18:32:47.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (626, N'Supzapamin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum cognitio,', CAST(N'1991-09-21 00:29:11.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (627, N'Monjubanicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'rarendum pladior', CAST(N'1978-11-21 03:08:11.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (628, N'Haprobicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum plorum', CAST(N'1969-02-09 17:14:39.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (629, N'Adpebommantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'manifestum novum', CAST(N'1994-10-25 10:46:43.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (630, N'Aderor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens et e in', CAST(N'1983-06-27 05:57:15.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (631, N'Winwerponower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio,', CAST(N'1984-08-07 00:43:51.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (632, N'Varrobover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1964-08-30 02:26:32.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (633, N'Thrusipaquentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si delerium.', CAST(N'1959-01-18 10:41:07.880' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (634, N'Hapmunan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1953-04-25 18:49:36.140' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (635, N'Qwirobantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si nomen homo,', CAST(N'1966-11-14 12:31:53.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (636, N'Winfropax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens essit.', CAST(N'1968-07-13 16:01:12.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (637, N'Cipdudazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et cognitio, non', CAST(N'1956-09-23 04:02:09.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (638, N'Winfropanax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo Sed non', CAST(N'1980-07-18 21:03:18.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (639, N'Empebedax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum si homo,', CAST(N'1977-08-30 01:37:23.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (640, N'Dopmunex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. Tam et', CAST(N'1954-08-04 16:56:45.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (641, N'Truquestanower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in et si plorum', CAST(N'1967-04-09 00:42:02.150' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (642, N'Emwerpistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum quis', CAST(N'1958-03-24 02:38:24.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (643, N'Zeehupplazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare non', CAST(N'1977-12-06 11:54:41.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (644, N'Lomrobistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum Tam', CAST(N'2000-03-30 15:13:14.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (645, N'Advenefex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'habitatio e', CAST(N'1981-06-03 13:58:25.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (646, N'Suphupexistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus estum.', CAST(N'1982-10-12 18:11:50.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (647, N'Varmunonazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis plorum', CAST(N'1997-08-06 15:05:41.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (648, N'Hapcadomman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'manifestum', CAST(N'1954-03-14 10:49:34.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (649, N'Infropadantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum non', CAST(N'1972-10-14 17:56:05.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (650, N'Truquestan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro homo, e', CAST(N'1975-11-28 18:25:51.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (651, N'Klisapentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, gravis', CAST(N'1983-11-14 12:27:09.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (652, N'Tipkilar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. linguens', CAST(N'1959-12-24 00:20:17.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (653, N'Qwierentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si in novum pars', CAST(N'1997-01-28 04:48:58.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (654, N'Tipnipommor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum quo', CAST(N'2002-01-30 23:28:50.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (655, N'Rapwerefower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis delerium.', CAST(N'1983-11-11 21:39:30.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (656, N'Ciptumamover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis in fecit,', CAST(N'1966-02-26 22:58:35.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (657, N'Tupzapax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1966-08-14 02:58:19.450' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (658, N'Parjuban', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad quo gravis', CAST(N'1984-03-09 06:30:58.650' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (659, N'Unbanentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'homo, habitatio', CAST(N'1993-10-14 17:13:24.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (660, N'Zeeglibepor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e gravis Versus', CAST(N'1953-05-05 23:21:10.910' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (661, N'Adtinollentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et delerium. Tam', CAST(N'1966-09-14 06:05:17.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (662, N'Groquestedgantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum et pars', CAST(N'1965-05-26 02:08:32.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (663, N'Uprobican', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e Tam', CAST(N'1992-07-18 12:57:08.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (664, N'Groquestar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, et brevens,', CAST(N'1970-01-12 19:50:37.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (665, N'Tupwerax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum quad', CAST(N'1972-03-28 10:34:02.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (666, N'Zeecadin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id sed linguens', CAST(N'1980-01-17 06:33:12.910' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (667, N'Dopdudicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare gravis', CAST(N'1992-02-10 15:53:08.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (668, N'Barpebollistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et vobis', CAST(N'1972-10-25 16:11:09.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (669, N'Varcadegax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens', CAST(N'1974-03-14 10:52:28.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (670, N'Tupmunower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non venit. et et', CAST(N'2007-06-04 21:17:50.270' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (671, N'Tipnipex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque esset', CAST(N'1960-11-16 03:10:37.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (672, N'Trumunicantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum et', CAST(N'1956-09-07 18:47:46.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (673, N'Inkilex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit.', CAST(N'1998-05-19 08:01:33.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (674, N'Supbanax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed non Id et', CAST(N'1962-08-28 20:56:58.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (675, N'Endtinantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum novum', CAST(N'1987-05-27 17:45:17.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (676, N'Trueranistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium.', CAST(N'1992-02-16 01:05:12.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (677, N'Klitanentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'egreddior quorum', CAST(N'1990-06-23 18:01:18.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (678, N'Parglibor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1988-12-28 20:05:29.060' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (679, N'Dopdimower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum habitatio', CAST(N'1964-08-27 02:25:35.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (680, N'Zeepebar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'esset glavans', CAST(N'1961-04-27 00:20:56.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (681, N'Upzapexistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis gravum non', CAST(N'2002-02-13 09:23:30.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (682, N'Endfropicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et estis Sed', CAST(N'1977-09-09 16:18:37.100' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (683, N'Haprobadantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad estum.', CAST(N'2004-04-26 23:18:33.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (684, N'Cipsipentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si non regit,', CAST(N'1971-11-28 16:20:52.540' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (685, N'Varquesticor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis venit.', CAST(N'2007-08-12 17:30:06.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (686, N'Thruwerpepower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e gravum', CAST(N'1977-08-16 00:23:33.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (687, N'Winkilackower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1975-08-01 21:37:33.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (688, N'Kliglibexazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in quad non', CAST(N'1964-12-17 11:12:17.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (689, N'Aderover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'habitatio', CAST(N'1977-06-13 21:10:05.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (690, N'Lomrobentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro egreddior', CAST(N'1953-09-18 13:33:18.340' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (691, N'Upsapor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1954-12-01 13:02:53.480' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (692, N'Dopmunor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed bono Versus', CAST(N'1980-07-15 21:31:10.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (693, N'Tiptumax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum e venit.', CAST(N'1994-10-04 03:49:53.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (694, N'Zeeeran', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut', CAST(N'1977-04-27 02:16:12.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (695, N'Varrobamar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte et non et', CAST(N'1995-11-20 10:51:28.180' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (696, N'Surkilollantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam apparens non', CAST(N'1954-11-14 22:33:29.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (697, N'Lomtanegistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit,', CAST(N'1973-09-04 13:41:16.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (698, N'Klipickepover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen estis', CAST(N'1960-11-24 16:04:04.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (699, N'Revenexin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus essit.', CAST(N'1981-01-03 04:39:11.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (700, N'Suptanamantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. quis Id', CAST(N'1954-05-19 07:25:12.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (701, N'Winvenupower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior. quo', CAST(N'1998-09-01 02:18:41.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (702, N'Winniponar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior e quo', CAST(N'1977-10-12 22:54:32.070' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (703, N'Ciptinedgower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis pladior', CAST(N'1987-12-30 23:57:05.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (704, N'Tupcadaman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et brevens,', CAST(N'1973-05-11 20:38:15.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (705, N'Supquestor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et Et essit.', CAST(N'1968-01-13 15:15:13.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (706, N'Endcadar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. sed', CAST(N'1980-03-08 17:39:22.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (707, N'Monpebexin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e apparens', CAST(N'1969-08-05 16:18:43.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (708, N'Parsapistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e homo, rarendum', CAST(N'1987-06-03 07:51:59.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (709, N'Varsapicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo bono sed', CAST(N'1967-09-07 05:43:32.650' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (710, N'Barcadefax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'esset', CAST(N'1999-09-25 20:34:17.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (711, N'Lommunexantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis si parte', CAST(N'1983-09-05 04:57:16.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (712, N'Klidimazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo linguens', CAST(N'1985-08-15 06:52:50.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (713, N'Emdimewover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit. quo Sed', CAST(N'1972-07-14 04:05:23.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (714, N'Frohupower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens quorum', CAST(N'1985-06-20 16:52:27.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (715, N'Thruhupadazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Longam,', CAST(N'1959-03-22 13:57:40.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (716, N'Thruhupegin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. plorum', CAST(N'1997-01-03 13:46:40.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (717, N'Dopnipistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum et', CAST(N'1969-01-28 02:49:58.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (718, N'Qwidudor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis venit.', CAST(N'1973-08-21 20:36:15.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (719, N'Parrobistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut si bono', CAST(N'1970-06-02 02:20:31.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (720, N'Tuptinackentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio,', CAST(N'2002-08-12 10:10:29.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (721, N'Grocador', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis si si', CAST(N'1957-06-18 08:55:39.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (722, N'Tippickimar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte vobis', CAST(N'1980-02-25 04:40:51.230' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (723, N'Barjubexantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et', CAST(N'2003-05-13 19:01:03.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (724, N'Hapwerpedgistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quad funem.', CAST(N'1973-06-26 21:19:10.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (725, N'Tupsapopax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens plorum', CAST(N'2005-07-05 08:14:56.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (726, N'Enderilantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut pars funem.', CAST(N'1988-05-02 05:39:27.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (727, N'Klifropopentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si dolorum', CAST(N'1969-12-04 14:57:52.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (728, N'Undudantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum fecit. non', CAST(N'1974-01-23 05:45:21.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (729, N'Lomwerponex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in Multum si', CAST(N'1974-10-08 10:05:30.340' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (730, N'Upfropar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in fecit, quoque', CAST(N'1985-07-13 19:11:48.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (731, N'Zeekilex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum sed Quad', CAST(N'1971-06-14 11:23:56.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (732, N'Truquestanazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum glavans', CAST(N'1982-07-31 01:57:14.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (733, N'Endpebentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e vobis quantare', CAST(N'1960-12-26 15:26:17.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (734, N'Grodimedgicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vantis. eudis', CAST(N'1963-04-11 19:00:22.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (735, N'Intanomman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu et ut', CAST(N'1970-08-25 22:02:15.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (736, N'Intanaquex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad non et in e', CAST(N'1977-05-04 10:28:58.270' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (737, N'Dopbanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis nomen', CAST(N'1997-09-09 16:21:31.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (738, N'Rapbanedgor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e funem. novum', CAST(N'1983-12-18 02:40:18.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (739, N'Addudar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis Multum e', CAST(N'1956-11-16 01:05:39.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (740, N'Supvenar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque quad', CAST(N'1980-06-05 23:50:43.380' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (741, N'Tupzapadower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio, essit.', CAST(N'1961-01-11 08:04:49.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (742, N'Tipvenazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen et', CAST(N'1996-02-28 13:45:33.880' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (743, N'Undimamin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vantis. esset', CAST(N'2005-04-11 05:28:44.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (744, N'Zeezapaquazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad Quad Tam', CAST(N'1985-09-18 00:35:48.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (745, N'Hapmunegan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens in', CAST(N'1978-01-13 08:36:01.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (746, N'Hapjubower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, quad quoque', CAST(N'1977-04-17 06:10:03.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (747, N'Rappickazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen fecit, quo', CAST(N'1961-01-02 17:25:47.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (748, N'Lomzapor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum et sed', CAST(N'1962-04-16 10:57:26.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (749, N'Parfropackazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut Sed apparens', CAST(N'1971-02-06 12:18:31.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (750, N'Partanplantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis quad', CAST(N'2006-11-15 21:04:47.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (751, N'Tipjubegor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo Et volcans', CAST(N'1993-10-16 19:43:31.140' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (752, N'Truwerommantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis esset', CAST(N'1953-01-14 17:05:00.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (753, N'Groerentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et e si Et', CAST(N'1998-06-24 13:43:32.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (754, N'Truwerpepar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior e gravis', CAST(N'1986-05-23 18:28:42.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (755, N'Barnipommover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id parte parte', CAST(N'2004-11-29 22:29:33.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (756, N'Varvenopistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum regit,', CAST(N'1971-07-08 21:37:50.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (757, N'Dopmunonower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit.', CAST(N'1982-06-01 03:53:28.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (758, N'Lomhupor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et venit.', CAST(N'1967-07-07 02:37:55.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (759, N'Empebadar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1975-11-06 12:23:18.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (760, N'Cipsipistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior non', CAST(N'1987-09-12 09:35:46.130' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (761, N'Tupdimollan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non nomen', CAST(N'1963-04-06 02:41:29.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (762, N'Zeetanepentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis vobis', CAST(N'2003-07-13 10:33:22.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (763, N'Cipsapanar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens fecit.', CAST(N'1965-08-26 21:46:29.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (764, N'Unpebimover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo', CAST(N'1990-02-23 10:31:44.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (765, N'Upcador', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis eggredior.', CAST(N'1981-04-14 09:10:21.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (766, N'Inbanar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum et essit.', CAST(N'1954-02-06 03:00:02.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (767, N'Infropor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis novum quad', CAST(N'1977-08-20 15:26:33.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (768, N'Zeekilegex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estis regit,', CAST(N'1962-04-05 09:05:01.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (769, N'Klidudefantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si quad bono et', CAST(N'1967-10-27 18:50:12.300' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (770, N'Thrutanistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'rarendum non', CAST(N'1989-11-17 03:28:42.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (771, N'Parrobex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars pladior', CAST(N'2007-04-25 21:25:09.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (772, N'Rapbanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu pars', CAST(N'1993-02-10 00:09:40.270' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (773, N'Montananentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et eggredior. ut', CAST(N'1969-10-06 19:30:36.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (774, N'Dopglibazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in quo habitatio', CAST(N'1997-12-26 02:29:48.230' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (775, N'Inbanedgower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non apparens et', CAST(N'1994-08-02 02:05:39.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (776, N'Hapsapplor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio, funem.', CAST(N'1969-04-19 19:10:05.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (777, N'Superanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. e si si', CAST(N'1982-11-14 23:12:06.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (778, N'Qwisipor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'manifestum', CAST(N'1970-08-26 18:25:15.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (779, N'Emtinistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. si', CAST(N'2003-06-13 12:43:09.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (780, N'Hapvenantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et volcans', CAST(N'1981-03-21 02:04:53.690' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (781, N'Cipquestover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad homo,', CAST(N'1980-04-30 12:13:39.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (782, N'Trucadazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in brevens,', CAST(N'1960-08-21 04:00:26.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (783, N'Supkilar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad homo,', CAST(N'1955-12-18 17:26:49.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (784, N'Trudimedgower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut quo, venit.', CAST(N'1975-11-22 13:00:42.650' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (785, N'Varwerpewar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et funem.', CAST(N'1999-09-26 23:28:16.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (786, N'Cipveninar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et si plorum', CAST(N'1971-05-31 18:18:55.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (787, N'Insapedgover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, quartu', CAST(N'1992-02-05 12:13:41.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (788, N'Trutinommicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens quorum', CAST(N'1982-08-04 20:57:34.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (789, N'Emquestegower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. habitatio', CAST(N'1966-05-11 20:30:48.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (790, N'Froglibover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad Longam,', CAST(N'2007-02-17 00:24:24.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (791, N'Zeefropower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et venit.', CAST(N'2002-12-03 06:35:29.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (792, N'Rapquestex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars vantis.', CAST(N'1983-03-07 13:43:36.020' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (793, N'Tupcadimex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non linguens', CAST(N'1961-02-11 14:17:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (794, N'Zeeweramar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo et ut', CAST(N'1977-03-05 15:29:53.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (795, N'Supzapamin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum gravum', CAST(N'1970-03-03 21:09:13.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (796, N'Barsapamover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e plorum non', CAST(N'1965-06-30 21:31:27.110' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (797, N'Tipwerpazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut e plorum', CAST(N'1983-07-05 15:59:04.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (798, N'Zeequestexex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum et Tam', CAST(N'1968-02-17 18:57:00.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (799, N'Tiptumanantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars Versus in', CAST(N'1957-06-06 15:25:26.130' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (800, N'Zeejubexazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'volcans', CAST(N'1976-12-08 05:35:58.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (801, N'Qwifropaquar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis essit.', CAST(N'2007-08-27 14:43:29.910' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (802, N'Monwerantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quartu nomen', CAST(N'2006-09-29 08:20:46.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (803, N'Happickupax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. plorum', CAST(N'2002-06-19 16:07:14.160' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (804, N'Trusapax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Longam,', CAST(N'1989-03-21 08:49:49.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (805, N'Enddudover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior volcans', CAST(N'2007-07-22 23:39:38.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (806, N'Wincadex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens', CAST(N'1953-09-06 19:33:50.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (807, N'Mondudonex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro in gravis e', CAST(N'1991-04-09 21:34:24.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (808, N'Surtanin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non et funem.', CAST(N'1953-06-30 11:13:44.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (809, N'Monsipefex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecundio, quad', CAST(N'1999-10-21 16:23:03.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (810, N'Parvenistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1988-08-13 03:19:28.570' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (811, N'Upquestower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis Longam,', CAST(N'1996-08-22 22:25:43.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (812, N'Tipzapimar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1964-10-06 09:28:12.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (813, N'Tuperax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium.', CAST(N'1972-01-23 13:54:46.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (814, N'Adcadazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte quoque', CAST(N'2002-08-17 10:18:24.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (815, N'Tupkilicover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. e ut', CAST(N'1960-05-14 21:22:06.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (816, N'Grosipentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed linguens', CAST(N'1955-08-16 22:41:03.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (817, N'Zeepickazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad quo quis', CAST(N'2000-07-26 17:06:32.880' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (818, N'Adwerpefar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono et vantis.', CAST(N'1997-10-04 08:45:46.960' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (819, N'Wintanan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars nomen', CAST(N'1998-05-29 11:03:27.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (820, N'Froglibaquower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1962-11-15 14:51:26.520' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (821, N'Endwerefax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in egreddior', CAST(N'1969-12-31 14:46:20.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (822, N'Unmunefax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estis fecit.', CAST(N'2003-01-15 18:28:58.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (823, N'Frocadplan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum non', CAST(N'2003-11-24 12:34:12.450' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (824, N'Barbanistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et nomen Et', CAST(N'1968-09-08 18:25:47.530' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (825, N'Bartinaquover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis quo', CAST(N'1994-12-14 20:53:12.480' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (826, N'Vardudackistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis e regit,', CAST(N'1977-02-19 07:41:33.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (827, N'Zeebanax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator', CAST(N'1984-12-30 09:52:40.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (828, N'Lomjubax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id bono quorum', CAST(N'2000-08-02 04:00:43.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (829, N'Endfropistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator', CAST(N'1980-10-25 05:20:34.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (830, N'Tipnipor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravum', CAST(N'1982-02-02 04:34:12.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (831, N'Partuman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum estis', CAST(N'1993-10-15 03:11:00.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (832, N'Parkilin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte sed quo in', CAST(N'1967-09-29 04:15:06.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (833, N'Enddimackistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non quad e', CAST(N'1966-03-23 02:59:49.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (834, N'Tiperor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et regit, gravis', CAST(N'1979-07-07 07:07:10.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (835, N'Tuppebaquover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis e vantis.', CAST(N'1981-07-24 11:21:23.780' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (836, N'Monrobazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non in Sed', CAST(N'1981-08-01 03:52:53.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (837, N'Rapjubin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et egreddior et', CAST(N'1966-03-03 11:53:12.350' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (838, N'Retanin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum linguens', CAST(N'1974-09-28 08:37:00.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (839, N'Tupglibanex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad egreddior', CAST(N'1976-05-01 13:21:04.150' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (840, N'Monhupanex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum Sed', CAST(N'1999-01-21 03:54:25.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (841, N'Barjubentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen Id et bono', CAST(N'1988-02-20 20:44:30.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (842, N'Adzapexazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens ut e e', CAST(N'1991-08-09 00:59:57.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (843, N'Rappebadar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, parte', CAST(N'1990-10-14 04:34:31.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (844, N'Qwidudan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum vobis et', CAST(N'1985-09-27 17:11:20.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (845, N'Resapepantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis bono in', CAST(N'1963-06-28 01:58:40.820' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (846, N'Tupwerpistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. quad', CAST(N'1991-02-27 21:07:16.940' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (847, N'Barmunazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e travissimantor', CAST(N'1964-02-13 23:48:49.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (848, N'Lomwerepex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum Tam', CAST(N'1966-06-27 03:58:35.270' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (849, N'Barcadollan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis manifestum', CAST(N'1969-10-22 18:07:46.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (850, N'Adtinupistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator si e', CAST(N'1967-07-19 22:28:10.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (851, N'Addudackower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis habitatio', CAST(N'1972-12-04 04:24:54.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (852, N'Zeebanackover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si fecit, nomen', CAST(N'1993-09-30 07:13:17.310' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (853, N'Supzapor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque parte', CAST(N'1977-09-20 07:53:32.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (854, N'Grodimicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad in', CAST(N'2002-07-22 00:33:59.610' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (855, N'Supwerpicover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Tam volcans Tam', CAST(N'1959-12-09 14:49:38.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (856, N'Barvenicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis pladior', CAST(N'1957-08-12 20:14:31.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (857, N'Barhupefantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eudis', CAST(N'1955-11-06 19:46:33.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (858, N'Monmunex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu', CAST(N'1986-10-11 21:41:26.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (859, N'Windimilistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'2002-04-06 11:56:37.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (860, N'Rapwerin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in', CAST(N'1983-06-03 15:58:05.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (861, N'Cipnipamar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et pladior Tam', CAST(N'1976-09-06 09:41:10.620' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (862, N'Lomtanackor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro dolorum', CAST(N'1998-09-06 17:33:41.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (863, N'Zeehupepar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens,', CAST(N'1980-09-04 05:53:09.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (864, N'Vartinar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio, nomen', CAST(N'1970-09-19 11:48:50.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (865, N'Incadupax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pars manifestum', CAST(N'2002-10-29 00:08:27.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (866, N'Refropexantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. homo,', CAST(N'1981-11-25 18:10:37.530' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (867, N'Frotanupistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, Tam', CAST(N'2006-07-16 06:28:23.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (868, N'Monfropazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior. quo', CAST(N'2000-07-18 06:47:36.190' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (869, N'Trupebax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et bono in', CAST(N'1974-01-24 22:57:00.950' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (870, N'Dophupor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et plorum regit,', CAST(N'1986-10-13 17:29:12.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (871, N'Upsipor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in Longam, bono', CAST(N'1997-06-14 10:47:50.770' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (872, N'Sursipan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis e quartu', CAST(N'1978-10-30 08:29:32.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (873, N'Qwiwerewax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et imaginator et', CAST(N'1974-06-05 19:24:42.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (874, N'Zeeerantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et parte regit,', CAST(N'2007-05-08 07:09:01.820' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (875, N'Unmunor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte gravis', CAST(N'1982-09-24 18:06:55.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (876, N'Lomtumazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad ut', CAST(N'1986-05-24 13:46:23.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (877, N'Untuman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. habitatio', CAST(N'1965-04-04 13:54:27.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (878, N'Tipcadex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Sed quorum', CAST(N'1971-02-27 19:22:52.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (879, N'Upjubicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis quo', CAST(N'1958-03-15 16:27:06.710' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (880, N'Lomsapedower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit. plorum', CAST(N'1956-10-17 23:45:55.530' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (881, N'Upsipicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quartu et Sed', CAST(N'1966-01-07 07:52:43.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (882, N'Qwiericantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum quad', CAST(N'1971-07-07 05:54:04.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (883, N'Hapwerican', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior. pars', CAST(N'1983-10-12 09:52:56.260' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (884, N'Upvenanower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen gravum', CAST(N'1966-04-09 06:52:23.870' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (885, N'Zeebanover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus gravis', CAST(N'1962-03-02 00:09:18.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (886, N'Endnipefistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen novum', CAST(N'1953-01-15 19:57:43.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (887, N'Surdudonex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum Tam', CAST(N'1998-01-07 20:21:34.140' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (888, N'Barsipilazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio,', CAST(N'1971-09-01 02:25:49.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (889, N'Inrobackor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum homo,', CAST(N'1972-07-29 16:17:32.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (890, N'Monbanommicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estis', CAST(N'1976-12-12 15:06:52.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (891, N'Tiperollor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo delerium.', CAST(N'1978-09-26 08:06:19.750' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (892, N'Grovenanantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis quo Et', CAST(N'1987-05-22 14:11:45.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (893, N'Lomerover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. regit,', CAST(N'1980-06-22 20:40:23.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (894, N'Endquestegantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. et', CAST(N'2000-11-27 21:22:58.820' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (895, N'Tuprobower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Quad eudis', CAST(N'1975-05-17 18:13:15.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (896, N'Thrusipefin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare et', CAST(N'1997-06-15 03:10:49.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (897, N'Surfropinentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit, Quad', CAST(N'1963-12-05 19:06:47.560' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (898, N'Qwitanantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'travissimantor', CAST(N'1973-01-25 16:45:41.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (899, N'Emfropex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id habitatio', CAST(N'1990-11-29 17:02:52.410' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (900, N'Rapfroponicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis', CAST(N'1989-08-01 07:48:03.290' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (901, N'Parpickentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum esset', CAST(N'1992-08-13 14:38:53.530' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (902, N'Doppebicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum Quad', CAST(N'1971-07-28 17:01:13.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (903, N'Embanamor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis funem.', CAST(N'2000-06-01 05:01:34.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (904, N'Uptumackantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro habitatio', CAST(N'1991-01-09 00:38:34.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (905, N'Endmunower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'cognitio,', CAST(N'1978-02-03 11:40:19.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (906, N'Partumower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, plorum', CAST(N'2002-02-03 19:55:21.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (907, N'Doptanexentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. ut in', CAST(N'1972-04-27 11:48:34.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (908, N'Haptinar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit,', CAST(N'1978-05-15 00:44:30.470' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (909, N'Partinexover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, non nomen', CAST(N'1972-10-28 06:27:04.060' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (910, N'Haprobopar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro et novum', CAST(N'1981-07-17 01:03:14.670' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (911, N'Dophupimax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'homo, et', CAST(N'2000-05-09 06:20:04.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (912, N'Fropickplex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. non', CAST(N'1992-09-08 00:23:41.390' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (913, N'Thrunipefan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans venit.', CAST(N'1981-07-30 05:14:19.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (914, N'Inwerpex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'volcans', CAST(N'1998-06-20 12:10:41.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (915, N'Grovenex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non rarendum et', CAST(N'1970-05-28 08:05:21.900' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (916, N'Suptinicin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, Longam,', CAST(N'1958-03-04 03:47:05.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (917, N'Hapwerpantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo quo, quo', CAST(N'1967-11-27 07:40:45.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (918, N'Emrobantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1967-05-29 23:00:45.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (919, N'Winrobegicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e trepicandor', CAST(N'1961-07-21 11:39:32.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (920, N'Frotumilantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plorum vantis.', CAST(N'2002-10-14 04:10:43.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (921, N'Rapquestantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vantis. Pro ut', CAST(N'1991-11-17 21:49:03.400' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (922, N'Klibanax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'venit. ut', CAST(N'1987-11-16 04:02:33.550' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (923, N'Doptinantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Et ut si Et quad', CAST(N'1980-12-20 14:30:04.180' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (924, N'Endzapommentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, nomen', CAST(N'2005-02-11 07:50:08.460' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (925, N'Renipimistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis pladior', CAST(N'1961-01-18 09:22:53.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (926, N'Zeetanamistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus in quoque', CAST(N'1963-01-24 20:36:05.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (927, N'Upnipopar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, quo,', CAST(N'2006-05-08 11:49:09.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (928, N'Rapdimistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis quad ut', CAST(N'1981-07-07 22:01:43.990' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (929, N'Intumplower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens Tam', CAST(N'1984-12-22 12:55:46.440' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (930, N'Parhupin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'homo, linguens', CAST(N'1993-04-10 14:56:05.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (931, N'Cipglibupar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'volcans nomen', CAST(N'1975-06-20 18:39:35.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (932, N'Uptumistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum eudis', CAST(N'2000-03-08 21:08:45.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (933, N'Klidimover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1988-03-30 02:20:00.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (934, N'Froerower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens non', CAST(N'1976-08-13 08:39:03.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (935, N'Unvenopicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. gravum', CAST(N'1972-03-05 09:07:02.010' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (936, N'Injubentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'glavans', CAST(N'2002-06-11 14:51:29.880' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (937, N'Surpickaquicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et plorum gravis', CAST(N'1979-02-04 00:52:30.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (938, N'Indimplicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen si', CAST(N'2000-10-02 09:14:22.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (939, N'Endsapaquower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit,', CAST(N'1996-03-04 19:12:04.040' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (940, N'Emdimistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Id essit. e', CAST(N'1968-01-26 04:13:56.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (941, N'Winkilackex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis fecit. si', CAST(N'1988-09-19 08:09:09.150' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (942, N'Winglibommazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut quad fecit.', CAST(N'1972-08-26 19:37:03.660' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (943, N'Cipdudex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'gravis Versus', CAST(N'2006-11-02 19:58:45.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (944, N'Qwidimackower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'funem. e funem.', CAST(N'1999-01-12 15:14:17.330' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (945, N'Kliwerpommex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'imaginator', CAST(N'1984-08-04 08:46:48.250' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (946, N'Trufropefax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono essit.', CAST(N'1994-01-17 07:42:38.360' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (947, N'Thrukilentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono nomen vobis', CAST(N'1958-10-24 08:57:25.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (948, N'Unvenackax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'delerium. Pro', CAST(N'1990-01-02 08:29:41.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (949, N'Adsapin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sed et eudis si', CAST(N'1983-07-04 13:17:36.920' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (950, N'Endbanazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo fecundio,', CAST(N'1970-03-16 14:26:09.730' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (951, N'Hapglibex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque bono', CAST(N'1982-07-20 19:30:04.680' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (952, N'Rapcadimentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quad venit. quad', CAST(N'2004-07-07 20:06:01.600' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (953, N'Kliquestor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1964-07-08 05:06:08.840' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (954, N'Barsipar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro non glavans', CAST(N'1996-02-15 12:52:08.430' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (955, N'Raprobefazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis eggredior.', CAST(N'2005-09-13 09:58:59.900' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (956, N'Lomkilazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e bono et', CAST(N'1957-09-29 10:36:19.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (957, N'Cipglibar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'pladior rarendum', CAST(N'1998-08-07 02:31:09.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (958, N'Trutanor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'et quo fecit.', CAST(N'2003-04-19 21:11:25.880' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (959, N'Supbanommistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quantare esset', CAST(N'1982-12-22 15:15:41.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (960, N'Doprobackan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro vobis Quad', CAST(N'1968-01-11 15:04:14.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (961, N'Rapjubewazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. et quartu', CAST(N'2003-09-28 08:06:11.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (962, N'Trurobex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. apparens', CAST(N'1998-10-20 13:46:50.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (963, N'Dopnipantor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis dolorum et', CAST(N'2001-05-22 11:48:44.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (964, N'Barrobover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis trepicandor', CAST(N'1987-07-19 15:30:58.210' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (965, N'Winjubonover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor e et', CAST(N'1959-01-28 06:23:38.810' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (966, N'Barzapin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si quad Pro Pro', CAST(N'1953-03-03 09:11:18.280' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (967, N'Qwijubamover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'plurissimum', CAST(N'1957-06-11 22:03:41.370' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (968, N'Resipollistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. novum', CAST(N'1957-12-17 20:35:16.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (969, N'Tupmunefentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis Versus', CAST(N'1979-04-18 03:03:39.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (970, N'Grojubedgar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'bono gravis quad', CAST(N'1975-12-16 17:13:52.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (971, N'Endtinommistor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Versus si', CAST(N'1987-01-28 02:54:34.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (972, N'Supdimazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. novum', CAST(N'1975-02-23 22:26:22.850' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (973, N'Undudamax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor', CAST(N'1979-03-26 03:48:41.930' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (974, N'Kliglibor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'dolorum novum', CAST(N'2001-06-25 07:25:35.490' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (975, N'Klinipex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'si manifestum', CAST(N'1967-01-25 20:01:34.830' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (976, N'Hapdimex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'manifestum', CAST(N'1972-03-24 02:11:59.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (977, N'Qwisipazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'nomen Longam, ut', CAST(N'1989-01-12 08:03:47.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (978, N'Haptinackan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'essit. nomen Pro', CAST(N'1989-12-07 23:15:46.860' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (979, N'Unerewor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'eggredior.', CAST(N'1973-07-10 23:03:20.200' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (980, N'Uptinexex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in quorum nomen', CAST(N'1975-01-01 01:40:14.650' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (981, N'Rapdudiman', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'parte egreddior', CAST(N'1957-04-03 11:38:39.220' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (982, N'Upglibax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo, sed pladior', CAST(N'1969-05-07 03:19:28.050' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (983, N'Frovenicazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor Pro', CAST(N'2006-01-03 08:44:07.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (984, N'Parcadimicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens in', CAST(N'1967-10-09 22:29:36.700' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (985, N'Endjubicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut estis volcans', CAST(N'1963-09-22 19:26:01.170' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (986, N'Upsapadax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'vobis essit.', CAST(N'1978-12-12 22:33:34.270' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (987, N'Groquestar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'apparens sed', CAST(N'1988-09-01 16:02:37.120' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (988, N'Lomtanackentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quoque in ut', CAST(N'2000-02-12 13:27:51.590' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (989, N'Monnipadover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Pro nomen', CAST(N'1978-11-29 22:40:03.530' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (990, N'Adweristor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'regit, novum', CAST(N'1967-06-07 22:30:42.030' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (991, N'Parjubicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'transit. nomen', CAST(N'1956-11-05 12:20:20.500' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (992, N'Grosapaquicator', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo et Quad', CAST(N'1983-10-17 03:41:04.790' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (993, N'Tupdudedar', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quo fecit.', CAST(N'1970-10-12 17:58:41.090' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (994, N'Rapbanedgex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'non linguens', CAST(N'1981-02-23 04:07:49.240' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (995, N'Raptanopover', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'brevens, Sed', CAST(N'1972-04-28 20:07:44.910' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (996, N'Haptumazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'fecit. transit.', CAST(N'1997-11-29 18:22:18.640' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (997, N'Trupebor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in fecit, quoque', CAST(N'1979-04-06 07:48:19.630' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (998, N'Emerower', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'novum quad', CAST(N'1972-04-17 04:27:47.580' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (999, N'Gromunamazz', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'quis pladior', CAST(N'1992-12-16 08:50:38.510' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1000, N'Zeesapupex', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'linguens', CAST(N'1984-06-03 23:35:29.970' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1001, N'Thruglibupan', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'estum. plorum', CAST(N'1989-08-08 09:38:56.980' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1002, N'Fronipin', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'ut plurissimum', CAST(N'1953-11-07 12:22:09.320' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1003, N'Tiprobentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'Multum in', CAST(N'1963-09-18 05:26:02.110' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1004, N'Qwibanedentor', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'in delerium.', CAST(N'1960-07-19 20:03:44.890' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1005, N'Rezapax', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'trepicandor Sed', CAST(N'1991-02-16 11:31:34.760' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1006, N'random', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'e', CAST(N'2010-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1007, N'add', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'1234235', CAST(N'2010-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1008, N'test', N'e5bb7f3f-f7df-4b2b-911f-b6ea5f4d85a9', N'test', CAST(N'2017-01-10 22:35:22.423' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1009, N'test', N'e5bb7f3f-f7df-4b2b-911f-b6ea5f4d85a9', N'test', CAST(N'2017-01-10 22:35:36.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1010, N'test1234', N'e5bb7f3f-f7df-4b2b-911f-b6ea5f4d85a9', N'test1', CAST(N'2017-01-10 22:38:56.687' AS DateTime), CAST(N'2017-01-10 23:25:22.263' AS DateTime), N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c')
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1011, N'test', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'test', CAST(N'2017-01-16 22:06:32.070' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1012, N'sdf', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'sdf', CAST(N'2017-01-16 22:09:46.080' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1013, N'1343edcf', N'e374ab46-067d-433d-a560-b866530de8fa', N'134', CAST(N'2017-01-16 22:15:13.480' AS DateTime), CAST(N'2017-01-16 22:15:27.970' AS DateTime), N'e374ab46-067d-433d-a560-b866530de8fa')
GO
INSERT [dbo].[Medicines] ([Id], [Name], [UserId], [Description], [AddedDate], [UpdateDate], [UpdateUserId]) VALUES (1014, N'243afgfasde', N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c', N'24323', CAST(N'2017-01-16 23:08:33.627' AS DateTime), CAST(N'2017-01-16 23:09:06.443' AS DateTime), N'a76bf83d-147c-4f90-9cd5-f7b81b3f2a6c')
GO
SET IDENTITY_INSERT [dbo].[Medicines] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 1/31/2017 11:01:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 1/31/2017 11:01:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 1/31/2017 11:01:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 1/31/2017 11:01:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 1/31/2017 11:01:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 1/31/2017 11:01:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Medicines]  WITH CHECK ADD  CONSTRAINT [FK_Medicines_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Medicines] CHECK CONSTRAINT [FK_Medicines_AspNetUsers]
GO
USE [master]
GO
ALTER DATABASE [HospitalPharmacy] SET  READ_WRITE 
GO
