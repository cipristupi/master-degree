﻿'use strict';

var app = angular.module('hospitalPharmacy', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar']);
app.config(function($routeProvider) {

    $routeProvider.when("/home",
    {
        controller: "homeController",
        templateUrl: "/app/views/home.html"
    });

    $routeProvider.when("/login",
    {
        controller: "loginController",
        templateUrl: "/app/views/login.html"
    });

    $routeProvider.when("/signup",
    {
        controller: "signupController",
        templateUrl: "/app/views/signup.html"
    });

    $routeProvider.when("/medicines",
    {
        controller: "medicinesController",
        templateUrl: "/app/views/medicines.html"
    });

    $routeProvider.when("/addMedicine",
    {
        controller: "medicinesController",
        templateUrl: "/app/views/addMedicine.html"
    });
    $routeProvider.when("/editMedicine/:id",
    {
        controller: "medicinesController",
        templateUrl: "/app/views/editMedicine.html"//,
        //method :'editMedicineLoad'
    });

    $routeProvider.otherwise({ redirectTo: "/home" });
});

//var serviceBase = 'http://hospitalpharmacy-web.local/';//TODO change here with localhost:xxxx
var serviceBase = 'http://localhost:6655/';//TODO change here with localhost:xxxx
app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'ngAuthApp'
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.run([
    'authService', function(authService) {
        authService.fillAuthData();
    }
]);