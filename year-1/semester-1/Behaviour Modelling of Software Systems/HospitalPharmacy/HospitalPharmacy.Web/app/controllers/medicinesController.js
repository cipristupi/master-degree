﻿'use strict';
app.controller('medicinesController',
[
    '$scope', 'medicinesService', '$timeout', '$routeParams', '$route', '$location','authService',
    function ($scope, medicinesService, $timeout, $routeParams, $route, $location, authService) {

        /*Object initialization on scope*/
        $scope.authentication = authService.authentication;

        $scope.addMedicineSavedSuccessfully = false;
        $scope.editMedicineSavedSuccessfully = false;

        $scope.addMedicineMessage = "";
        $scope.editMedicineMessage = "";

        $scope.medicines = [];

        $scope.medicine = {
            Name: "",
            AddedDate: "",
            Description: ""
        }

        $scope.editMedicine = {
            Id: "",
            Name: "",
            AddedDate: "",
            Description: "",
            UserId: ""
        }

        var redirectTimer = function() {
            var timer = $timeout(function() {
                    $timeout.cancel(timer);
                    $location.path('/medicines');
                },
                2000);
        }

        /*Authomaticaly executing function
         */
        medicinesService.getMedicines()
            .then(function(results) {
                    $scope.medicines = results.data;

                },
                function(error) {
                    alert(error.data.message);
                });

        var loadMedicine = function() {
            if ($route.current.templateUrl === '/app/views/editMedicine.html') {
                var id = $routeParams.id;
                medicinesService.getMedicineById(id)
                    .then(function(result) {
                        var data = result.data;
                        $scope.editMedicine.Name = data.name;
                        $scope.editMedicine.AddedDate = data.addedDate;
                        $scope.editMedicine.Description = data.description;
                        $scope.editMedicine.UserId = data.userId;
                        $scope.editMedicine.Id = data.id;
                    });
            }
        }

        loadMedicine();

        $scope.addMedicine = function() {
            medicinesService.addMedicine($scope.medicine)
                .then(function() {
                        $scope.addMedicineSavedSuccessfully = true;
                        $scope
                            .addMedicineMessage =
                            "Medicine has been added successfully, you will be redicted to medicine page in 2 seconds.";
                        redirectTimer();
                    },
                    function(response) {
                        //var errors = [];
                        //for (var key in response.data.modelState) {
                        //    for (var i = 0; i < response.data.modelState[key].length; i++) {
                        //        errors.push(response.data.modelState[key][i]);
                        //    }
                        //}
                        //$scope.message = "Failed to add medicine due to:" + errors.join(' ');
                        $scope.message = "Failed to add medicine";
                    });
        }

        $scope.editMedicineSave = function() {
            medicinesService.editMedicine($scope.editMedicine)
                .then(function() {
                        $scope.editMedicineSavedSuccessfully = true;
                        $scope
                            .editMedicineMessage =
                            "Medicine has been updated successfully, you will be redicted to medicine page in 2 seconds.";
                        redirectTimer();
                    },
                    function(response) {
                        //var errors = [];
                        //for (var key in response.data.modelState) {
                        //    for (var i = 0; i < response.data.modelState[key].length; i++) {
                        //        errors.push(response.data.modelState[key][i]);
                        //    }
                        //}
                        //$scope.message = "Failed to update medicine due to:" + errors.join(' ');
                        $scope.message = "Failed to update medicine";
                    });
        }

        $scope.deleteMedicine = function(id) {
            medicinesService.deleteMedicine(id)
                .then(function() {
                        $route.reload();
                    },
                    function() {

                    });
        }
    }
]);