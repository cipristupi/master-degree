﻿'use strict';
app.factory('medicinesService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var medicinesServiceFactory = {};

    var _getMedicines = function () {
        return $http.get(serviceBase + 'api/medicine').then(function (results) {
            return results;
        });
    };

    var _addMedicine =function(data) {
        return $http.post(serviceBase + 'api/medicine/add', data).then(function (response) {
            return response;
        });
    }

    var _getMedicineById = function(medicineId) {
        return $http.get(serviceBase + 'api/medicine/getById/' + medicineId).then(function (results) {
            return results;
        });
    }

    var _editMedicine = function (data) {
        return $http.post(serviceBase + 'api/medicine/edit', data).then(function (response) {
            return response;
        });
    }

    var _deleteMedicine = function(id) {
        return $http.post(serviceBase + 'api/medicine/deleteMedicine/'+id).then(function (response) {
            return response;
        });
    }

    medicinesServiceFactory.getMedicines= _getMedicines;
    medicinesServiceFactory.addMedicine = _addMedicine;
    medicinesServiceFactory.getMedicineById = _getMedicineById;
    medicinesServiceFactory.editMedicine = _editMedicine;
    medicinesServiceFactory.deleteMedicine = _deleteMedicine;

    return medicinesServiceFactory;

}]);