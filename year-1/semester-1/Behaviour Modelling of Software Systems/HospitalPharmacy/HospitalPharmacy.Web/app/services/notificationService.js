﻿'use strict';
app.factory('notificationService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var notificationServiceFactory = {};

    var notificationHubProxy = null;

    var _notificationInitializer = function() {
        $.connection.hub.logging = true;
        notificationHubProxy = $.connection.notificationHub;

        notificationHubProxy.client.hello = function () {
            console.log("Hello from ASP.NET Web API");
        };

        $.connection.hub.start().done(function () {
            console.log("started");
        }).fail(function (result) {
            console.log(result);
        });
    }

    notificationServiceFactory.notificationInitializer = _notificationInitializer;


    return notificationServiceFactory;

}]);