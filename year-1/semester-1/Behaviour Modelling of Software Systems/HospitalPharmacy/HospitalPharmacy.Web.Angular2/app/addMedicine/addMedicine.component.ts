import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, MedicineService } from '../_services/index';


@Component({
    moduleId: module.id,
    templateUrl: 'addMedicine.component.html'
})

export class AddMedicineComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private medicineService: MedicineService,
        private alertService: AlertService) { }

    addMedicine() {
        this.loading = true;
        this.medicineService.create(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Add successful', true);
                    this.router.navigate(['/home']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
