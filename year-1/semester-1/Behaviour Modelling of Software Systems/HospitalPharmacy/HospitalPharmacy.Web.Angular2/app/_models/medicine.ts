export class Medicine {
    id: number;
    name: string;
    addedDate: string;
    description: string;
    userId: string;
}