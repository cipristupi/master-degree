"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var index_1 = require("../_services/index");
var EditMedicineComponent = (function () {
    function EditMedicineComponent(router, medicineService, alertService, activatedRoute) {
        this.router = router;
        this.medicineService = medicineService;
        this.alertService = alertService;
        this.activatedRoute = activatedRoute;
        this.model = {};
        this.loading = false;
    }
    EditMedicineComponent.prototype.ngOnInit = function () {
        this.loadMedicine();
    };
    EditMedicineComponent.prototype.editMedicine = function () {
        var _this = this;
        this.loading = true;
        this.medicineService.update(this.model)
            .subscribe(function (data) {
            _this.alertService.success('Update successful', true);
            _this.router.navigate(['/home']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    EditMedicineComponent.prototype.loadMedicine = function () {
        var _this = this;
        var id = this.activatedRoute.snapshot.params['id'];
        this.medicineService.getById(id)
            .subscribe(function (data) {
            _this.model = data;
            //this.alertService.success('Update successful', true);
            //this.router.navigate(['/editMedicine']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    return EditMedicineComponent;
}());
EditMedicineComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'editMedicine.component.html'
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.MedicineService,
        index_1.AlertService,
        router_2.ActivatedRoute])
], EditMedicineComponent);
exports.EditMedicineComponent = EditMedicineComponent;
//# sourceMappingURL=editMedicine.component.js.map