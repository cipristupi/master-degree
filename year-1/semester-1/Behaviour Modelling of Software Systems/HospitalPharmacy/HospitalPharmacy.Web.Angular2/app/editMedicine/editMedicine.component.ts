import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


import { AlertService, MedicineService } from '../_services/index';


@Component({
    moduleId: module.id,
    templateUrl: 'editMedicine.component.html'
})

export class EditMedicineComponent implements OnInit {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private medicineService: MedicineService,
        private alertService: AlertService,
        private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.loadMedicine();
    }

    editMedicine() {
        this.loading = true;
        this.medicineService.update(this.model)
            .subscribe(
            data => {
                this.alertService.success('Update successful', true);
                this.router.navigate(['/home']);
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
    }

    loadMedicine() {
        var id = this.activatedRoute.snapshot.params['id'];
        this.medicineService.getById(id)
            .subscribe(
            data => {
                this.model = data;
                //this.alertService.success('Update successful', true);
                //this.router.navigate(['/editMedicine']);
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
    }
}
