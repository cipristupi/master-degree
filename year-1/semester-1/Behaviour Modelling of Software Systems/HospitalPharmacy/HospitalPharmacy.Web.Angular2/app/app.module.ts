﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers/index';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { AlertService, AuthenticationService, MedicineService, UserService } from './_services/index';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { AddMedicineComponent } from './addMedicine/index';
import { EditMedicineComponent } from './editMedicine/index';
import { ChannelService, ChannelConfig, SignalrWindow } from "./_services/channel.service";
import { AppSettings } from './_helpers/index'

let channelConfig = new ChannelConfig();
channelConfig.url = AppSettings.API_ENDPOINT+"/signalr";
channelConfig.hubName = "NotificationHub";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        AddMedicineComponent,
        EditMedicineComponent
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        MedicineService,
        UserService,

        // providers used to create fake backend
        //fakeBackendProvider,
        //MockBackend,
        BaseRequestOptions,

        ChannelService,
        { provide: SignalrWindow, useValue: window },
        { provide: 'channel.config', useValue: channelConfig }
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }