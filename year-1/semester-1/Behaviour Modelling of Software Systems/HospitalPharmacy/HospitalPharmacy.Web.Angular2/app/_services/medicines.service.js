"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var index_1 = require("../_helpers/index");
var MedicineService = (function () {
    function MedicineService(http) {
        this.http = http;
    }
    MedicineService.prototype.getAll = function () {
        return this.http.get(index_1.AppSettings.API_ENDPOINT + '/api/medicine', this.token()).map(function (response) { return response.json(); });
    };
    MedicineService.prototype.getById = function (id) {
        return this.http.get(index_1.AppSettings.API_ENDPOINT + '/api/medicine/getById/' + id, this.token()).map(function (response) { return response.json(); });
    };
    MedicineService.prototype.create = function (medicine) {
        return this.http.post(index_1.AppSettings.API_ENDPOINT + '/api/medicine/add', medicine, this.token()).map(function (response) { return response.json(); });
    };
    MedicineService.prototype.update = function (medicine) {
        return this.http.post(index_1.AppSettings.API_ENDPOINT + '/api/medicine/edit', medicine, this.token()).map(function (response) { return response.json(); });
    };
    MedicineService.prototype.delete = function (id) {
        return this.http.post(index_1.AppSettings.API_ENDPOINT + '/api/medicine/deleteMedicine/' + id, this.token()).map(function (response) { return response.json(); });
    };
    // private helper methods
    MedicineService.prototype.token = function () {
        // create authorization header with jwt token
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.access_token) {
            var headers = new http_1.Headers({ 'Authorization': 'Bearer ' + currentUser.access_token });
            return new http_1.RequestOptions({ headers: headers });
        }
    };
    return MedicineService;
}());
MedicineService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], MedicineService);
exports.MedicineService = MedicineService;
//# sourceMappingURL=medicines.service.js.map