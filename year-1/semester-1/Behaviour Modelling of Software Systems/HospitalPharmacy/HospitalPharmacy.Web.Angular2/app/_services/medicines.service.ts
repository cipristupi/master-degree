import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Medicine } from '../_models/index';
import {AppSettings} from '../_helpers/index'

@Injectable()
export class MedicineService {
    constructor(private http: Http) { }

    getAll() {
        return this.http.get(AppSettings.API_ENDPOINT+'/api/medicine', this.token()).map((response: Response) => response.json());
    }

    getById(id: number) {
        return this.http.get(AppSettings.API_ENDPOINT+'/api/medicine/getById/' + id, this.token()).map((response: Response) => response.json());
    }

    create(medicine: Medicine) {
        return this.http.post(AppSettings.API_ENDPOINT+'/api/medicine/add', medicine, this.token()).map((response: Response) => response.json());
    }

    update(medicine: Medicine) {
        return this.http.post(AppSettings.API_ENDPOINT+'/api/medicine/edit', medicine, this.token()).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.post(AppSettings.API_ENDPOINT+'/api/medicine/deleteMedicine/' + id, this.token()).map((response: Response) => response.json());
    }

    // private helper methods

    private token() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.access_token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.access_token });
            return new RequestOptions({ headers: headers });
        }
    }
}