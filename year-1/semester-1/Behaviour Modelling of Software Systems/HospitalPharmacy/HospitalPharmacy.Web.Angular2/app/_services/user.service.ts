﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User } from '../_models/index';
import {AppSettings} from '../_helpers/index'

@Injectable()
export class UserService {
    constructor(private http: Http) { }

    getAll() {
        return this.http.get(AppSettings.API_ENDPOINT+'/api/users', this.jwt()).map((response: Response) => response.json());
    }

    getById(id: number) {
        return this.http.get(AppSettings.API_ENDPOINT+'/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    create(user: User) {
        var response = this.http.post(AppSettings.API_ENDPOINT+'/api/account/register', user, this.jwt());
        return response.map((response: Response) => response.json());
    }

    update(user: User) {
        return this.http.put(AppSettings.API_ENDPOINT+'/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete(AppSettings.API_ENDPOINT+'/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    // private helper methods

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.access_token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.access_token });
            return new RequestOptions({ headers: headers });
        }
    }
}