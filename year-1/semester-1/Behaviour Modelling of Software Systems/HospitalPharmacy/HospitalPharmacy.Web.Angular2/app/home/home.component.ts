﻿import { Component, OnInit, Input } from '@angular/core';

import { User } from '../_models/index';
import { Medicine } from '../_models/index';
import { MedicineService } from '../_services/index';
import { UserService } from '../_services/index';
import { ChannelService, ChannelEvent } from "../_services/index";

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    medicines: Medicine[] = [];
    notifications: any = [];

    messages = "";
    private channel = "notification";


    constructor(private userService: UserService, private medicineService: MedicineService, private channelService: ChannelService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.loadAllMedicines();
        this.bindToSignalR();
    }

    deleteMedicine(id: number) {
        this.medicineService.delete(id).subscribe(() => { this.loadAllMedicines() });
    }
    clearNotifications() {
        for (let item of this.notifications) {
            item = "";
        }
        this.notifications = [];
    }

    private bindToSignalR() {
        // Get an observable for events emitted on this channel
        //
        this.channelService.sub(this.channel).subscribe(
            (x: ChannelEvent) => {
                this.appendStatusUpdate(x);
            },
            (error: any) => {
                console.warn("Attempt to join channel failed!", error);
            }
        )
    }
    private loadAllMedicines() {
        this.medicineService.getAll().subscribe(medicines => { this.medicines = medicines; });
    }

    private appendStatusUpdate(ev: ChannelEvent): void {
        // Just prepend this to the messages string shown in the textarea
        //
        var finalMessage = '';
        let date = new Date();
        switch (ev.Data.State) {
            case "starting": {
                finalMessage = `${date.toLocaleTimeString()} : starting\n` + this.messages;
                this.notifications.push(finalMessage)
                console.log(finalMessage);
                //this.messages = `${date.toLocaleTimeString()} : starting\n` + this.messages;
                break;
            }

            case "complete": {
                finalMessage = `${date.toLocaleTimeString()} : complete\n` + this.messages;
                this.notifications.push(finalMessage)
                console.log(finalMessage);
                // console.log(`${date.toLocaleTimeString()} : complete\n` + this.messages);
                // this.messages = `${date.toLocaleTimeString()} : complete\n` + this.messages;
                break;
            }

            default: {
                finalMessage = `${date.toLocaleTimeString()} : ${JSON.stringify(ev.Data)} \n` + this.messages;
                this.notifications.push(finalMessage)
                console.log(finalMessage);
                // console.log(`${date.toLocaleTimeString()} : ${ev.Data} \n` + this.messages);
                // this.messages = `${date.toLocaleTimeString()} : ${ev.Data} \n` + this.messages;
            }
        }
    }
}