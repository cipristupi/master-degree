"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var index_1 = require("../_services/index");
var index_2 = require("../_services/index");
var index_3 = require("../_services/index");
var HomeComponent = (function () {
    function HomeComponent(userService, medicineService, channelService) {
        this.userService = userService;
        this.medicineService = medicineService;
        this.channelService = channelService;
        this.users = [];
        this.medicines = [];
        this.notifications = [];
        this.messages = "";
        this.channel = "notification";
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.loadAllMedicines();
        this.bindToSignalR();
    };
    HomeComponent.prototype.deleteMedicine = function (id) {
        var _this = this;
        this.medicineService.delete(id).subscribe(function () { _this.loadAllMedicines(); });
    };
    HomeComponent.prototype.clearNotifications = function () {
        for (var _i = 0, _a = this.notifications; _i < _a.length; _i++) {
            var item = _a[_i];
            item = "";
        }
        this.notifications = [];
    };
    HomeComponent.prototype.bindToSignalR = function () {
        var _this = this;
        // Get an observable for events emitted on this channel
        //
        this.channelService.sub(this.channel).subscribe(function (x) {
            _this.appendStatusUpdate(x);
        }, function (error) {
            console.warn("Attempt to join channel failed!", error);
        });
    };
    HomeComponent.prototype.loadAllMedicines = function () {
        var _this = this;
        this.medicineService.getAll().subscribe(function (medicines) { _this.medicines = medicines; });
    };
    HomeComponent.prototype.appendStatusUpdate = function (ev) {
        // Just prepend this to the messages string shown in the textarea
        //
        var finalMessage = '';
        var date = new Date();
        switch (ev.Data.State) {
            case "starting": {
                finalMessage = date.toLocaleTimeString() + " : starting\n" + this.messages;
                this.notifications.push(finalMessage);
                console.log(finalMessage);
                //this.messages = `${date.toLocaleTimeString()} : starting\n` + this.messages;
                break;
            }
            case "complete": {
                finalMessage = date.toLocaleTimeString() + " : complete\n" + this.messages;
                this.notifications.push(finalMessage);
                console.log(finalMessage);
                // console.log(`${date.toLocaleTimeString()} : complete\n` + this.messages);
                // this.messages = `${date.toLocaleTimeString()} : complete\n` + this.messages;
                break;
            }
            default: {
                finalMessage = date.toLocaleTimeString() + " : " + JSON.stringify(ev.Data) + " \n" + this.messages;
                this.notifications.push(finalMessage);
                console.log(finalMessage);
            }
        }
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'home.component.html'
    }),
    __metadata("design:paramtypes", [index_2.UserService, index_1.MedicineService, index_3.ChannelService])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map