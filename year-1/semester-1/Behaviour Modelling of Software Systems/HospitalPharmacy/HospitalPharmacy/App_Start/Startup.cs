﻿using System;
using System.Web.Http;
using ExpressMapper;
using HospitalPharmacy.Api;
using HospitalPharmacy.Api.Services;
using HospitalPharmacy.Models;
using HospitalPharmacy.Repository.EntityFrameworkRepositories;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace HospitalPharmacy.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            ConfigureOAuth(app);

            WebApiConfig.Register(config);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
                Provider = new SimpleAuthorizationServerProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        }

        public void RegisterMapper()
        {
            Mapper.Register<UserViewModel, AspNetUsers>();

            Mapper.Register<AspNetUsers, UserViewModel>();

            Mapper.Register<MedicineViewModel, Medicines>();

            Mapper.Register<Medicines, MedicineViewModel>();
        }

        public void CreateSignalRChannel()
        {
            //// Let's wire up a SignalR client here to easily inspect what
            ////  calls are happening
            ////
            //var hubConnection = new HubConnection();
            //IHubProxy eventHubProxy = hubConnection.CreateHubProxy("EventHub");
            //eventHubProxy.On<string, ChannelEvent>("OnEvent", (channel, ev) => Log.Information("Event received on {channel} channel - {@ev}", channel, ev));
            //hubConnection.Start().Wait();

            //// Join the channel for task updates in our console window
            ////
            //eventHubProxy.Invoke("Subscribe", Constants.AdminChannel);
        }
    }
}