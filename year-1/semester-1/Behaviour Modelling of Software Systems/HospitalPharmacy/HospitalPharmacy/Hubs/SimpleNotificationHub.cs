﻿using Microsoft.AspNet.SignalR;

namespace HospitalPharmacy.Api.Hubs
{
    public class SimpleNotificationHub : Hub
    {
        private static readonly IHubContext HubContext = GlobalHost.ConnectionManager.GetHubContext<SimpleNotificationHub>();

        public void Hello()
        {
            Clients.All.hello();
        }

        public static void SayHello()
        {
            HubContext.Clients.All.hello();
        }
    }
}