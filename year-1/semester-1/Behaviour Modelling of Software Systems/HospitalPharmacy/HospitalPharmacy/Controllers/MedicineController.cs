﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ExpressMapper.Extensions;
using HospitalPharmacy.Api.Hubs;
using HospitalPharmacy.Api.Models;
using HospitalPharmacy.Models;
using HospitalPharmacy.Repository.EntityFrameworkRepositories;
using Microsoft.AspNet.SignalR;

namespace HospitalPharmacy.Api.Controllers
{
    [RoutePrefix("api/Medicine")]
    public class MedicineController : ApiController
    {
        private readonly MedicineRepository _medicineRepository;
        private readonly AuthRepository _authRepository;
        private IHubContext _context;
     
        private string _channel = HospitalPharmacyConstants.NotificationChannel;

        public MedicineController()
        {
            _medicineRepository = new MedicineRepository();
            _authRepository = new AuthRepository();
            _context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
        }

        [System.Web.Http.Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            List<MedicineViewModel> medicinesViewModel;
            try
            {
                var medicines = _medicineRepository.GetAll();
                medicinesViewModel =
                    medicines.Select(x => x.Map(new MedicineViewModel())).OrderByDescending(x => x.AddedDate).ToList();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            object eventDetails = new Dictionary<string, string>()
            {
                {"MethodName", nameof(Get)},
                {"User", GetCurrentUsername()}
            };
            PublishEvent(nameof(Get), eventDetails);
            return Ok(medicinesViewModel);
        }

        [System.Web.Http.Authorize]
        [Route("Add")]
        [HttpPost]
        public IHttpActionResult Add(MedicineViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var userId = User.Identity.Name;
                var medicine = model.Map(new Medicines());
                medicine.UserId = userId;
                medicine.AddedDate = DateTime.Now;
                _medicineRepository.Add(medicine);
                object eventDetails = new Dictionary<string, string>()
                {
                    {"MethodName", nameof(Add)},
                    {"User", GetCurrentUsername()}
                };
                PublishEvent(nameof(Add), eventDetails);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            //notify clients
            return Ok(model);
        }

        [System.Web.Http.Authorize]
        [Route("GetById/{id}")]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            MedicineViewModel medicineViewModel = null;
            try
            {
                var medicine = _medicineRepository.GetById(id);
                medicineViewModel = medicine.Map(new MedicineViewModel());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            object eventDetails = new Dictionary<string, string>()
            {
                {"MethodName", nameof(GetById)},
                {"User", GetCurrentUsername()}
            };
            PublishEvent(nameof(GetById), eventDetails);
            return Ok(medicineViewModel);
        }


        [System.Web.Http.Authorize]
        [Route("Edit")]
        [HttpPost]
        public IHttpActionResult Edit(MedicineViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var userId = User.Identity.Name;
                var medicine = model.Map(new Medicines());
                medicine.UpdateDate = DateTime.Now;
                medicine.UpdateUserId = userId;
                _medicineRepository.Update(medicine);
                object eventDetails = new Dictionary<string, string>()
                {
                    {"MethodName", nameof(Edit)},
                    {"User", GetCurrentUsername()}
                };
                PublishEvent(nameof(Edit), eventDetails);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            //notify clients
            return Ok();
        }

        [System.Web.Http.Authorize]
        [Route("DeleteMedicine/{id}")]
        [HttpPost]
        public IHttpActionResult DeleteMedicine(int id)
        {
            try
            {
                var medicine = _medicineRepository.GetById(id);
                _medicineRepository.Delete(medicine);
                object eventDetails = new Dictionary<string, string>()
                {
                    {"MethodName", nameof(DeleteMedicine)},
                    {"User", GetCurrentUsername()}
                };
                PublishEvent(nameof(DeleteMedicine), eventDetails);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            //notify clients
            return Ok();
        }


        private void PublishEvent(string eventName, object eventDetails)
        {
            // From .NET code like this we can't invoke the methods that
            //  exist on our actual Hub class...because we only have a proxy
            //  to it. So to publish the event we need to call the method that
            //  the clients will be listening on.
            //
            _context.Clients.Group(_channel).OnEvent(HospitalPharmacyConstants.NotificationChannel, new ChannelEvent
            {
                ChannelName = HospitalPharmacyConstants.NotificationChannel,
                Name = eventName,
                Data = eventDetails
            });
        }

        private string GetCurrentUsername()
        {
            var userId = User.Identity.Name;
            var user = _authRepository.FindUserById(userId);
            return user?.UserName;
        }
    }
}
