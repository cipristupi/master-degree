﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace HospitalPharmacy.Repository.EntityFrameworkRepositories
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("AuthContext")
        {

        }
    }
}