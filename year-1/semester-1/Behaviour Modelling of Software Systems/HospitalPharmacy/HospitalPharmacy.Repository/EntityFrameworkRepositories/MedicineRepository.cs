﻿using System;
using System.Collections.Generic;
using System.Linq;
using HospitalPharmacy.Repository.Interfaces;

namespace HospitalPharmacy.Repository.EntityFrameworkRepositories
{
    public class MedicineRepository : IRepository<Medicines>
    {
        private readonly HospitalPharmacyEntities _context;
        public MedicineRepository()
        {
            _context = new HospitalPharmacyEntities();
        }
        public void Add(Medicines model)
        {
            try
            {
                _context.Medicines.Add(model);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        public void Update(Medicines model)
        {
            try
            {
                var entity = _context.Medicines.Find(model.Id);
                if (entity == null)
                {
                    return;
                }
                _context.Entry(entity).CurrentValues.SetValues(model);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public IEnumerable<Medicines> GetAll()
        {
            return _context.Medicines.Include("AspNetUsers");
        }

        public void Delete(Medicines model)
        {
            try
            {
                _context.Medicines.Remove(model);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Medicines GetById(int id)
        {
            return _context.Medicines.FirstOrDefault(x => x.Id == id);
        }
    }
}
