﻿using System.Collections.Generic;

namespace HospitalPharmacy.Repository.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Add(T model);
        void Update(T model);
        IEnumerable<T> GetAll();
        void Delete(T model);
    }
}