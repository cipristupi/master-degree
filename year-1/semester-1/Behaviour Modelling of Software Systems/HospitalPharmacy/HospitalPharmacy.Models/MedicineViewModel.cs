﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HospitalPharmacy.Models
{
    public class MedicineViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string UserId { get; set; }

        public string FullName { get; set; }

        public DateTime? AddedDate { get; set; }

        public string Description { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string UpdateUserId { get; set; }
    }
}