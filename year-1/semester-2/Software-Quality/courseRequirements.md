# Laboratory Assignments
General remarks:
- For all tasks you will need a medium size application of your own. I recommend to use your bachelor degree thesis.
- every assignment should be presented at laboratory (only exceptional cases will be accepted for delivering the task through e-mail)
- you may use any tool that fits the designated role; however see the SQTools for recommended tools for your labs and project.


Lab 1: Choose a Code review tool and apply it for your app. Your task is to produce a report about the use of the chosen tool. Results for your app, advantages, disadvantages

Deadline: deliver at lab 2.

Lab 2: Choose a software metrics tool and apply it for your app. Your task is to produce a report about the use of the chosen tool (which metrics have been computed and their relevance). Results for your app, advantages, disadvantages

Deadline: deliver at lab 3.

Rest of the labs are dedicated for your project. The project report must include:

- Establish used SQ model, and establish software quality factors or subfactors that will be traced. Argue with regard to the characteristics of the application. (to be delivered in lab 4)
- Presentation of the strategy for evaluation/measurement of the selected factors: used criteria, used methodologies.
- Results and interpretation.
- For 1 factor you should use a tool (see the recommended list)

The report will be presented at the last seminar.