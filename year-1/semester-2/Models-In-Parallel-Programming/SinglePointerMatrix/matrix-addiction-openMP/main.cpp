/*
Compute the addition of Matrix A with B and the result will be saved in C.
A+B = C, where A,B,C matrices
*/
#include <iostream>
#include <fstream>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <omp.h>
#include<chrono>
#include <string>
using namespace std;

ifstream finA("matrixA.txt");
ifstream finB("matrixB.txt");
ofstream foutC("matrixC.txt");

ostringstream oss;


int *matrixA;
int *matrixB;
int *matrixC;

struct timeval  tv1, tv2;

struct rowsPair
{
    int startRowNo;
    int endRowNo;
};

int rowsNumber, columnsNumber, threadsNumber;
int programPID;
#define LOGNAME_FORMAT "log/%Y%m%d_%H%M%S"

#define LOGNAME_SIZE 50

FILE *logfile(void)
{
    static char name[LOGNAME_SIZE];

    time_t now = time(0);

    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));

    sprintf(name, "%s_%d_%d_%d_%d", name,rowsNumber,columnsNumber,threadsNumber,programPID);

    return fopen(name, "ab");
}
FILE *file;//Log file instance;
char* logMessage;

void ReadGenericInformation();//Read general information about matrices from file
void AllocateMatrices();//Allocate memory for the matrices based on general information
void ReadMatrices();//Read matrices from files
void ThreadsHandler();//Method to handle thread creation
void OpenMPThreadsHandler();//Method to handle thread creation with openMP
void PrintMatrixResult();// Print resulted matrix in file

char* GetFormattedTime(time_t timeToFormat);//Get formatted time
void LogMessage(char* message,int logFile,int logConsole, int logTime);//Log a given message in file, console. If logTime= 1 then append current time
char* BuildMessageFromStringAndInt(char* message, int number);//Create a message in which a int is appended
void PrintMatrices();//Print matrices on console
void DebugMethod();//Method used for debug purpose
void ReleaseMethod();//Main flow for the program

int main(int argc, char *argv[])
{
    auto start = std::chrono::high_resolution_clock::now();
    programPID = getpid();

    if(argc <=1)
    {
        printf("You did not feed me arguments, I will die now :( ...");
        getchar();
        exit(1);
    }

    rowsNumber =  atoi(argv[1]);
    columnsNumber =  atoi(argv[2]);
    threadsNumber =  atoi(argv[3]);

    ReleaseMethod();

    auto elapsed = std::chrono::high_resolution_clock::now() - start;

    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

    long long milliseconds= std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();

    long long seconds = std::chrono::duration_cast<std::chrono::seconds>(elapsed).count();

    ostringstream ss;
    ss<< "Seconds:" << seconds << " | Milliseconds:" << milliseconds << " | Microseconds:"<< microseconds <<" | PID:"<< programPID;

    string timeInMicroSeconds = ss.str();

    LogMessage((char*)timeInMicroSeconds.c_str(),1,0,0);

    fclose(file);

    return 0;
}

void ReleaseMethod()
{
    file = logfile();

    AllocateMatrices();

    ReadMatrices();

    OpenMPThreadsHandler();

    PrintMatrixResult();

    fclose(file);
}

//{--Basic Read/Write Methods for matrices
void ReadGenericInformation()
{
    /*LogMessage("In ReadGenericInformation",1,1,1);

    finGeneric >> rowsNumber;
    finGeneric >> columnsNumber;
    finGeneric >> threadsNumber;

    LogMessage(BuildMessageFromStringAndInt("Row: ", rowsNumber),1,1,0);
    LogMessage(BuildMessageFromStringAndInt("Columns: ", columnsNumber),1,1,0);
    LogMessage(BuildMessageFromStringAndInt("Threads: ", threadsNumber),1,1,0);*/

}

void AllocateMatrices()
{
    matrixA = (int *)malloc(rowsNumber * columnsNumber * sizeof(int));
    matrixB = (int *)malloc(rowsNumber * columnsNumber * sizeof(int));
    matrixC = (int *)malloc(rowsNumber * columnsNumber * sizeof(int));
}

void ReadMatrices()
{
    for(int row=0; row<rowsNumber; row++)
    {
        for(int column=0; column<columnsNumber; column++)
        {
            finA>>*(matrixA + row*columnsNumber + column);
            finB>>*(matrixB + row*columnsNumber + column);
        }
    }
}

void PrintMatrixResult()
{
    ofstream myfile;
    static char name[256];

    sprintf(name, "matrixC_%d_%d_%d_%d.txt", rowsNumber,columnsNumber,threadsNumber,programPID);
    myfile.open (name);

    for(int row=0; row<rowsNumber; row++)
    {
        for(int column=0; column<columnsNumber; column++)
        {
            myfile<<*(matrixC + row*columnsNumber +column)<< " ";
        }
        myfile<<endl;
    }
    myfile<<endl;
    myfile.close();
}

void PrintMatrices()
{
    for(int row=0; row<rowsNumber; row++)
    {
        for(int column=0; column<columnsNumber; column++)
        {
            cout<<"matrixA["<<row<<"]["<<column<<"]="<<*(matrixA + row*columnsNumber +column)<< " ";
        }
        cout<<endl;
    }
    cout<<endl;

    for(int row=0; row<rowsNumber; row++)
    {
        for(int column=0; column<columnsNumber; column++)
        {
            cout<<"matrixB["<<row<<"]["<<column<<"]="<<*(matrixB + row*columnsNumber +column)<< " ";
        }
        cout<<endl;
    }
    cout<<endl;

    for(int row=0; row<rowsNumber; row++)
    {
        for(int column=0; column<columnsNumber; column++)
        {
            cout<<"matrixC["<<row<<"]["<<column<<"]="<<*(matrixC + row*columnsNumber +column)<< " ";
        }
        cout<<endl;
    }
    cout<<endl;
}

//}--end block

//{ --OpenMP
void ComputeMatricesSumOpenMP(int threadId, int rowsPerThread, int rowsDeviation)
{
    int startRow, endRow;

    startRow = threadId * rowsPerThread;
    endRow = startRow + rowsPerThread;
    if(threadId+1 == threadsNumber)//here I know that is the last thread
    {
        endRow+=rowsDeviation;
    }
    if(endRow > rowsNumber)
    {
        endRow = rowsNumber;
    }

    for(int row=startRow; row<endRow; row++)
    {
        for(int column=0; column<columnsNumber; column++)
        {
            *(matrixC + row *columnsNumber +column)= *(matrixA + row *columnsNumber +column)+ *(matrixB + row *columnsNumber +column);
        }
    }
}

void OpenMPThreadsHandler()
{
    if(threadsNumber == 0 || threadsNumber == 1)
    {
        omp_set_num_threads(0);
        int threadId = omp_get_thread_num();
        ComputeMatricesSumOpenMP(threadId,rowsNumber,0);
    }
    else
    {
        int numbersOfRowsPerThread = rowsNumber/threadsNumber;

        int deviation = rowsNumber % threadsNumber;

        omp_set_num_threads(threadsNumber);

        #pragma omp parallel
        {
            int threadId = omp_get_thread_num();
            #pragma omp critical
            ComputeMatricesSumOpenMP(threadId,numbersOfRowsPerThread,deviation);
        }
    }
}

//}--



//{-- Logging methods
void LogMessage(char* message,int logFile,int logConsole, int logTime)
{
    char* tempMessage;
    if(logTime == 1)
    {
        char* tempMessage = (char*)malloc(sizeof(char)*256);
        time_t currentTime;
        currentTime = time(0);
        memset(tempMessage, 0, sizeof tempMessage);
        sprintf(tempMessage, "%s %s \r\n", message, GetFormattedTime(currentTime));
        if(logFile == 1)
        {
            fprintf(file, "%s \r\n", tempMessage);
        }
        if(logConsole==1)
        {
            cout<<tempMessage << endl;
        }
        free(tempMessage);
        return;
    }
    if(logFile == 1)
    {
        fprintf(file, "%s \r\n", message);
    }
    if(logConsole==1)
    {
        cout<<message << endl;
    }
}

char *GetFormattedTime(time_t timeToFormat)
{
    char* buffer = (char*)malloc(sizeof(char)*26);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", localtime(&timeToFormat));
    return buffer;
}

char* BuildMessageFromStringAndInt(char* message, int number)
{
    char* tempMessage = (char*)malloc(sizeof(char)*256);
    memset(tempMessage, 0, sizeof tempMessage);
    sprintf(tempMessage, "%s %d", message, number);
    return tempMessage;
}

//} -- as a block end
