/*
Compute the multiplication of Matrix A with B and the result will be saved in C.
A*B = C, where A,B,C matrices
*/
#include <iostream>
#include <fstream>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <omp.h>
using namespace std;

ifstream finA("matrixA.txt");
ifstream finB("matrixB.txt");
ifstream finGeneric("generic.txt");
ofstream foutC("matrixC.txt");

ostringstream oss;

unsigned long long matrixASum=0;
unsigned long long matrixBSum=0;
unsigned long long matrixCSum=0;

int *matrixA;
int *matrixB;
int *matrixC;

struct rowsPair
{
    int startRowNo;
    int endRowNo;
};

int rowsNumber, columnsNumber, threadsNumber;
int matrixARowsNumber, matrixAColumnsNumber;//Matrix A
int matrixBRowsNumber, matrixBColumnsNumbers;//Matrix B
int matrixCRowsNumber, matrixCColumnsNumbers;//Matrix C
#define LOGNAME_FORMAT "log/%Y%m%d_%H%M%S"

#define LOGNAME_SIZE 256

FILE *logfile(void)
{
    static char name[LOGNAME_SIZE];

    time_t now = time(0);

    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));

    //Experiment
    sprintf(name, "%s_%d_%d_%d", name,rowsNumber,columnsNumber,threadsNumber);

    return fopen(name, "ab");
}

FILE *file;//Log file instance;
char* logMessage;

void ReadGenericInformation();//Read general information about matrices from file
void AllocateMatrices();//Allocate memory for the matrices based on general information
void ReadMatrices();//Read matrices from files
void OpenMPThreadsHandler();//Method to handle thread creation with openMP
void PrintMatrixResult();// Print resulted matrix in file

char* GetFormattedTime(time_t timeToFormat);//Get formatted time
void LogMessage(char* message,int logFile,int logConsole, int logTime);//Log a given message in file, console. If logTime= 1 then append current time
char* BuildMessageFromStringAndInt(char* message, int number);//Create a message in which a int is appended
void PrintMatrices();//Print matrices on console
void DebugMethod();//Method used for debug purpose
void ReleaseMethod();//Main flow for the program

int main(int argc, char *argv[])
{

    if(argc <=1)
    {
        printf("You did not feed me arguments, I will die now :( ...");
        getchar();
        exit(1);
    }
    //matrixA
    matrixARowsNumber =  atoi(argv[1]);
    matrixAColumnsNumber =  atoi(argv[2]);

    threadsNumber =  atoi(argv[3]);

    matrixBRowsNumber = matrixAColumnsNumber;
    matrixBColumnsNumbers = matrixARowsNumber;

    matrixCRowsNumber = matrixARowsNumber;
    matrixCColumnsNumbers= matrixBColumnsNumbers;
    //DebugMethod();

    ReleaseMethod();

    return 0;
}

void DebugMethod()
{
    file = logfile();
    logMessage = (char*)malloc(sizeof(char)*256);

    //ReadGenericInformation();

    AllocateMatrices();

    ReadMatrices();

    PrintMatrices();

    fclose(file);
    getchar();
}

void ReleaseMethod()
{
    clock_t start, endTime;

    start = clock();

    file = logfile();

    logMessage = (char*)malloc(sizeof(char)*256);

    time_t currentTime;

    currentTime = time(0); // Get the system time

    fprintf(file, "START TIME: %s \r\n", GetFormattedTime(currentTime));

    memset(logMessage, 0, sizeof(logMessage));
    sprintf(logMessage, "PID: %d \r\n", getpid());

    LogMessage(logMessage,1,1,0);

    AllocateMatrices();

    ReadMatrices();

    OpenMPThreadsHandler();

    PrintMatrixResult();

    currentTime = time(0); // Get the system time
    fprintf(file, "END TIME: %s \r\n", GetFormattedTime(currentTime));

    endTime = clock();
    double msecs;
    msecs = ((double) (endTime - start)) * 1000 / CLOCKS_PER_SEC;

    memset(logMessage, 0, sizeof logMessage);
    sprintf(logMessage, "Execution Time: %f \r\n", msecs/1000);
    LogMessage(logMessage,1,1,0);

    fclose(file);

    //getchar();
}

//{--Basic Read/Write Methods for matrices
void ReadGenericInformation()
{
    LogMessage("In ReadGenericInformation",1,1,1);

    finGeneric >> rowsNumber;
    finGeneric >> columnsNumber;
    finGeneric >> threadsNumber;

    LogMessage(BuildMessageFromStringAndInt("Row: ", rowsNumber),1,1,0);
    LogMessage(BuildMessageFromStringAndInt("Columns: ", columnsNumber),1,1,0);
    LogMessage(BuildMessageFromStringAndInt("Threads: ", threadsNumber),1,1,0);

    LogMessage("Out ReadGenericInformation",1,1,1);
}

void AllocateMatrices()
{
    LogMessage("In AllocateMatrices",1,1,1);

    matrixA = (int *)malloc(matrixARowsNumber * matrixAColumnsNumber * sizeof(int));
    matrixB = (int *)malloc(matrixBRowsNumber * matrixBColumnsNumbers * sizeof(int));
    matrixC = (int *)malloc(matrixCRowsNumber * matrixCColumnsNumbers * sizeof(int));

    LogMessage("Out AllocateMatrices",1,1,1);
}

void ReadMatrices()
{
    LogMessage("In ReadMatrices",1,1,1);

    for(int row=0; row<matrixARowsNumber; row++)
    {
        LogMessage(BuildMessageFromStringAndInt("Matrix A Row :", row),1,1,1);
        for(int column=0; column<matrixAColumnsNumber; column++)
        {
            finA>>*(matrixA + row*matrixAColumnsNumber + column);
        }
    }

    for(int row=0; row<matrixBRowsNumber; row++)
    {
        LogMessage(BuildMessageFromStringAndInt("Matrix B Row :", row),1,1,1);
        for(int column=0; column<matrixBColumnsNumbers; column++)
        {
            finB>>*(matrixB + row*matrixBColumnsNumbers + column);
        }
    }

    LogMessage("Out ReadMatrices",1,1,1);
}

void PrintMatrixResult()
{
    LogMessage("In PrintMatrixResult",1,1,1);

    for(int row=0; row<matrixCRowsNumber; row++)
    {
        for(int column=0; column<matrixCColumnsNumbers; column++)
        {
            foutC<<*(matrixC + row*matrixCColumnsNumbers +column)<< " ";
        }
        foutC<<endl;
    }
    foutC<<endl;

    LogMessage("Out PrintMatrixResult",1,1,1);
}

void PrintMatrices()
{
    for(int row=0; row<matrixARowsNumber; row++)
    {
        for(int column=0; column<matrixAColumnsNumber; column++)
        {
            cout<<"matrixA["<<row<<"]["<<column<<"]="<<*(matrixA + row*matrixAColumnsNumber +column)<< " ";
        }
        cout<<endl;
    }
    cout<<endl;

    for(int row=0; row<matrixBRowsNumber; row++)
    {
        for(int column=0; column<matrixBColumnsNumbers; column++)
        {
            cout<<"matrixB["<<row<<"]["<<column<<"]="<<*(matrixB + row*matrixBColumnsNumbers +column)<< " ";
        }
        cout<<endl;
    }
    cout<<endl;

    for(int row=0; row<matrixCRowsNumber; row++)
    {
        for(int column=0; column<matrixCColumnsNumbers; column++)
        {
            cout<<"matrixC["<<row<<"]["<<column<<"]="<<*(matrixC + row*matrixCColumnsNumbers +column)<< " ";
        }
        cout<<endl;
    }
    cout<<endl;
}

//}--end block

//{ --OpenMP

void ComputeMatricesMultiplication(int threadId, int rowsPerThread, int rowsDeviation)
{
    int startRow, endRow;
    LogMessage("In ComputeMatricesMultiplication",1,1,1);

    startRow = threadId * rowsPerThread;
    endRow = startRow + rowsPerThread;
    if(threadId+1 == threadsNumber)//here I know that is the last thread
    {
        endRow+=rowsDeviation;
    }
    if(endRow > rowsNumber)
    {
        endRow = rowsNumber;
    }

    char* msg = (char*)malloc(sizeof(char)*256);
    sprintf(msg, "Thread Id %d, Start row %d, End row %d \r\n",threadId, startRow, endRow);
    LogMessage(msg,1,1,0);
    free(msg);

    int matrixAElement;
    int matrixBElement;

    for(int row=startRow; row<endRow; row++)//Matrix A rows
    {
        for(int column=0; column<matrixAColumnsNumber-1; column++)//Matrix A Columns
        {
            *(matrixC + row*matrixCColumnsNumbers +column) = 0;
            for(int columnK=0; columnK<matrixAColumnsNumber; columnK++)
            {
                matrixAElement = *(matrixA + row*matrixAColumnsNumber +columnK);
                matrixBElement = *(matrixB + columnK*matrixBColumnsNumbers +column);
                *(matrixC + row*matrixCColumnsNumbers +column)= *(matrixC + row*matrixCColumnsNumbers +column) + matrixAElement * matrixBElement;
            }
        }
    }
    LogMessage("Out ComputeMatricesMultiplication",1,1,1);
}

void OpenMPThreadsHandler()
{
    LogMessage("In OpenMPThreadsHandler",1,1,1);

    if(threadsNumber == 0 || threadsNumber == 1)
    {
        omp_set_num_threads(0);
        int threadId = omp_get_thread_num();
        ComputeMatricesMultiplication(threadId,matrixARowsNumber, 0);
    }
    else
    {
        int numbersOfRowsPerThread = matrixARowsNumber/threadsNumber;

        int deviation = matrixARowsNumber % threadsNumber;

        omp_set_num_threads(threadsNumber);
        #pragma omp parallel
        {
            int threadId = omp_get_thread_num();
            #pragma omp critical
            ComputeMatricesMultiplication(threadId,numbersOfRowsPerThread,deviation);
        }
    }
    LogMessage("Out OpenMPThreadsHandler",1,1,1);
}

//}--

//{-- Logging methods
void LogMessage(char* message,int logFile,int logConsole, int logTime)
{
    char* tempMessage;
    if(logTime == 1)
    {
        char* tempMessage = (char*)malloc(sizeof(char)*256);
        time_t currentTime;
        currentTime = time(0);
        memset(tempMessage, 0, sizeof tempMessage);
        sprintf(tempMessage, "%s %s \r\n", message, GetFormattedTime(currentTime));
        if(logFile == 1)
        {
            fprintf(file, "%s \r\n", tempMessage);
        }
        if(logConsole==1)
        {
            cout<<tempMessage << endl;
        }
        free(tempMessage);
        return;
    }
    if(logFile == 1)
    {
        fprintf(file, "%s \r\n", message);
    }
    if(logConsole==1)
    {
        cout<<message << endl;
    }
}

char *GetFormattedTime(time_t timeToFormat)
{
    char* buffer = (char*)malloc(sizeof(char)*26);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", localtime(&timeToFormat));
    return buffer;
}

char* BuildMessageFromStringAndInt(char* message, int number)
{
    char* tempMessage = (char*)malloc(sizeof(char)*256);
    memset(tempMessage, 0, sizeof tempMessage);
    sprintf(tempMessage, "%s %d", message, number);
    return tempMessage;
}

//} -- as a block ender
