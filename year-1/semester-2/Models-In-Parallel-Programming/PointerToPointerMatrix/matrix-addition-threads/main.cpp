/*
Compute the addition of Matrix A with B and the result will be saved in C.
A+B = C, where A,B,C matrices
*/
#include <iostream>
#include <fstream>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <chrono>
#include <string>
using namespace std;

ifstream finA("matrixA.txt");
ifstream finB("matrixB.txt");

ostringstream oss;

int **matrixA;
int **matrixB;
int **matrixC;

struct rowsPair
{
    int startRowNo;
    int endRowNo;
};

int rowsNumber, columnsNumber, threadsNumber;
int programPID;
#define LOGNAME_FORMAT "log/%Y%m%d_%H%M%S"
#define LOGNAME_SIZE 50

FILE *logfile(void)
{
    static char name[LOGNAME_SIZE];
    time_t now = time(0);
    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));

    sprintf(name, "%s_%d_%d_%d_%d", name, rowsNumber, columnsNumber, threadsNumber, programPID);

    return fopen(name, "ab");
}
FILE *file;
char *logMessage;

void ReadGenericInformation();
void AllocateMatrices();
void ReadMatrices();
void ThreadsHandler();
void PrintMatrixResult();
char *GetFormattedTime(time_t timeToFormat);
void LogMessage(char *message, int logFile, int logConsole, int logTime);
char *BuildMessageFromStringAndInt(char *message, int number);
void PrintMatrices();
void ReleaseMethod();

int main(int argc, char *argv[])
{
    auto start = std::chrono::high_resolution_clock::now();
    programPID = getpid();

    if (argc <= 1)
    {
        printf("You did not feed me arguments, I will die now :( ...");
        getchar();
        exit(1);
    }
    rowsNumber = atoi(argv[1]);
    columnsNumber = atoi(argv[2]);
    threadsNumber = atoi(argv[3]);
    ReleaseMethod();

    auto elapsed = std::chrono::high_resolution_clock::now() - start;

    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

    long long milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();

    long long seconds = std::chrono::duration_cast<std::chrono::seconds>(elapsed).count();

    ostringstream ss;
    ss << "Seconds:" << seconds << " | Milliseconds:" << milliseconds << " | Microseconds:" << microseconds << " | PID:" << programPID;

    string timeInMicroSeconds = ss.str();

    LogMessage((char *)timeInMicroSeconds.c_str(), 1, 0, 0);

    fclose(file);

    return 0;
}

void ReleaseMethod()
{
    file = logfile();
    logMessage = (char *)malloc(sizeof(char) * 256);

    AllocateMatrices();

    ReadMatrices();

    //PrintMatrices();
    ThreadsHandler();

    PrintMatrixResult();
}

char *GetFormattedTime(time_t timeToFormat)
{
    char *buffer = (char *)malloc(sizeof(char) * 26);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", localtime(&timeToFormat));
    return buffer;
}

void ReadGenericInformation()
{
    /*LogMessage("In ReadGenericInformation",1,1,1);

    finGeneric >> rowsNumber;
    finGeneric >> columnsNumber;
    finGeneric >> threadsNumber;

    LogMessage("Out ReadGenericInformation",1,1,1);*/
}

void AllocateMatrices()
{
    /*matrixA = (int *)malloc(rowsNumber * columnsNumber * sizeof(int));
    matrixB = (int *)malloc(rowsNumber * columnsNumber * sizeof(int));
    matrixC = (int *)malloc(rowsNumber * columnsNumber * sizeof(int));*/

    matrixA = new int *[rowsNumber];
    matrixB = new int *[rowsNumber];
    matrixC = new int *[rowsNumber];
    for (int i = 0; i < rowsNumber; ++i)
    {
        matrixA[i] = new int[columnsNumber];
        matrixB[i] = new int[columnsNumber];
        matrixC[i] = new int[columnsNumber];
    }
}

void ReadMatrices()
{
    for (int row = 0; row < rowsNumber; row++)
    {
        for (int column = 0; column < columnsNumber; column++)
        {
            /*finA>>*(matrixA + row*columnsNumber + column);
            finB>>*(matrixB + row*columnsNumber + column);*/
            finA >> matrixA[row][column];
            finB >> matrixB[row][column];
        }
    }
}

void *ComputeMatricesSumThread(void *arg)
{
    pthread_t self_id;
    self_id = pthread_self();

    struct rowsPair *p = (struct rowsPair *)arg;
    int startRow = p->startRowNo;
    int endRow = p->endRowNo;

    for (int row = startRow; row < endRow; row++)
    {
        for (int column = 0; column < columnsNumber; column++)
        {
            //*(matrixC + row*columnsNumber +column)= *(matrixA + row*columnsNumber +column)+ *(matrixB + row*columnsNumber +column);
            matrixC[row][column] = matrixA[row][column] + matrixB[row][column];
        }
    }

    return NULL;
}

void PrintMatrices()
{
    for (int row = 0; row < rowsNumber; row++)
    {
        for (int column = 0; column < columnsNumber; column++)
        {
            //cout<<"matrixA["<<row<<"]["<<column<<"]="<<*(matrixA + row*columnsNumber +column)<< " ";
            cout << "matrixA[" << row << "][" << column << "]=" << matrixA[row][column] << " ";
        }
        cout << endl;
    }
    cout << endl;

    for (int row = 0; row < rowsNumber; row++)
    {
        for (int column = 0; column < columnsNumber; column++)
        {
            //cout<<"matrixB["<<row<<"]["<<column<<"]="<<*(matrixB + row*columnsNumber +column)<< " ";
            cout << "matrixB[" << row << "][" << column << "]=" << matrixB[row][column] << " ";
        }
        cout << endl;
    }
    cout << endl;

    for (int row = 0; row < rowsNumber; row++)
    {
        for (int column = 0; column < columnsNumber; column++)
        {
            //cout<<"matrixC["<<row<<"]["<<column<<"]="<<*(matrixC + row*columnsNumber +column)<< " ";
            cout << "matrixC[" << row << "][" << column << "]=" << matrixC[row][column] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void ComputeMatricesSum(int startRow, int endRow)
{
    for (int row = startRow; row < endRow; row++)
    {
        for (int column = 0; column < columnsNumber; column++)
        {
            //*(matrixC + row *columnsNumber +column)= *(matrixA + row *columnsNumber +column)+ *(matrixB + row *columnsNumber +column);
            matrixC[row][column] = matrixA[row][column] + matrixB[row][column];
        }
    }
}

void ThreadsHandler()
{
    if (threadsNumber == 0 || threadsNumber == 1)
    {
        ComputeMatricesSum(0, rowsNumber);
    }
    else
    {
        pthread_t thr[threadsNumber];       //handler
        struct rowsPair arg[threadsNumber]; //store data in different memory locations
        int startRowIndex = 0;
        int endRowIndex = 0;
        int numbersOfRowsPerThread = rowsNumber / threadsNumber;

        int deviation = rowsNumber % threadsNumber;

        for (int i = 0; i < threadsNumber; i++)
        {
            arg[i].startRowNo = startRowIndex;

            if (i + 1 == threadsNumber) //here I know that is the last thread
            {
                endRowIndex += deviation;
            }

            endRowIndex += numbersOfRowsPerThread;

            if (endRowIndex < rowsNumber)
            {
                arg[i].endRowNo = endRowIndex;
            }
            else
            {
                arg[i].endRowNo = rowsNumber;
            }
            startRowIndex += numbersOfRowsPerThread;
            pthread_create(&thr[i], NULL, ComputeMatricesSumThread, &arg[i]);
        }

        for (int i = 0; i < threadsNumber; i++)
        {
            pthread_join(thr[i], NULL);
        }
    }
}

/*template <typename T>
  string NumberToString ( T Number )
  {
     ostringstream ss;
     ss << Number;
     return ss.str();
  }*/

//{-- Logging methods
void LogMessage(char *message, int logFile, int logConsole, int logTime)
{
    char *tempMessage;
    if (logTime == 1)
    {
        char *tempMessage = (char *)malloc(sizeof(char) * 256);
        time_t currentTime;
        currentTime = time(0);
        memset(tempMessage, 0, sizeof tempMessage);
        sprintf(tempMessage, "%s %s \r\n", message, GetFormattedTime(currentTime));
        if (logFile == 1)
        {
            fprintf(file, "%s \r\n", tempMessage);
        }
        if (logConsole == 1)
        {
            cout << tempMessage << endl;
        }
        free(tempMessage);
        return;
    }
    if (logFile == 1)
    {
        fprintf(file, "%s \r\n", message);
    }
    if (logConsole == 1)
    {
        cout << message << endl;
    }
}

char *BuildMessageFromStringAndInt(char *message, int number)
{
    char *tempMessage = (char *)malloc(sizeof(char) * 256);
    memset(tempMessage, 0, sizeof(tempMessage));
    sprintf(tempMessage, "%s %d", message, number);
    return tempMessage;
}

//}
