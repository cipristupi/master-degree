/*
Compute the multiplication of Matrix A with B and the result will be saved in C.
A*B = C, where A,B,C matrices
*/
#include <iostream>
#include <fstream>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <omp.h>
#include<chrono>
#include <string>
using namespace std;

ifstream finA("matrixA.txt");
ifstream finB("matrixB.txt");

ostringstream oss;

int** matrixA;
int** matrixB;
int** matrixC;

struct rowsPair
{
    int startRowNo;
    int endRowNo;
};

int rowsNumber, columnsNumber, threadsNumber;
int matrixARowsNumber, matrixAColumnsNumber;//Matrix A
int matrixBRowsNumber, matrixBColumnsNumbers;//Matrix B
int matrixCRowsNumber, matrixCColumnsNumbers;//Matrix C
int programPID;

#define LOGNAME_FORMAT "log/%Y%m%d_%H%M%S"

#define LOGNAME_SIZE 256

FILE *logfile(void)
{
    static char name[LOGNAME_SIZE];

    time_t now = time(0);

    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));

    sprintf(name, "%s_%d_%d_%d_%d", name,matrixCRowsNumber,matrixCColumnsNumbers,threadsNumber,programPID);

    return fopen(name, "ab");
}

FILE *file;//Log file instance;
char* logMessage;

void ReadGenericInformation();//Read general information about matrices from file
void AllocateMatrices();//Allocate memory for the matrices based on general information
void ReadMatrices();//Read matrices from files
void ThreadsHandler();//Method to handle thread creation
void PrintMatrixResult();// Print resulted matrix in file

char* GetFormattedTime(time_t timeToFormat);//Get formatted time
void LogMessage(char* message,int logFile,int logConsole, int logTime);//Log a given message in file, console. If logTime= 1 then append current time
char* BuildMessageFromStringAndInt(char* message, int number);//Create a message in which a int is appended
void PrintMatrices();//Print matrices on console
void DebugMethod();//Method used for debug purpose
void ReleaseMethod();//Main flow for the program

int main(int argc, char *argv[])
{
    auto start = std::chrono::high_resolution_clock::now();
    programPID = getpid();

    if(argc <=1)
    {
        printf("You did not feed me arguments, I will die now :( ...");
        getchar();
        exit(1);
    }

    //matrixA
    matrixARowsNumber =  atoi(argv[1]);
    matrixAColumnsNumber =  atoi(argv[2]);

    threadsNumber =  atoi(argv[3]);

    matrixBRowsNumber = matrixAColumnsNumber;
    matrixBColumnsNumbers = matrixARowsNumber;

    matrixCRowsNumber = matrixARowsNumber;
    matrixCColumnsNumbers= matrixBColumnsNumbers;

    ReleaseMethod();

    auto elapsed = std::chrono::high_resolution_clock::now() - start;

    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

    long long milliseconds= std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();

    long long seconds = std::chrono::duration_cast<std::chrono::seconds>(elapsed).count();

    ostringstream ss;
    ss<< "Seconds:" << seconds << " | Milliseconds:" << milliseconds << " | Microseconds:"<< microseconds <<" | PID:"<< programPID;

    string timeInMicroSeconds = ss.str();

    LogMessage((char*)timeInMicroSeconds.c_str(),1,0,0);

    fclose(file);
    return 0;
}

void ReleaseMethod()
{

    file = logfile();

    AllocateMatrices();

    ReadMatrices();

    ThreadsHandler();

    PrintMatrixResult();
}

//{--Basic Read/Write Methods for matrices
void ReadGenericInformation()
{
    /*LogMessage("In ReadGenericInformation",1,1,1);

    finGeneric >> rowsNumber;
    finGeneric >> columnsNumber;
    finGeneric >> threadsNumber;

    LogMessage(BuildMessageFromStringAndInt("Row: ", rowsNumber),1,1,0);
    LogMessage(BuildMessageFromStringAndInt("Columns: ", columnsNumber),1,1,0);
    LogMessage(BuildMessageFromStringAndInt("Threads: ", threadsNumber),1,1,0);

    LogMessage("Out ReadGenericInformation",1,1,1);*/
}

void AllocateMatrices()
{
    /*matrixA = (int *)malloc(matrixARowsNumber * matrixAColumnsNumber * sizeof(int));
    matrixB = (int *)malloc(matrixBRowsNumber * matrixBColumnsNumbers * sizeof(int));
    matrixC = (int *)malloc(matrixCRowsNumber * matrixCColumnsNumbers * sizeof(int));*/

    matrixA = new int*[matrixARowsNumber];
    matrixB = new int*[matrixBRowsNumber];
    matrixC = new int*[matrixCRowsNumber];

    for(int i = 0; i < matrixARowsNumber; ++i)
    {
        matrixA[i] = new int[matrixAColumnsNumber];
    }

    for(int i = 0; i < matrixBRowsNumber; ++i)
    {
        matrixB[i] = new int[matrixBColumnsNumbers];
    }

    for(int i = 0; i < matrixCRowsNumber; ++i)
    {
        matrixC[i] = new int[matrixCColumnsNumbers];
    }
}

void ReadMatrices()
{
    for(int row=0; row<matrixARowsNumber; row++)
    {
        for(int column=0; column<matrixAColumnsNumber; column++)
        {
            //finA>>*(matrixA + row*matrixAColumnsNumber + column);
            finA>>matrixA[row][column];
        }
    }

    for(int row=0; row<matrixBRowsNumber; row++)
    {
        for(int column=0; column<matrixBColumnsNumbers; column++)
        {
            //finB>>*(matrixB + row*matrixBColumnsNumbers + column);
            finB>>matrixB[row][column];
        }
    }
}

void PrintMatrixResult()
{
    ofstream myfile;
    static char name[256];

    sprintf(name, "matrixC_%d_%d_%d_%d.txt", matrixCRowsNumber,matrixCColumnsNumbers,threadsNumber,programPID);
    myfile.open (name);

    for(int row=0; row<matrixCRowsNumber; row++)
    {
        for(int column=0; column<matrixCColumnsNumbers; column++)
        {
            //myfile<<*(matrixC + row*matrixCColumnsNumbers +column)<< " ";
            myfile<<matrixC[row][column]<<" ";
        }
        myfile<<endl;
    }
    myfile<<endl;
    myfile.close();

}

void PrintMatrices()
{
    for(int row=0; row<matrixARowsNumber; row++)
    {
        for(int column=0; column<matrixAColumnsNumber; column++)
        {
            //cout<<"matrixA["<<row<<"]["<<column<<"]="<<*(matrixA + row*matrixAColumnsNumber +column)<< " ";
            cout<<"matrixA["<<row<<"]["<<column<<"]="<<matrixA[row][column]<< " ";
        }
        cout<<endl;
    }
    cout<<endl;

    for(int row=0; row<matrixBRowsNumber; row++)
    {
        for(int column=0; column<matrixBColumnsNumbers; column++)
        {
            //cout<<"matrixB["<<row<<"]["<<column<<"]="<<*(matrixB + row*matrixBColumnsNumbers +column)<< " ";
            cout<<"matrixB["<<row<<"]["<<column<<"]="<<matrixB[row][column]<< " ";
        }
        cout<<endl;
    }
    cout<<endl;

    for(int row=0; row<matrixCRowsNumber; row++)
    {
        for(int column=0; column<matrixCColumnsNumbers; column++)
        {
            //cout<<"matrixC["<<row<<"]["<<column<<"]="<<*(matrixC + row*matrixCColumnsNumbers +column)<< " ";
            cout<<"matrixC["<<row<<"]["<<column<<"]="<<matrixC[row][column]<< " ";
        }
        cout<<endl;
    }
    cout<<endl;
}

//}--end block

//{--Threads
void* ComputeMatricesMultiplicationThread(void* arg)
{
    pthread_t self_id;
    self_id = pthread_self();

    struct rowsPair* p = (struct rowsPair*)arg;
    int startRow = p->startRowNo;
    int endRow = p->endRowNo;

    int matrixAElement;
    int matrixBElement;

    for(int row=startRow; row<endRow; row++)//Matrix A rows
    {
        for(int column=0; column<matrixAColumnsNumber-1; column++)//Matrix A Columns
        {
            /*
            *(matrixC + row*matrixCColumnsNumbers +column) = 0;
            for(int columnK=0; columnK<matrixAColumnsNumber; columnK++)
            {
                matrixAElement = *(matrixA + row*matrixAColumnsNumber +columnK);
                matrixBElement = *(matrixB + columnK*matrixBColumnsNumbers +column);
                *(matrixC + row*matrixCColumnsNumbers +column)= *(matrixC + row*matrixCColumnsNumbers +column) + matrixAElement * matrixBElement;
            }*/
            matrixC[row][column]=0;
            for(int columnK=0; columnK<matrixAColumnsNumber; columnK++)
            {
                /*matrixAElement = *(matrixA + row*matrixAColumnsNumber +columnK);
                matrixBElement = *(matrixB + columnK*matrixBColumnsNumbers +column);*/
                matrixAElement = matrixA[row][columnK];
                matrixBElement = matrixB[columnK][column];
                matrixC[row][column] = matrixC[row][column] + matrixAElement * matrixBElement;
                //*(matrixC + row*matrixCColumnsNumbers +column)= *(matrixC + row*matrixCColumnsNumbers +column) + matrixAElement * matrixBElement;
            }
        }
    }

    return NULL;
}


void ComputeMatricesMultiplication(int startRow, int endRow)
{
    int matrixAElement;
    int matrixBElement;

    for(int row=startRow; row<endRow; row++)//Matrix A rows
    {
        for(int column=0; column<matrixAColumnsNumber-1; column++)//Matrix A Columns
        {
            /*
            *(matrixC + row*matrixCColumnsNumbers +column) = 0;
            for(int columnK=0; columnK<matrixAColumnsNumber; columnK++)
            {
                matrixAElement = *(matrixA + row*matrixAColumnsNumber +columnK);
                matrixBElement = *(matrixB + columnK*matrixBColumnsNumbers +column);
                *(matrixC + row*matrixCColumnsNumbers +column)= *(matrixC + row*matrixCColumnsNumbers +column) + matrixAElement * matrixBElement;
            }*/
            matrixC[row][column]=0;
            for(int columnK=0; columnK<matrixAColumnsNumber; columnK++)
            {
                /*matrixAElement = *(matrixA + row*matrixAColumnsNumber +columnK);
                matrixBElement = *(matrixB + columnK*matrixBColumnsNumbers +column);*/
                matrixAElement = matrixA[row][columnK];
                matrixBElement = matrixB[columnK][column];
                matrixC[row][column] = matrixC[row][column] + matrixAElement * matrixBElement;
                //*(matrixC + row*matrixCColumnsNumbers +column)= *(matrixC + row*matrixCColumnsNumbers +column) + matrixAElement * matrixBElement;
            }
        }
    }

}


void ThreadsHandler()
{
    if(threadsNumber == 0 || threadsNumber == 1)
    {

        ComputeMatricesMultiplication(0, matrixARowsNumber);
    }
    else
    {
        pthread_t thr[threadsNumber]; //handler
        struct rowsPair arg[threadsNumber]; //store data in different memory locations
        int startRowIndex = 0;
        int endRowIndex=0;
        int numbersOfRowsPerThread = matrixARowsNumber/threadsNumber;

        int defiation = rowsNumber % threadsNumber;

        for(int i=0; i<threadsNumber; i++)
        {

            arg[i].startRowNo=startRowIndex;

            if(i+1 == threadsNumber)//here I know that is the last thread
            {
                endRowIndex+=defiation;
            }

            endRowIndex+=numbersOfRowsPerThread;

            if(endRowIndex<matrixARowsNumber)
            {
                arg[i].endRowNo=endRowIndex;
            }
            else
            {
                arg[i].endRowNo=matrixARowsNumber;
            }
            startRowIndex+=numbersOfRowsPerThread;
            pthread_create(&thr[i],NULL,ComputeMatricesMultiplicationThread,&arg[i]);
        }

        for(int i=0; i<threadsNumber; i++)
        {
            pthread_join(thr[i],NULL);
        }
    }
}

//}--

//{-- Logging methods
void LogMessage(char* message,int logFile,int logConsole, int logTime)
{
    char* tempMessage;
    if(logTime == 1)
    {
        char* tempMessage = (char*)malloc(sizeof(char)*256);
        time_t currentTime;
        currentTime = time(0);
        memset(tempMessage, 0, sizeof tempMessage);
        sprintf(tempMessage, "%s %s \r\n", message, GetFormattedTime(currentTime));
        if(logFile == 1)
        {
            fprintf(file, "%s \r\n", tempMessage);
        }
        if(logConsole==1)
        {
            cout<<tempMessage << endl;
        }
        free(tempMessage);
        return;
    }
    if(logFile == 1)
    {
        fprintf(file, "%s \r\n", message);
    }
    if(logConsole==1)
    {
        cout<<message << endl;
    }
}

char *GetFormattedTime(time_t timeToFormat)
{
    char* buffer = (char*)malloc(sizeof(char)*26);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", localtime(&timeToFormat));
    return buffer;
}

char* BuildMessageFromStringAndInt(char* message, int number)
{
    char* tempMessage = (char*)malloc(sizeof(char)*256);
    memset(tempMessage, 0, sizeof tempMessage);
    sprintf(tempMessage, "%s %d", message, number);
    return tempMessage;
}

//} -- as a block ender
