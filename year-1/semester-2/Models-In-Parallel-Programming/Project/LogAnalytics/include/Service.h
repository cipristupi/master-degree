#ifndef SERVICE_H
#define SERVICE_H
#include "Log.h"
#include "Operation.h"
#include<algorithm>
#include<string>
#include<vector>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

class Service
{
    public:
        Service();
        virtual ~Service();
        vector<Log> ProcessData(vector<Log> logs, vector<Operation> operations,string resultFileName);
    protected:
    private:
        bool SearchOperation(Log log, Operation operation);
        vector<Log> SamplingOperation(vector<Log> logs, Operation operation);
        vector<Log> OrderOperation(vector<Log> logs, Operation operation);
        vector<Operation> GetOperationsByType(vector<Operation> operations, string type);
        bool InVector(Log element, vector<Log> elements);
};

#endif // SERVICE_H
