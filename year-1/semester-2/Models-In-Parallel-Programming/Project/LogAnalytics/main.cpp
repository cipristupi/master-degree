#include <iostream>
#include<algorithm>
#include<string>
#include<vector>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/Repository.h"
#include "include/Log.h"
#include "include/Service.h"
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <chrono>
#include <string>
using namespace std;


#define LOGNAME_FORMAT "log/%Y%m%d_%H%M%S"
#define LOGNAME_SIZE 50
FILE *file;
char *logMessage;
int programPID;
string operationsFileName;

FILE *logfile(void)
{
    static char name[LOGNAME_SIZE];
    time_t now = time(0);
    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));

    sprintf(name, "%s_%d_%s", name, programPID,operationsFileName.c_str());

    return fopen(name, "ab");
}

void PrintResult(vector<Log> logs);
char *GetFormattedTime(time_t timeToFormat);
void LogMessage(char *message, int logFile, int logConsole, int logTime);
char *BuildMessageFromStringAndInt(char *message, int number);

int main(int argc, char *argv[])
{
    auto start = std::chrono::high_resolution_clock::now();
    programPID = getpid();

    if (argc <= 1)
    {
        printf("You did not feed me arguments, I will die now :( ...");
        getchar();
        exit(1);
    }

    string logFileName = argv[1];
    operationsFileName = argv[2];

    file = logfile();
    logMessage = (char *)malloc(sizeof(char) * 256);


    Repository *repository = new Repository(logFileName,operationsFileName);
    Service *service=new Service();

    vector<Log> logs = repository->GetAll();
    vector<Operation> operations = repository->GetAllOperations();
    vector<Log> result = service->ProcessData(logs,operations,"");

    PrintResult(result);

    auto elapsed = std::chrono::high_resolution_clock::now() - start;

    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

    long long milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();

    long long seconds = std::chrono::duration_cast<std::chrono::seconds>(elapsed).count();

    ostringstream ss;
    ss << "Seconds:" << seconds << " | Milliseconds:" << milliseconds << " | Microseconds:" << microseconds << " | PID:" << programPID;

    string timeInMicroSeconds = ss.str();

    LogMessage((char *)timeInMicroSeconds.c_str(), 1, 0, 0);

    fclose(file);
    return 0;
}


void PrintResult(vector<Log> logs)
{
    ofstream myfile;
    static char name[256];

    sprintf(name, "logs_result_%s_%d.txt",operationsFileName.c_str(),programPID);
    myfile.open (name);


    for(int row=0; row<logs.size(); row++)
    {
        myfile<<logs[row];
    }
    myfile<<endl;
    myfile.close();
}

char *GetFormattedTime(time_t timeToFormat)
{
    char *buffer = (char *)malloc(sizeof(char) * 26);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", localtime(&timeToFormat));
    return buffer;
}

void LogMessage(char *message, int logFile, int logConsole, int logTime)
{
    char *tempMessage;
    if (logTime == 1)
    {
        char *tempMessage = (char *)malloc(sizeof(char) * 256);
        time_t currentTime;
        currentTime = time(0);
        memset(tempMessage, 0, sizeof tempMessage);
        sprintf(tempMessage, "%s %s \r\n", message, GetFormattedTime(currentTime));
        if (logFile == 1)
        {
            fprintf(file, "%s \r\n", tempMessage);
        }
        if (logConsole == 1)
        {
            cout << tempMessage << endl;
        }
        free(tempMessage);
        return;
    }
    if (logFile == 1)
    {
        fprintf(file, "%s \r\n", message);
    }
    if (logConsole == 1)
    {
        cout << message << endl;
    }
}

char *BuildMessageFromStringAndInt(char *message, int number)
{
    char *tempMessage = (char *)malloc(sizeof(char) * 256);
    memset(tempMessage, 0, sizeof(tempMessage));
    sprintf(tempMessage, "%s %d", message, number);
    return tempMessage;
}
