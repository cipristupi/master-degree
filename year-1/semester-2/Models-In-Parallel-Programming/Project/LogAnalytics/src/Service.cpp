#include "Service.h"

Service::Service()
{
    //ctor
}

Service::~Service()
{
    //dtor
}

vector<Log> Service::ProcessData(vector<Log> logs, vector<Operation> operations,string resultFileName)
{
    vector<Operation> searchOperations = GetOperationsByType(operations,"search");
    vector<Operation> orderOperation = GetOperationsByType(operations,"order");

    vector<Log> result;

    //Search operations
    for(unsigned int logIndex=0; logIndex<logs.size(); logIndex++)
    {
        Log currentLog = logs[logIndex];
        for(unsigned int operationIndex=0; operationIndex < searchOperations.size(); operationIndex++)
        {
            Operation currentOperation = searchOperations[operationIndex];
            if(SearchOperation(currentLog,currentOperation)==true)
            {
                //if(InVector(currentLog,result)==false){
                    result.push_back(currentLog);
                    break;
                //}
            }
        }
    }

    //Order Operations
    for(unsigned int operationIndex=0; operationIndex < orderOperation.size(); operationIndex++)
    {
        Operation currentOperation = orderOperation[operationIndex];
        result = OrderOperation(result,currentOperation);
    }
    return result;
}

bool Service::SearchOperation(Log log, Operation operation)
{
    string operationType = operation.GetoperationType();
    string property = operation.GetpropertyOnWhichIsApplied();
    string value = operation.Getvalue();
    if(property == "id")
    {
        string idString = std::to_string(log.GetId());
        //if(idString == value)
        if(strstr(idString.c_str(),value.c_str()) != NULL)
        {
            return true;
        }
    }
    if(property == "timestamp")
    {
        if(strstr(log.GetTimestamp().c_str(),value.c_str()) != NULL)
        {
            return true;
        }
    }

    if(property == "itemType")
    {

        if(strstr(log.GetItemType().c_str(),value.c_str()) != NULL)
        {
            return true;
        }
    }

    if(property == "name")
    {
        if(strstr(log.GetName().c_str(),value.c_str()) != NULL)
        {
            return true;
        }
    }

    if(property == "message")
    {
        if(strstr(log.GetMessage().c_str(),value.c_str()) != NULL)
        {
            return true;
        }
    }

    if(property == "itemCount")
    {
        string itemCountString = std::to_string(log.GetItemCount());
        if(strstr(itemCountString.c_str(),value.c_str()) != NULL)
        {
            return true;
        }
    }
    return false;
}

vector<Log> Service::SamplingOperation(vector<Log> logs, Operation operation)
{
    vector<Log> emptyList;
    return emptyList;
}

vector<Log> Service::OrderOperation(vector<Log> logs, Operation operation)
{
    string operationType = operation.GetoperationType();
    string property = operation.GetpropertyOnWhichIsApplied();
    string value = operation.Getvalue();
    if(property == "id")
    {
        if(value =="asc")
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return one.GetId() < two.GetId();
            });
        }
        else
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return one.GetId() > two.GetId();
            });
        }
    }
    if(property == "timestamp")
    {
        if(value =="asc")
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return strcmp(one.GetTimestamp().c_str(),two.GetTimestamp().c_str()) < 0;
            });
        }
        else
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return strcmp(one.GetTimestamp().c_str(),two.GetTimestamp().c_str()) > 0;
            });
        }

    }

    if(property == "itemType")
    {
        if(value =="asc")
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return strcmp(one.GetItemType().c_str(),two.GetItemType().c_str()) < 0;
            });
        }
        else
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return strcmp(one.GetItemType().c_str(),two.GetItemType().c_str()) > 0;
            });
        }
    }

    if(property == "name")
    {
        if(value =="asc")
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return strcmp(one.GetName().c_str(),two.GetName().c_str()) < 0;
            });
        }
        else
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return strcmp(one.GetName().c_str(),two.GetName().c_str()) > 0;
            });
        }
    }

    if(property == "message")
    {
        if(value =="asc")
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return strcmp(one.GetMessage().c_str(),two.GetMessage().c_str()) < 0;
            });
        }
        else
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return strcmp(one.GetMessage().c_str(),two.GetMessage().c_str()) > 0;
            });
        }
    }

    if(property == "itemCount")
    {
        if(value =="asc")
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return one.GetItemCount() < two.GetItemCount();
            });
        }
        else
        {
            sort(logs.begin(),logs.end(),[](Log &one, Log &two)
            {
                return one.GetItemCount() > two.GetItemCount();
            });
        }
    }
    return logs;
}
vector<Operation> Service::GetOperationsByType(vector<Operation> operations, string type)
{
    vector<Operation> operationsByType;
    for(unsigned int vectorIndex= 0; vectorIndex < operations.size(); vectorIndex++)
    {
        if(operations[vectorIndex].GetoperationType()== type)
        {
            operationsByType.push_back(operations[vectorIndex]);
        }
    }
    return operationsByType;
}

bool Service::InVector(Log element, vector<Log> elements)
{
    for(int i=0;i<elements.size();i++)
    {

        if(elements[i].GetId() == element.GetId())
        {
            return true;
        }
    }
    return false;
}
