#include "Operation.h"
#include <iostream>
#include <sstream>

Operation::Operation()
{
    //ctor
}

Operation::~Operation()
{
    //dtor
}

Operation::Operation(string operationTypeVal,string propertyOnWhichIsAppliedVal,string valueVal)
{
    operationType = operationTypeVal;
    propertyOnWhichIsApplied = propertyOnWhichIsAppliedVal;
    value = valueVal;
}

/*
 * Operator overloading, overload the >> operator for reading expenses.
 */
istream& operator>>(istream&is, Operation& operation)
{
    //string operationTypeV,propertyOnWhichIsAppliedV,valueV;
    string line, token;
    is>>line;
    stringstream ss(line);

    getline(ss, token, ',');
    operation.SetoperationType(token);

    getline(ss, token, ',');
    operation.SetpropertyOnWhichIsApplied(token);

    getline(ss, token, ',');
    operation.Setvalue(token);

    return is;
}
