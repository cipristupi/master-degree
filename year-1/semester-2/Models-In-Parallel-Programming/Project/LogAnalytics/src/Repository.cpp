#include "../include/Repository.h"
#include<algorithm>
#include<string>
#include<vector>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

Repository::Repository()
{
    //ctor
}

Repository::Repository(string fileNameVal)
{
    //ctor
    filename=fileNameVal;
    loadFromFile();
}

Repository::Repository(string logFileNameVal,string operationsFileNameVal)
{
    filename=logFileNameVal;
    operationsFileName = operationsFileNameVal;
    loadFromFile();
    loadOperationsFromFile();
}

Repository::~Repository()
{
    //dtor
}

void Repository::loadFromFile()
{
    ifstream fin(filename.c_str(),ios::in);
    if(!fin.is_open())
    {
        cout<<"ERROR again"<<endl;
        cout<<"ERROR again"<<endl;
    }
    while(fin.good())
    {
        Log el;
        fin>>el;
        entities.push_back(el);
        //cout<<ex.getID()<< " " << ex.getMonth() << "  " << ex.getDay() << " " << ex.getValue() << "  " << ex.getCategory() << endl;
    }
    fin.close();
}

void Repository::loadOperationsFromFile()
{
    ifstream fin(operationsFileName.c_str(),ios::in);
    if(!fin.is_open())
    {
        cout<<"ERROR again"<<endl;
        cout<<"ERROR again"<<endl;
    }
    while(fin.good())
    {
        Operation el;
        fin>>el;
        operations.push_back(el);
        //cout<<ex.getID()<< " " << ex.getMonth() << "  " << ex.getDay() << " " << ex.getValue() << "  " << ex.getCategory() << endl;
    }
    fin.close();
}

vector<Log> Repository::GetAll()
{
    return entities;
}

vector<Operation> Repository::GetAllOperations()
{
    return operations;
}
