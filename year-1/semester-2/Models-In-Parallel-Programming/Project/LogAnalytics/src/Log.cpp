#include "../include/Log.h"
#include <iostream>
#include <sstream>

Log::Log()
{
    //ctor
}

Log::~Log()
{
    //dtor
}

Log::Log(unsigned int idVal,string timestampVal,string itemTypeVal,string nameVal,string messageVal,unsigned int itemCountVal)
{
    //ctor
    id = idVal;
    timestamp = timestampVal;
    itemType =itemTypeVal;
    name=nameVal;
    message=messageVal;
    itemCount=itemCountVal;
}

/*
 * Operator overloading, overload the >> operator for reading expenses.
 */
istream& operator>>(istream&is, Log& log)
{

    //id,timestamp,itemType,name,message,itemCount
    unsigned int id, itemCount;
    string timestamp,itemType,name,message;
    string line, token;

    is >> line;
    //stringstream ss(line);
    stringstream ss(line);

    getline(ss, token, ',');
    stringstream(token) >> id;
    log.SetId(id);

    getline(ss, token, ',');
    //stringstream(token) >> timestamp;
    //log.SetTimestamp(timestamp);
    log.SetTimestamp(token);

    getline(ss, token, ',');
    //stringstream(token) >> itemType;
    //log.SetItemType(itemType);
    log.SetItemType(token);

    getline(ss, token, ',');
    //stringstream(token) >> name;
    //log.SetName(name);
    log.SetName(token);

    getline(ss, token, ',');
    //stringstream(token) >> message;
    //log.SetMessage(message);
    log.SetMessage(token);

    getline(ss, token, ',');
    stringstream(token) >> itemCount;
    log.SetItemCount(itemCount);

    return is;
}

ostream& operator<<(ostream&os, Log& log)
{
    os <<log.GetId() << "," << log.GetTimestamp() <<","<<log.GetItemType()<<","<<log.GetName()<<","<<log.GetMessage()<<","<<log.GetItemCount()	<< std::endl;
    return os;
}
