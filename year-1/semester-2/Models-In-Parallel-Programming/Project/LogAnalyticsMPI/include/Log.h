#ifndef LOG_H
#define LOG_H
#include<string>
using namespace std;

class Log
{
public:
    Log();
    virtual ~Log();
    Log(unsigned int idVal,string timestampVal,string itemTypeVal,string nameVal,string messageVal,unsigned int itemCountVal);

    static Log GetLogFromString(string logString);
    static string GetLogAsString(Log log);
    unsigned int GetId()
    {
        return id;
    }
    void SetId(unsigned int val)
    {
        id = val;
    }

    string GetTimestamp()
    {
        return timestamp;
    }
    void SetTimestamp(string val)
    {
        timestamp = val;
    }

    string GetItemType()
    {
        return itemType;
    }
    void SetItemType(string val)
    {
        itemType = val;
    }

    string GetMessage()
    {
        return message;
    }

    void SetMessage(string val)
    {
        message = val;
    }

    string GetName()
    {
        return name;
    }
    void SetName(string val)
    {
        name = val;
    }

    unsigned int GetItemCount()
    {
        return itemCount;
    }
    void SetItemCount(unsigned int val)
    {
        itemCount = val;
    }

    friend std::istream& operator>>(std::istream&is, Log& log);
    friend std::ostream& operator<<(std::ostream& os, Log& log);

protected:

private:
    unsigned int id;
    string timestamp;
    string itemType;
    string message;
    unsigned int itemCount;
    string name;
};

#endif // LOG_H
