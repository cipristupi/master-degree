#ifndef OPERATION_H
#define OPERATION_H
#include<string>
using namespace std;

class Operation
{
public:
    Operation();

    virtual ~Operation();

    Operation(string operationTypeVal,string propertyOnWhichIsAppliedVal,string valueVal);

    static Operation GetOperationFromString(string operationString);
    static string GetOperationAsString(Operation operation);

    string GetoperationType()
    {
        return operationType;
    }
    void SetoperationType(string val)
    {
        operationType = val;
    }
    string GetpropertyOnWhichIsApplied()
    {
        return propertyOnWhichIsApplied;
    }
    void SetpropertyOnWhichIsApplied(string val)
    {
        propertyOnWhichIsApplied = val;
    }
    string Getvalue()
    {
        return value;
    }
    void Setvalue(string val)
    {
        value = val;
    }
    friend std::istream& operator>>(std::istream&is, Operation& o);
    friend std::ostream& operator<<(std::ostream& os, Operation& operation);

protected:
private:
    string operationType;
    string propertyOnWhichIsApplied;
    string value;
};

#endif // OPERATION_H
