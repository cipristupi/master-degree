#ifndef REPOSITORY_H
#define REPOSITORY_H
#include "Log.h"
#include "Operation.h"
#include <iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;

class Repository
{
public:
    Repository();
    Repository(string fileName);
    Repository(string logFileName,string operationsFileName);
    virtual ~Repository();

    void loadFromFile();
    void loadOperationsFromFile();
    vector<Log> GetAll();
    vector<Operation> GetAllOperations();
    vector<char*> GetAllLogsAsCharString();
    vector<string> GetAllLogsAsString();

protected:

private:
    string filename;
    string operationsFileName;
    vector<Log> entities;
    vector<Operation> operations;
};

#endif // REPOSITORY_H
