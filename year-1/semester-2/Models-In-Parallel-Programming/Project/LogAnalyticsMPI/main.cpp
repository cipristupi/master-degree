#include <iostream>
#include<algorithm>
#include<string>
#include<vector>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/Repository.h"
#include "include/Log.h"
#include "include/Service.h"
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <chrono>
#include <string>
# include <cmath>
# include <cstdlib>
# include <ctime>
# include <mpi.h>
using namespace std;


#define LOGNAME_FORMAT "log/%Y%m%d_%H%M%S"
#define LOGNAME_SIZE 50
FILE *file;
char *logMessage;
int programPID;
string operationsFileName;
string logFileName;
const int rootProcess = 0;

FILE *logfile(void)
{
    static char name[LOGNAME_SIZE];
    time_t now = time(0);
    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));

    sprintf(name, "%s_%d_%s", name, programPID,operationsFileName.c_str());

    return fopen(name, "ab");
}
struct membuf : std::streambuf
{
    membuf(char* begin, char* end)
    {
        this->setg(begin, begin, end);
    }
};


void PrintResult(vector<Log> logs);
char *GetFormattedTime(time_t timeToFormat);
void LogMessage(char *message, int logFile, int logConsole, int logTime);
char *BuildMessageFromStringAndInt(char *message, int number);

int ierr;
int id;

void p0_handle(int numberOfProcesses,MPI_File *logsIn, MPI_File *operationsIn);
void pn_handle(MPI_File *logsIn, MPI_File *operationsIn);

int main(int argc, char *argv[])
{
    auto start = std::chrono::high_resolution_clock::now();
    programPID = getpid();

    int p;
    double wtime;

    ierr = MPI_Init ( &argc, &argv );

    ierr = MPI_Comm_rank ( MPI_COMM_WORLD, &id );

    ierr = MPI_Comm_size ( MPI_COMM_WORLD, &p );

    if (argc <= 1)
    {
        printf("You did not feed me arguments, I will die now :( ...");
        getchar();
        exit(1);
    }

    if ( p < 2 )
    {
        printf ( "\n" );
        printf ( "MPI_MULTITASK - Fatal error!\n" );
        printf ( "  Number of available processes must be at least 2!\n" );
        MPI_Finalize ( );
        exit ( 1 );
    }
    logFileName = argv[1];
    operationsFileName = argv[2];

    //cout<< logFileName << operationsFileName;

    file = logfile();
    logMessage = (char *)malloc(sizeof(char) * 256);

    MPI_File logsIn, operationsIn;
    ierr = MPI_File_open(MPI_COMM_WORLD, logFileName.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &logsIn);
    if (ierr)
    {
        if (id == 0) fprintf(stderr, " Couldn't open file \n");
        MPI_Finalize();
        exit(2);
    }

    ierr = MPI_File_open(MPI_COMM_WORLD, operationsFileName.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &operationsIn);
    if (ierr)
    {
        if (id == 0) fprintf(stderr, " Couldn't open file operationsIn\n");
        MPI_Finalize();
        exit(2);
    }

    if(id == rootProcess)
    {
        cout<< "In monitor process";
        wtime = MPI_Wtime ( );
        p0_handle(p,&logsIn,&operationsIn);
        wtime = MPI_Wtime ( ) - wtime;
        printf ("Process 0 time = %g\n", wtime );
        MPI_Finalize ( );
        printf ("\n" );
        printf ("MPI_MULTITASK:\n" );
        printf ("=>Normal end of execution.\n" );
    }
    else
    {
        //
        wtime = MPI_Wtime ();
        pn_handle(&logsIn,&operationsIn);
        wtime = MPI_Wtime () - wtime;
        printf ("Process %d time = %g\n",id, wtime );
        MPI_Finalize ( );
    }

    auto elapsed = std::chrono::high_resolution_clock::now() - start;

    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

    long long milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();

    long long seconds = std::chrono::duration_cast<std::chrono::seconds>(elapsed).count();

    ostringstream ss;
    ss << "Seconds:" << seconds << " | Milliseconds:" << milliseconds << " | Microseconds:" << microseconds << " | PID:" << programPID;

    string timeInMicroSeconds = ss.str();

    LogMessage((char *)timeInMicroSeconds.c_str(), 1, 0, 0);

    fclose(file);
    return 0;
}

void p0_handle(int numberOfProcesses,MPI_File *logsIn, MPI_File *operationsIn)
{
    Repository *repository = new Repository(logFileName,operationsFileName);
    Service *service=new Service();

    //Get logs as string
    vector<string> logs = repository->GetAllLogsAsString();

    //Broadcast log vector length
    long logVectorSize = logs.size();
    //printf("ROOT broadcast value: %lu \n",logVectorSize);
    MPI_Bcast(&logVectorSize,1,MPI_LONG,rootProcess,MPI_COMM_WORLD);

    //Send each log from vector to workers

    //Send logs as string from root
    for(int i=0; i<logVectorSize; i++)
    {
        printf("Sending log index from root %d \n",i);
        for(int j=1; j<numberOfProcesses; j++)
        {
            printf("Sending log index from root %d to worker %d \n",i,j);
            string currentLog = logs[i];

            int logStringLength = currentLog.length();

            //printf("Sending LogLength is %d \n",logStringLength);

            //For each log we get first the length of the string
            ierr = MPI_Send(&logStringLength,1,MPI_INT,j,rootProcess,MPI_COMM_WORLD);
            //printf("IERR is %d \n",ierr);

            //After this we get the string
            ierr = MPI_Send(currentLog.c_str(),logStringLength,MPI_UNSIGNED_CHAR,j,rootProcess,MPI_COMM_WORLD);
            //printf("IERR is %d \n",ierr);


        }
    }

    printf("clear memory in root \n");
    logs.clear();
    logs.shrink_to_fit();
    //vector<Log>().swap(logs);
    printf("clear memory in root \n");

//    printf("ROOT BEFORE SENDING OPERATIONS  \n");

    //Split searchOperations on workers
    vector<Operation> allOperations = repository->GetAllOperations();
    vector<Operation> searchOperations = service->GetOperationsByType(allOperations,"search");

    int searchOperationsLength = searchOperations.size();
    int searchOperationsPerWorker = searchOperationsLength/numberOfProcesses;
    int deviation = searchOperationsLength%numberOfProcesses;

    int startIndex,endIndex;
    startIndex=endIndex=0;

//    printf("SearchOperationLength : %d \n",searchOperationsLength);
//    printf("searchOperationsPerWorker : %d \n",searchOperationsPerWorker);
//    printf("deviation : %d \n",deviation);

    for(int i = 1; i<numberOfProcesses; i++)
    {
//        printf("In search operation for %d \n",i);
        if(i+1 == numberOfProcesses)
        {
            endIndex += deviation;
        }
        endIndex += searchOperationsPerWorker;
        if(endIndex > searchOperationsLength)
        {
            endIndex = searchOperationsLength;
        }
//        printf("Startindex : %d \n",startIndex);
//        printf("Endindex : %d \n",endIndex);

        int numberOfSearchPerWorker = endIndex - startIndex;
        if(numberOfSearchPerWorker <0)
        {
            numberOfSearchPerWorker = 0;
        }

//        printf("Sending numberOfSearchPerWorker is %d \n",numberOfSearchPerWorker);

        //Send number of expected search operation per worker
        ierr = MPI_Send(&numberOfSearchPerWorker,1,MPI_INT,i,rootProcess,MPI_COMM_WORLD);

        for(int j=startIndex; j<endIndex; j++)
        {
//            printf("Sending search operation index from root to worker \n");

            Operation currentOperation = searchOperations[j];
            string currentOperationString = Operation::GetOperationAsString(currentOperation);

//            printf("currentOperationString %s \n",currentOperationString.c_str());

            int searchStringLength = currentOperationString.length();

//            printf("Sending searchStringLength is %d \n",searchStringLength);

//            For each log we get first the length of the string
            ierr = MPI_Send(&searchStringLength,1,MPI_INT,i,rootProcess,MPI_COMM_WORLD);
//            printf("IERR is %d \n",ierr);

//            After this we get the string
            ierr = MPI_Send(currentOperationString.c_str(),searchStringLength,MPI_UNSIGNED_CHAR,i,rootProcess,MPI_COMM_WORLD);
//            printf("IERR is %d \n",ierr);
        }

        startIndex+=searchOperationsPerWorker;
    }
//    printf("Out of sending search operations \n");


    //Gathering result
    vector<Log> logVector;
    int resultVectorSize;
    for(int i=1; i<numberOfProcesses; i++)
    {
        //Receive broadcasted the length of the vector

        MPI_Recv(&resultVectorSize,1,MPI_INT,i,i,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

        printf ("Log Vector Size in ROOT %d is %lu \n", id, resultVectorSize);

        //allocate memory for log vector

        int logStringLength;

        //Get Logs as string from root
        for(int j=0; j<resultVectorSize; j++)
        {
            //For each log we get first the length of the string
            ierr = MPI_Recv(&logStringLength,1,MPI_INT,i,i,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            printf ("After Receiving logStringLength in ROOT %d \n", id);

            //Allocate the string
            char* logAsString = (char*)malloc(logStringLength*sizeof(char)+42);

            //After this we get the string
            ierr = MPI_Recv(logAsString,logStringLength,MPI_UNSIGNED_CHAR,i,i,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            printf ("After Receiving logAsString in ROOT %d \n", id);

            //Add log to vector
            std::string myString(logAsString);

            logVector.push_back(Log::GetLogFromString(logAsString));

            printf ("After adding log in result in ROOT %d \n", id);

            //Clear memory
            free(logAsString);
        }
    }

    for(int i=0; i<resultVectorSize; i++)
    {
        printf ("ROOT %d Log %d => ", id,i);
        cout<<"Root=>"<<logVector[i]<<endl;
    }

    PrintResult(logVector);
}


void pn_handle(MPI_File *logsIn, MPI_File *operationsIn)
{
    ierr = MPI_Comm_rank( MPI_COMM_WORLD, &id );// MPI_Comm_rank ( MPI_COMM_WORLD, &id );

//    printf ( "=====>ierr = %d \n", ierr );
//    printf ( "=====>WorkerId %d \n", id);


    //Receive broadcasted the length of the vector
    long logVectorSize;
    MPI_Bcast(&logVectorSize,1,MPI_LONG,rootProcess,MPI_COMM_WORLD);
//    printf ( "=====>Log Vector Size in worker %d is %lu \n", id, logVectorSize);

    //allocate memory for log vector
    vector<Log> logVector;
    int logStringLength;

    //Get Logs as string from root
    for(int i=0; i<logVectorSize; i++)
    {
        printf("=====>Receiving log index from root %d \n",i);

        //For each log we get first the length of the string
        ierr = MPI_Recv(&logStringLength,1,MPI_INT,rootProcess,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        printf("=====>IERR is %d  worker \n",ierr);
//        printf("=====>LogLength is %d \n",logStringLength);

        //Allocate the string
        char* logAsString = (char*)malloc(logStringLength*sizeof(char)+42);

        //        printf("=====>AFTER ALLOCATE in WORKER \n");

        //After this we get the string
        ierr = MPI_Recv(logAsString,logStringLength,MPI_UNSIGNED_CHAR,rootProcess,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

        //        printf("=====>AFTER MPI_RECV in WORKER \n");

        //        printf("=====>IERR is %d  worker \n",ierr);
        //printf("=====>Log is %s \n",logAsString);

        //        printf("=====>Before converting char* to string \n");
        //        printf("=====>Before converting char* to string is %lu \n",strlen(logAsString));

        //Add log to vector
        std::string myString(logAsString);
//        printf("=====>logVector.push_back(Log::GetLogFromString(logAsString)); \n");

        logVector.push_back(Log::GetLogFromString(logAsString));

//        printf("=====>AFTER add log in vector in WORKER \n");

        //Clear memory
        free(logAsString);
//        printf("=====>AFTER Clear memory in WORKER \n");
    }

//    printf("=====>AFTER Receiving Logs in WORKER \n");

    //Get search operations from root;
    int searchOperationVectorLength;

    //For each log we get first the length of the string
    ierr = MPI_Recv(&searchOperationVectorLength,1,MPI_INT,rootProcess,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

//    printf("=====>searchOperationVectorLength %d in WORKER \n",searchOperationVectorLength);

    //Allocate search operation vector
    vector<Operation> searchOperation;
    for(int i=0; i<searchOperationVectorLength; i++)
    {
        printf("=====>Receiving log index from root %d \n",i);

        int operationStringLength;

        //For each log we get first the length of the string
        ierr = MPI_Recv(&operationStringLength,1,MPI_INT,rootProcess,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        printf("=====>IERR is %d  worker \n",ierr);
//        printf("=====>operationStringLength is %d \n",operationStringLength);

        //Allocate the string
        char* operationAsString = (char*)malloc(operationStringLength*sizeof(char)+42);

//        printf("=====>AFTER ALLOCATE operationAsString in WORKER \n");

        //After this we get the string
        ierr = MPI_Recv(operationAsString,operationStringLength,MPI_UNSIGNED_CHAR,rootProcess,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

//        printf("=====>AFTER MPI_RECV operationAsString in WORKER \n");
//
//        printf("=====>IERR is %d worker \n",ierr);
//
//        printf("=====>Before converting char* to string operationAsString \n");
//        printf("=====>Before converting char* to string operationAsString is %lu \n",strlen(operationAsString));

        //Add log to vector
        std::string myString(operationAsString);
//        printf("=====>searchOperation.push_back(Operation::GetOperationFromString(logAsString)); \n");

        searchOperation.push_back(Operation::GetOperationFromString(operationAsString));

//        printf("=====>AFTER add log in vector in WORKER \n");

        //Clear memory
        free(operationAsString);
//        printf("=====>AFTER Clear memory in WORKER \n");
    }

//    //Print received logs;
//    printf ( "=====>WorkerId %d BEFORE Log FOR \n", id);
//    for(int i=0; i<logVectorSize; i++)
//    {
//        printf ("=====>WorkerId %d Log %d => ", id,i);
//        cout<<logVector[i]<<endl;
//    }
//
    //Print received search operation ;
//    printf ( "=====>WorkerId %d BEFORE Operation FOR \n", id);
//    for(int i=0; i<searchOperationVectorLength; i++)
//    {
//        printf ("=====>WorkerId %d Operation %d => ", id,i);
//        cout<<searchOperation[i]<<endl;
//    }

    //Repository *repository = new Repository(logFileName,operationsFileName);
    Service *service=new Service();

    vector<Log> result = service->ProcessData(logVector,searchOperation,"search");

    int resultVectorLength = result.size();
    //For each log we get first the length of the string
//    printf("=====>Sending resultVectorLength is %d \n",resultVectorLength);

    //Send number of logs from result to ROOT
    ierr = MPI_Send(&resultVectorLength,1,MPI_INT,rootProcess,id,MPI_COMM_WORLD);

    //Send logs as string from root
    for(int i=0; i<resultVectorLength; i++)
    {
//        printf("Sending log index from root %d to worker %d \n",i,j);
        Log currentLog = result[i];

        string currentLogAsString = Log::GetLogAsString(currentLog);
        int logStringLength = currentLogAsString.length();

//        printf("=====>Sending LogLength is %d \n",logStringLength);

        //For each log we get first the length of the string
        ierr = MPI_Send(&logStringLength,1,MPI_INT,rootProcess,id,MPI_COMM_WORLD);

//        printf("=====>After Sending LogLength is %d \n",logStringLength);
        //printf("IERR is %d \n",ierr);

        //After this we get the string
        ierr = MPI_Send(currentLogAsString.c_str(),logStringLength,MPI_UNSIGNED_CHAR,rootProcess,id,MPI_COMM_WORLD);
        //printf("IERR is %d \n",ierr);

//        printf("=====>After Sending log \n");
    }
    printf("=====>After Sending log \n");
    //MPI_Barrier(MPI_COMM_WORLD);
}

//-------------------------------------------------------------------


void PrintResult(vector<Log> logs)
{
    ofstream myfile;
    static char name[256];

    sprintf(name, "logs_result_%s_%d.txt",operationsFileName.c_str(),programPID);
    myfile.open (name);


    for(int row=0; row<logs.size(); row++)
    {
        myfile<<logs[row];
    }
    myfile<<endl;
    myfile.close();
}



char *GetFormattedTime(time_t timeToFormat)
{
    char *buffer = (char *)malloc(sizeof(char) * 26);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", localtime(&timeToFormat));
    return buffer;
}

void LogMessage(char *message, int logFile, int logConsole, int logTime)
{
    char *tempMessage;
    if (logTime == 1)
    {
        char *tempMessage = (char *)malloc(sizeof(char) * 256);
        time_t currentTime;
        currentTime = time(0);
        memset(tempMessage, 0, sizeof tempMessage);
        sprintf(tempMessage, "%s %s \r\n", message, GetFormattedTime(currentTime));
        if (logFile == 1)
        {
            fprintf(file, "%s \r\n", tempMessage);
        }
        if (logConsole == 1)
        {
            cout << tempMessage << endl;
        }
        free(tempMessage);
        return;
    }
    if (logFile == 1)
    {
        fprintf(file, "%s \r\n", message);
    }
    if (logConsole == 1)
    {
        cout << message << endl;
    }
}

char *BuildMessageFromStringAndInt(char *message, int number)
{
    char *tempMessage = (char *)malloc(sizeof(char) * 256);
    memset(tempMessage, 0, sizeof(tempMessage));
    sprintf(tempMessage, "%s %d", message, number);
    return tempMessage;
}
